"""Contains context structs which can be used to pass
configurated objects in a specific scope
to different objects and functions.
"""
from enum import Enum
from typing import Any, Dict, List, Optional, Union

from twilio.twiml.messaging_response import MessagingResponse

from structs.context import SessionContext, WorkspaceContext
from type_check.input_types import Url


class MediaOrigin(Enum):
    """Enum to indicate origin of the media."""

    WHATSAPP_BUSINESS = "whatsapp_business"
    TELEGRAM = "telegram"
    TWILIO = "twilio"


class MediaType(Enum):
    """Enum to indicate type/category of the media."""

    AUDIO = "audio"
    IMAGE = "image"
    DOCUMENT = "document"
    VIDEO = "video"


class Media:
    def __init__(
        self,
        origin: MediaOrigin,
        url: Optional[Url] = None,
        file_id: Optional[str] = None,
        media_type: Optional[MediaType] = None,
        content_type: Optional[str] = None,
    ) -> None:
        self.origin = origin
        self.url = url
        self.file_id = file_id
        self.media_type = media_type
        self.content_type = content_type


class IncommingMessage:
    def __init__(
        self,
        identifier: str,
        message: str = "",
        latitude: Optional[str] = None,
        longitude: Optional[str] = None,
        media: Optional[Media] = None,
    ) -> None:
        self.identifier = identifier
        self.message = message
        self.latitude = latitude
        self.longitude = longitude
        self.media = media


class ResponseMessage:
    """Class for response data. Can be converted to XML-string for Twilio."""

    def __init__(
        self,
        step_type: str,
        message: str,
        image: Optional[str] = None,
        options: Optional[List[str]] = [],
        option_error: Optional[str] = "",
    ) -> None:
        self.type = step_type
        self.message = message
        self.image = image
        self.options = options
        self.option_error = option_error

    def telegram(self, identifier: str) -> Dict[str, str]:
        response_json = {"chat_id": identifier}

        if self.image:
            response_json["photo"] = self.image
            response_json["caption"] = self.__get_message_body()
        else:
            response_json["text"] = self.__get_message_body()

        return response_json

    def whatsapp_business(self, identifier: str) -> Dict[str, str]:
        response_json = {
            "messaging_product": "whatsapp",
            "recipient_type": "individual",
            "to": identifier,
        }

        if self.image:
            image_section = {
                "type": "image",
                "image": {"link": self.image, "caption": self.__get_message_body()},
            }

            response_json.update(image_section)
            return response_json

        message_section = {
            "type": "text",
            "text": {"preview_url": False, "body": self.__get_message_body()},
        }

        response_json.update(message_section)
        return response_json

    def twilio(self) -> str:
        """Returns XML-string for response message."""
        resp = MessagingResponse()
        resp_xml = resp.message(self.__get_message_body())
        if self.image:
            resp_xml.media(self.image)

        return str(resp)

    def __get_message_body(self) -> str:
        if self.type == "selection":
            return f"{self.message}\n\n{self.__get_selection_body()}"

        return self.message

    def __get_selection_body(self) -> str:
        """Generates the options string of selection response."""
        if len(self.options) == 0:
            return self.option_error

        return "\n".join(
            [f"{index+1}: {option}" for index, option in enumerate(self.options)]
        )


class TranslatedMessage:
    """Class containing all translations and variables for a translation key."""

    def __init__(self, messages: Dict[str, str]) -> None:
        self.messages: Dict[str, str] = messages

        self.variables: Dict[str, str] = {}

    def get_message(
        self, ctx: Union[WorkspaceContext, SessionContext], language: str
    ) -> str:
        """Returns message for provided language.
        If no translation in language is available,
        translation in default-language is returned.
        """

        localized_message = self.messages.get(language)
        if localized_message is None:
            return self.messages.get(ctx.default_language)

        return localized_message

    def to_response(
        self,
        ctx: Union[WorkspaceContext, SessionContext],
        language: str,
        variables: Dict[str, Any] = {},
    ) -> ResponseMessage:
        """Returns response message object (message step)
        that contains translated message.
        """

        text = self.get_message(ctx, language)
        for token, variable_value in variables.items():
            text = text.replace(token, str(variable_value))

        return ResponseMessage("message", text)
