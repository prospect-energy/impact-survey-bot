"""Contains structs for survey and step sections."""
from typing import Any, Callable, Dict, List, Optional

from structs.messages import TranslatedMessage


class ConfigSection:
    """Struct for config section of survey."""

    def __init__(
        self,
        create_db_entry: bool,
        table: Optional[str],
        id_column: Optional[str],
        has_finish_message: bool,
        finish_message: Optional[TranslatedMessage],
    ) -> None:
        self.create_db_entry = create_db_entry
        self.table = table
        self.id_column = id_column
        self.has_finish_message = has_finish_message
        self.finish_message = finish_message


class ConditionSection:
    """Struct for condition section of steps."""

    def __init__(
        self,
        func: Callable,
        params: Dict[str, Any],
        variables: Dict[str, Any],
        successor: Any,
    ) -> None:
        self.func = func
        self.params = params
        self.variables = variables
        self.successor = successor


class SkipSection:
    """Struct for skip section of response steps."""

    def __init__(self, enabled: bool, successor: str) -> None:
        self.enabled = enabled
        self.successor = successor


class CompareSection:
    """Struct for compare section of message steps."""

    def __init__(
        self,
        func: Callable,
        params: Dict[str, Any],
        variables: Dict[str, List[str]],
        fail_message: TranslatedMessage,
    ) -> None:
        self.func = func
        self.params = params
        self.variables = variables
        self.fail_message = fail_message


class SpecifySection:
    """Struct for specify section of selection steps."""

    def __init__(
        self,
        option: TranslatedMessage,
        message: TranslatedMessage,
        help_: Optional[TranslatedMessage],
        image: Optional[str],
        input_validation: Callable,
        input_type: str,
        invalid_input_message: TranslatedMessage,
        modifiers: List[Callable],
        successor: str,
    ) -> None:
        self.option = option
        self.message = message
        self.help_ = help_
        self.image = image
        self.input_validaiton = input_validation
        self.input_type = input_type
        self.invalid_input_message = invalid_input_message
        self.modifiers = modifiers
        self.successor = successor
