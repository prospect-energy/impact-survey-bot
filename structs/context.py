"""Contains context classes."""
from __future__ import annotations

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from database.database import Postgres
    from logger.logger import Logger
    from session.session import Session
    from storage.s3 import S3
    from workspace.event_handler import EventHandler


class BotContext:
    """Class for the root context containing bot-specific data."""

    def __init__(
        self,
        logger: Logger,
        postgres: Postgres,
        s3: S3,
        timeout_interval: int,
        wab_api_token: str,
        telegram_api_token: str,
    ) -> None:
        self.logger = logger
        self.postgres = postgres
        self.s3 = s3
        self.timeout_interval = timeout_interval
        self.wab_api_token = wab_api_token
        self.telegram_api_token = telegram_api_token


class WorkspaceContext(BotContext):
    """Class for the context that contains workspace-specific data."""

    def __init__(
        self,
        logger: Logger,
        postgres: Postgres,
        s3: S3,
        timeout_interval: int,
        wab_api_token: str,
        telegram_api_token: str,
        workspace_id: int,
        workspace_schema: str,
        default_language: str,
        default_group_id: int,
        workspace_asset_key: str,
    ) -> None:
        super().__init__(
            logger=logger,
            postgres=postgres,
            s3=s3,
            timeout_interval=timeout_interval,
            wab_api_token=wab_api_token,
            telegram_api_token=telegram_api_token,
        )

        self.workspace_id = workspace_id
        self.workspace_schema = workspace_schema
        self.default_language = default_language
        self.default_group_id = default_group_id
        self.workspace_asset_key = workspace_asset_key


class SessionContext(WorkspaceContext):
    """Class for the context that contains session-specific data."""

    def __init__(
        self,
        logger: Logger,
        postgres: Postgres,
        s3: S3,
        timeout_interval: int,
        wab_api_token: str,
        telegram_api_token: str,
        workspace_id: int,
        workspace_schema: str,
        default_language: str,
        default_group_id: int,
        workspace_asset_key: str,
        event_handler: EventHandler,
        session: Session,
    ) -> None:
        super().__init__(
            logger=logger,
            postgres=postgres,
            s3=s3,
            timeout_interval=timeout_interval,
            wab_api_token=wab_api_token,
            telegram_api_token=telegram_api_token,
            workspace_id=workspace_id,
            workspace_schema=workspace_schema,
            default_language=default_language,
            default_group_id=default_group_id,
            workspace_asset_key=workspace_asset_key,
        )

        self.event_handler = event_handler
        self.session = session
