# Module: Bot

The bot module is the core module of this project. It contains the `ImpactSurveyBot` class which is the main class of this project containing all the objects connected to the bot functionality. In addition, the module contains the classes `TimeoutHandler`, `UpdateHandler` and `WorkspaceHandler` which are direct dependencies of the `ImpactSurveyBot` class.

## When to use it?

Only one `ImpactSurveyBot` class is needed for operating the bot instance.
In addition, it is used for integration tests.

## How to use it?

```python
bot_config = {
    "instance": {"instance_id": <instance_id>},
    "surveys": {"registration_survey": <name of registration survey>},
    "postgres": {
        "host": <postgres server address>,
        "database": <postgres database>,
        "user": <postgres user>,
        "password": <postgres password>,
        "system_schema": <name of system schema>,
        "shared_schema": <name of shared schema>,
    },
    "s3": {
        "enabled": <s3 enabled <"True"/"False">>,
        "access_key": <s3 access key>,
        "secret_key": <s3 secret key>,
        "bucket": <s3 bucket>,
        "localpath": <s3 localpath>,
    },
    "whatsapp_business": {
        "api_token": <whatsapp business api token>,
        "phone_number_id": <whatsapp business phone_number_id >,
    },
    "telegram": {
        "api_token": <telegram bot token>,
    },
    "timeout": {"timeout_interval": <timeout check interval <minutes>>},
    "logging": {"level": <log level>},
}

bot = ImpactSurveyBot(bot_config)
```
*All configuration values require strings to support environmental variables.*
