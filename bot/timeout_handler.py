"""Contains TimeoutHandler class."""
import datetime
import time
from sched import scheduler
from threading import Lock, Thread
from typing import Dict

from session import session_handler
from session.session import Session
from structs.context import BotContext


class TimeoutHandler:
    """Singleton to manage timeout check across all workspaces."""

    def __init__(
        self, bot_ctx: BotContext, sessions: Dict[str, Session], sessions_mutex: Lock
    ) -> None:
        self.__ctx = bot_ctx
        self.__sessions = sessions
        self.__sessions_mutex = sessions_mutex

        self.__check_interval = bot_ctx.timeout_interval
        self.__scheduler = scheduler(time.time, time.sleep)

        self.__thread = Thread(target=self.__timeout_thread, args=(), daemon=True)
        self.__thread.start()

    def __timeout_thread(self) -> None:
        """Function running in a seperate threat as wrapper for timeout check."""

        def __check_timeout(scheduler: scheduler) -> None:
            """Checks for timeouts and handles them."""
            timeout_identifiers = []
            current_time = datetime.datetime.now()

            self.__ctx.logger.debug("[Timeout] Checking for timeout...")

            for participant_identifier, session in self.__sessions.items():
                if session is None:
                    continue

                last_action = session.last_action_utc
                workspace = session.workspace

                if workspace.timeout_enabled:
                    time_difference = current_time - last_action
                    time_difference_minutes = time_difference.total_seconds() / 60

                    if time_difference_minutes > workspace.timeout_in_minutes:
                        timeout_identifiers.append(participant_identifier)

            for participant_identifier in timeout_identifiers:
                self.__ctx.logger.debug(
                    f"[Timeout] Removing session: {participant_identifier}"
                )
                session = self.__sessions[participant_identifier]
                session_ctx = session_handler.get_session_ctx(session)

                session_handler.end_session(
                    self.__sessions, self.__sessions_mutex, session_ctx, False
                )

            self.__scheduler.enter(
                self.__check_interval * 60, 1, __check_timeout, (scheduler,)
            )

        self.__scheduler.enter(
            self.__check_interval * 60, 1, __check_timeout, (self.__scheduler,)
        )
        self.__scheduler.run()
