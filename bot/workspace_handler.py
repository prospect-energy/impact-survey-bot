"""Contains WorkspaceHandler class."""
from datetime import datetime
from threading import Lock
from typing import Dict, List, Optional

from psycopg2 import DatabaseError

import database.queries as queries
from bot.timeout_handler import TimeoutHandler
from bot.update_handler import UpdateHandler
from database.components import Column, GroupBy, JoinTable, Table, WhereClause
from request.update import UpdateRequest
from session import session_handler
from session.session import Session
from structs.context import BotContext
from structs.messages import IncommingMessage, ResponseMessage
from type_check import request_validation
from workspace.workspace import Workspace


class WorkspaceHandler:
    """Singleton to manage workspaces and handle incomming twilio and update requests."""

    def __init__(self, bot_ctx: BotContext, instance_id: int) -> None:
        self.__bot_ctx = bot_ctx
        self.__instance_id = instance_id

        self.__workspaces: Dict[int, Workspace] = {}
        self.__sessions: Dict[str, Session] = {}

        self.__in_workspace_selection: Dict[str, List[int]] = {}
        self.__in_verification = []

        self.__workspaces_mutex = Lock()
        self.__sessions_mutex = Lock()

        self.__update_handler = UpdateHandler(
            bot_ctx=self.__bot_ctx,
            workspaces=self.__workspaces,
            workspaces_mutex=self.__workspaces_mutex,
        )

        self.__update_handler.load_all_workspaces(self.__instance_id)

        TimeoutHandler(
            bot_ctx=self.__bot_ctx,
            sessions=self.__sessions,
            sessions_mutex=self.__sessions_mutex,
        )

    def process(self, request: IncommingMessage) -> ResponseMessage:
        """Checks if participant identifier is associated with active session.
        If there isn't any active session it handles both workspace
        selection as well as verification code check.
        """
        identifier = request.identifier
        active_session = self.__sessions.get(identifier)

        if "/invitation=" in request.message:
            return self.__handle_invitation(request, active_session)

        # active session exists
        if active_session:
            return session_handler.process_existing(
                self.__sessions, self.__sessions_mutex, active_session, request
            )

        # participant was prompted for verification code
        if identifier in self.__in_verification:
            return self.__handle_verification(request)

        # participant was prompted for workspace selection
        if workspace_ids := self.__in_workspace_selection.get(identifier):
            return self.__handle_workspace_selection(workspace_ids, request)

        # no active session, no active prompt
        workspace_ids = self.__get_workspaces_ids_by_participant_identifier(identifier)
        if len(workspace_ids) == 0:
            self.__in_verification.append(identifier)
            return get_verification_response()

        if len(workspace_ids) > 1:
            self.__in_workspace_selection[identifier] = workspace_ids
            return self.__get_workspace_selection_response(workspace_ids)

        workspace = self.__workspaces[workspace_ids[0]]
        return session_handler.process_new(
            self.__sessions, self.__sessions_mutex, workspace, request
        )

    def update(self, request: UpdateRequest) -> None:
        """Triggers workspace update in UpdateHandler."""
        self.__update_handler.update_workspace(request)

    def __get_workspaces_ids_by_participant_identifier(
        self, identifier: str
    ) -> List[int]:
        """Returns a list of workspace ids that the participant with the identifier has access to."""
        postgres = self.__bot_ctx.postgres

        participants_table = Table(postgres.shared_schema, "participants")
        group_allocation_table = Table(postgres.shared_schema, "group_allocation")
        groups_table = Table(postgres.shared_schema, "groups")
        workspaces_table = Table(postgres.system_schema, "workspaces")

        workspace_id_rows = postgres.select(
            participants_table,
            False,
            [Column(groups_table, "workspace_id")],
            [
                JoinTable(
                    group_allocation_table,
                    Column(participants_table, "participant_id"),
                    Column(group_allocation_table, "participant_id"),
                ),
                JoinTable(
                    groups_table,
                    Column(group_allocation_table, "group_id"),
                    Column(groups_table, "group_id"),
                ),
                JoinTable(
                    workspaces_table,
                    Column(groups_table, "workspace_id"),
                    Column(workspaces_table, "workspace_id"),
                ),
            ],
            [
                WhereClause(Column(participants_table, "identifier"), identifier),
                WhereClause(
                    Column(workspaces_table, "instance_id"), self.__instance_id
                ),
            ],
            None,
            GroupBy([Column(groups_table, "workspace_id")]),
            True,
        )

        return [row[0] for row in workspace_id_rows]

    def __get_workspace_selection_response(
        self, workspace_ids: List[int]
    ) -> ResponseMessage:
        """Returns the workspace selection response from a list of workspace ids."""
        options = []

        for id_ in workspace_ids:
            workspace = self.__workspaces[id_]
            options.append(workspace.display_name)

        return ResponseMessage(
            "selection",
            "Please select the workspace you want to use:\n\nVeuillez sélectionner l'espace de travail que vous souhaitez utiliser:",
            None,
            options,
        )

    def __handle_invitation(
        self, request: IncommingMessage, is_active_session: bool
    ) -> ResponseMessage:
        """Handle is request where message is invitation token and code.
        Checks whether code is valid and creates database entries.
        """

        invitation_token = request.message.split("=")[1]
        postgres = self.__bot_ctx.postgres

        res = queries.get_group_and_workspace_id_by_invitation_token(
            postgres, invitation_token
        )
        if not res:
            return ResponseMessage(
                "message",
                "Invitation token is invalid or expired.\n\nLe jeton d'invitation n'est pas valide ou a expiré.",
            )

        group_id = res[0]
        workspace_id = res[1]

        participant_id = queries.get_participant_id_by_workspace_and_identifier(
            postgres, workspace_id, request.identifier
        )
        group_allocation_table = Table(postgres.shared_schema, "group_allocation")
        workspace = self.__workspaces[workspace_id]
        default_group_id = workspace.ctx.default_group_id

        group_allocation = []

        try:
            if not participant_id:
                participant_id = queries.create_participant_entry_and_return_id(
                    postgres, request.identifier, "invitation_link"
                )

                group_allocation = [
                    {
                        "participant_id": participant_id,
                        "group_id": default_group_id,
                        "created_at": datetime.now(),
                        "created_by": "invitation_link",
                    }
                ]

            if group_id != default_group_id:
                invitation_group_allocation = {
                    "participant_id": participant_id,
                    "group_id": group_id,
                    "created_at": datetime.now(),
                    "created_by": "invitation_link",
                }
                group_allocation.append(invitation_group_allocation)

            postgres.insert(
                table=group_allocation_table,
                data=group_allocation,
                error_on_conflict=False,
            )
        except DatabaseError:
            return ResponseMessage(
                "message",
                "Invitation was not successful. Please try again.\n\n"
                "L'invitation n'a pas abouti. Veuillez réessayer.",
            )

        if is_active_session:
            return ResponseMessage(
                "message",
                "Invitation was successful. You can continue your current survey. To see the new survey, restart the chat by sending *start*.\n\n"
                "L'invitation a été acceptée. Vous pouvez poursuivre l'enquête en cours. Pour voir la nouvelle enquête, redémarrez le chat en envoyant *start*.",
            )

        return session_handler.process_new(
            self.__sessions, self.__sessions_mutex, workspace, request
        )

    def __handle_verification(self, request: IncommingMessage) -> ResponseMessage:
        """Checks if request.message is an valid verification code.
        If database contains provided verification code, the participant identifier is added
        in database entry and session is created.
        If verification code is invalid, participant is asked to try again.
        """

        postgres = self.__bot_ctx.postgres
        row = queries.get_participant_entry_by_verification_code(
            postgres, request.message
        )

        if row == [] or row is None:
            return ResponseMessage(
                "message",
                "Verification code is invalid, please try again:\n\n"
                "Le code de vérification n'est pas valide, veuillez réessayer:",
            )

        participant_identifier = row[0]
        workspace_id = row[1]

        if participant_identifier:
            return ResponseMessage(
                "message",
                "Verification code is already in use, please contact the project supervisor.\n\n"
                "Le code de vérification est déjà utilisé, veuillez contacter le superviseur du projet.",
            )

        workspace = self.__workspaces[workspace_id]

        participants_table = Table(postgres.shared_schema, "participants")
        postgres.update(
            participants_table,
            [
                WhereClause(
                    Column(participants_table, "verification_code"), request.message
                )
            ],
            {"identifier": request.identifier},
        )

        if request.identifier in self.__in_verification:
            self.__in_verification.remove(request.identifier)

        return session_handler.process_new(
            self.__sessions, self.__sessions_mutex, workspace, request
        )

    def __handle_workspace_selection(
        self, workspace_ids: List[int], request: IncommingMessage
    ) -> Optional[ResponseMessage]:
        """Checks if participant response is compatible with workspace selection message.
        If response is not a number or out of range, workspace selection message
        is sent again.
        """
        if request_validation.can_be_int(request):
            selected_number = int(request.message)
            if 0 < selected_number <= len(workspace_ids):
                workspace_id = workspace_ids[selected_number - 1]
                workspace = self.__workspaces[workspace_id]

                del self.__in_workspace_selection[request.identifier]
                return session_handler.process_new(
                    self.__sessions, self.__sessions_mutex, workspace, request
                )

        return self.__get_workspace_selection_response(workspace_ids)


def get_verification_response() -> ResponseMessage:
    """Returns response message with request to enter verification code."""
    return ResponseMessage(
        "message",
        "What is your verification code?\n\nQuel est votre code de vérification?",
    )
