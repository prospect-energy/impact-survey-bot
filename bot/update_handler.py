"""Contains UpdateHandler class."""
from threading import Lock
from typing import Any, Dict, List, Tuple

from database.components import Column, Table
from database.queries import get_survey_data, get_workspace_data
from parsing.util import safe_get
from request.update import UpdateRequest
from result.error import BotException
from structs.context import BotContext, WorkspaceContext
from workspace.workspace import Workspace


class UpdateHandler:
    """Singleton to handle workspace updates."""

    def __init__(
        self, bot_ctx: BotContext, workspaces: List[Workspace], workspaces_mutex: Lock
    ) -> None:
        self.__ctx = bot_ctx
        self.__workspaces = workspaces
        self.__workspaces_mutex = workspaces_mutex

    def update_workspace(self, request: UpdateRequest) -> None:
        """Wrapper for workspace update."""
        db_row = get_workspace_row(self.__ctx, request.workspace_id)
        self.__convert_and_set_workspace(row=db_row, include_invalid_surveys=True)

    def load_all_workspaces(self, instance_id: int) -> None:
        """Queries database for workspaces and initializes them."""
        workspaces_table = Table("system", "workspaces")
        translations_table = Table("system", "translations")

        workspace_entries = get_workspace_data(
            self.__ctx.postgres,
            columns=[
                Column(workspaces_table, "workspace_id"),
                Column(workspaces_table, "display_name"),
                Column(workspaces_table, "db_schema"),
                Column(workspaces_table, "timeout_in_minutes"),
                Column(workspaces_table, "timeout_enabled"),
                Column(workspaces_table, "workspace_token"),
                Column(workspaces_table, "workspace_asset_key"),
                Column(translations_table, "translations"),
            ],
            instance_id=instance_id,
        )

        for db_row in workspace_entries:
            try:
                self.__convert_and_set_workspace(
                    row=db_row, include_invalid_surveys=False
                )
            except BotException as error:
                workspace_name = safe_get(db_row, 2, "unknown")
                self.__ctx.logger.error(
                    f"""Workspace '{workspace_name}' could not be loaded.
                    Error: {error.get_log_message()}"""
                )
                pass

    def __convert_and_set_workspace(
        self, row: tuple, include_invalid_surveys: bool
    ) -> None:
        """Converts database row of workspace, retrieves survey config, initializes and sets workspace."""
        config = convert_workspace_config(row)
        workspace_id = config["workspace"]["workspace_id"]

        survey_config = get_survey_config(
            ctx=self.__ctx,
            workspace_id=workspace_id,
            include_invalid=include_invalid_surveys,
        )
        config["surveys"] = survey_config

        workspace_ctx = WorkspaceContext(
            *self.__ctx.__dict__.values(),
            workspace_id=workspace_id,
            workspace_schema=config["db_schema"],
            default_language=config["default_language"],
            default_group_id=config["default_group_id"],
            workspace_asset_key=config["workspace_asset_key"],
        )

        workspace = Workspace(workspace_ctx=workspace_ctx, config=config)

        self.__workspaces_mutex.acquire()
        self.__workspaces[workspace_id] = workspace
        self.__workspaces_mutex.release()


def convert_workspace_config(row: tuple) -> Dict[str, Any]:
    """Returns workspace config from database entry tuple."""
    workspace_id = row[0]

    return {
        "workspace": {
            "workspace_id": workspace_id,
            "display_name": row[1],
        },
        "timeout": {"timeout_in_minutes": row[3], "timeout_enabled": row[4]},
        "commands": {
            "command_aliases": convert_command_aliases(row[10]),
        },
        "translations": {
            "translations": row[7],
            "enabled_languages": convert_enabled_languages(row[11]),
            "mandatory_translations": get_dict_from_db_array(row[13]),
        },
        "workspace_asset_key": row[6],
        "events": {"webhooks": convert_webhooks(row[12]), "workspace_token": row[5]},
        "db_schema": row[2],
        "default_language": row[8],
        "default_group_id": row[9],
    }


def convert_enabled_languages(db_array: List[Tuple[str, str, str]]) -> Dict[str, dict]:
    """Returns dictionary of enabled languages from query-specific tuple-array."""
    enabled_languages = {}

    for item in db_array:
        shortcode = item[0]
        display_name = item[1]
        command = item[2]

        enabled_languages[shortcode] = {
            "display_name": display_name,
            "command": command,
        }

    return enabled_languages


def convert_command_aliases(
    db_array: List[Tuple[str, str, str]]
) -> Dict[str, List[str]]:
    """Returns dictionary of command aliases from query-specific tuple-array."""
    command_aliases = {}

    for item in db_array:
        command_type = item[0]
        alias = item[1]

        if command_type not in command_aliases:
            command_aliases[command_type] = [alias]
        else:
            command_aliases[command_type].append(alias)

    return command_aliases


def convert_webhooks(db_array: List[Tuple[str, str, str]]) -> Dict[str, dict]:
    """Returns dictionary of webhook configuration from query-specific tuple-array."""
    webhooks = {}

    for item in db_array:
        display_name = item[0]
        url = item[1]
        include_token = item[2] in ["true", "True"]

        webhooks[display_name] = {"url": url, "include_token": include_token}

    return webhooks


def get_dict_from_db_array(array: List[Tuple[str, str]]) -> Dict[str, Any]:
    """Returns python dictionary from query-specific tuple-array."""
    dictionary = {}

    for tuple_ in array:
        key = tuple_[0]
        value = tuple_[1]

        dictionary[key] = value

    return dictionary


def get_workspace_row(ctx: BotContext, workspace_id: int) -> List[tuple]:
    """Returns workspace entry from database."""
    workspaces_table = Table("system", "workspaces")
    translations_table = Table("system", "translations")

    workspace_entries = get_workspace_data(
        ctx.postgres,
        columns=[
            Column(workspaces_table, "workspace_id"),
            Column(workspaces_table, "display_name"),
            Column(workspaces_table, "db_schema"),
            Column(workspaces_table, "timeout_in_minutes"),
            Column(workspaces_table, "timeout_enabled"),
            Column(workspaces_table, "workspace_token"),
            Column(workspaces_table, "workspace_asset_key"),
            Column(translations_table, "translations"),
        ],
        workspace_id=workspace_id,
    )

    return workspace_entries[0]


def get_survey_config(
    ctx: BotContext, workspace_id: int, include_invalid: bool
) -> Dict[str, Any]:
    """Retrieves a dictionary with data from all surveys
    as well as configuration for this workspace.
    """

    survey_config = {
        "registration": "registration",
        "selection": "selection",
        "surveys": {},
    }

    survey_entries = get_survey_data(ctx.postgres, workspace_id, include_invalid)

    for item in survey_entries:
        display_name = item[0]
        is_registration = item[1]
        survey = item[2]
        group_name = item[3]

        if is_registration:
            survey_config["registration"] = display_name

        survey_config["surveys"][display_name] = {
            "data": survey,
            "group": group_name,
        }

    return survey_config
