# Postgres-Database Setup <!-- omit in toc -->

Because there is no auto-creation of the database yet, the setup needs to be done manually. To create the database, just execute the following queries __(in the order they are displayed!)__. Make sure to replace database-name and user passwords.

__Requires:__
- Postgres-Server (`version: >= 12`)

__Entity–Relationship Model:__
![ERD-Diagram](/docs/assets/ERD_full.png)

---

## Content <!-- omit in toc -->

- [1. Create Users](#1-create-users)
- [2. Create Database](#2-create-database)
- [3. Create Schemas](#3-create-schemas)
- [4. Create Tables](#4-create-tables)
  - [4.1. Instances](#41-instances)
  - [4.2. Organizations](#42-organizations)
  - [4.3. Workspaces](#43-workspaces)
  - [4.4. Managers](#44-managers)
  - [4.5. Translations](#45-translations)
  - [4.6. Data\_Types](#46-data_types)
  - [4.7. Custom\_Fields](#47-custom_fields)
  - [4.8. Languages](#48-languages)
  - [4.9. Commands](#49-commands)
  - [4.10. Mandatory\_Translations](#410-mandatory_translations)
  - [4.11. Webhooks](#411-webhooks)
  - [4.12. Enabled\_Languages](#412-enabled_languages)
  - [4.13. Command\_Aliases](#413-command_aliases)
  - [4.14. Participants](#414-participants)
  - [4.15. Groups](#415-groups)
  - [4.16. Group\_Allocation](#416-group_allocation)
  - [4.17. Sessions](#417-sessions)
  - [4.18. Surveys](#418-surveys)
  - [4.19. Survey\_Visibility](#419-survey_visibility)
  - [4.20. Group\_Invitations](#420-group_invitations)
- [5. Content of Static Tables / Data](#5-content-of-static-tables--data)
  - [5.1. Mandatory\_Translations](#51-mandatory_translations)
  - [5.2. Languages](#52-languages)
  - [5.3. Commands](#53-commands)
  - [5.4. Data\_Types](#54-data_types)
- [6. Content of Other Tables](#6-content-of-other-tables)
  - [6.1. Organizations](#61-organizations)
  - [6.2. Instances](#62-instances)
  - [6.3. Workspaces](#63-workspaces)
  - [6.4. Managers](#64-managers)
  - [6.5. Webhooks](#65-webhooks)
  - [6.6. Enabled\_Languages](#66-enabled_languages)
  - [6.7. Command\_Aliases](#67-command_aliases)
  - [6.8. Groups](#68-groups)
  - [6.9. Participants](#69-participants)
  - [6.10. Group\_Allocation](#610-group_allocation)
  - [6.11. Translations](#611-translations)
  - [6.12. Surveys](#612-surveys)
  - [6.12. Survey\_Visability](#612-survey_visability)
  - [6.13. Group\_Invitations](#613-group_invitations)
- [7. Create Workspace Role](#7-create-workspace-role)
  - [7.1 Create Role](#71-create-role)
  - [7.2 Grant Permissions for Workspace Schema](#72-grant-permissions-for-workspace-schema)
  - [7.3 Grant Permissions for Shared Schema](#73-grant-permissions-for-shared-schema)
  - [7.4 Create RLS Policy on Groups Table](#74-create-rls-policy-on-groups-table)
  - [7.5 Create RLS Policy on Group\_Allocation Table](#75-create-rls-policy-on-group_allocation-table)
  - [7.6 Create RLS Policy on Participants Table](#76-create-rls-policy-on-participants-table)

## 1. Create Users
```sql
CREATE ROLE isb_admin WITH
	LOGIN
    SUPERUSER
    INHERIT
    CREATEDB
    CREATEROLE
    NOREPLICATION
	CONNECTION LIMIT -1
	PASSWORD '<password>';

CREATE ROLE isb_client WITH
	LOGIN
	NOSUPERUSER
	NOCREATEDB
	NOCREATEROLE
	INHERIT
	NOREPLICATION
	CONNECTION LIMIT -1
	PASSWORD '<password>';
```

## 2. Create Database
```sql
CREATE DATABASE <name>
    WITH 
    OWNER = isb_admin
    ENCODING = 'UTF8'
    CONNECTION LIMIT = -1;

REVOKE CONNECT ON DATABASE <name> FROM public;

GRANT CONNECT ON DATABASE <name> TO isb_client;
GRANT CONNECT ON DATABASE postgres TO isb_client;
```

## 3. Create Schemas
*"System" Schema:*
```sql
CREATE SCHEMA system
    AUTHORIZATION isb_admin;

GRANT USAGE ON SCHEMA system TO isb_client;

ALTER DEFAULT PRIVILEGES IN SCHEMA system
GRANT SELECT ON TABLES TO isb_client;

ALTER DEFAULT PRIVILEGES IN SCHEMA system
GRANT USAGE ON SEQUENCES TO isb_client;
```

*"Shared" Schema:*
```sql
CREATE SCHEMA shared
    AUTHORIZATION isb_admin;

GRANT USAGE ON SCHEMA shared TO isb_client;

ALTER DEFAULT PRIVILEGES IN SCHEMA shared
GRANT INSERT, SELECT, UPDATE ON TABLES TO isb_client;

ALTER DEFAULT PRIVILEGES IN SCHEMA shared
GRANT USAGE ON SEQUENCES TO isb_client;
```

*"Workspace" Schema:*
```sql
CREATE SCHEMA <name>
    AUTHORIZATION isb_admin;

GRANT CREATE, USAGE ON SCHEMA <name> TO isb_client;

ALTER DEFAULT PRIVILEGES IN SCHEMA <name>
GRANT INSERT, SELECT, UPDATE ON TABLES TO isb_client;

ALTER DEFAULT PRIVILEGES IN SCHEMA <name>
GRANT USAGE, UPDATE ON SEQUENCES TO isb_client;
```

## 4. Create Tables

### 4.1. Instances
```sql
CREATE TABLE IF NOT EXISTS system.instances
(
    instance_id serial NOT NULL,
    display_name character varying(50) NOT NULL,
    url text NOT NULL,
    phone_number character varying(50) NOT NULL,
    is_development boolean NOT NULL,
    modified_at timestamp without time zone,
    created_at timestamp without time zone NOT NULL,
    modified_by character varying(50),
    created_by character varying(50) NOT NULL,
    PRIMARY KEY (instance_id),
    CONSTRAINT instances_display_name_unique UNIQUE (display_name)
);

ALTER TABLE system.instances
    OWNER to isb_admin;
```

### 4.2. Organizations
```sql
CREATE TABLE IF NOT EXISTS system.organizations
(
    organization_id serial NOT NULL,
    display_name character varying(250) NOT NULL,
    street character varying(250) NOT NULL,
    house_number character varying(20) NOT NULL,
    postal_code character varying(50) NOT NULL,
    city character varying(250) NOT NULL,
    country character varying(250) NOT NULL,
    state character varying(250) NOT NULL,
    modified_at timestamp without time zone,
    created_at timestamp without time zone NOT NULL,
    modified_by character varying(50),
    created_by character varying(50) NOT NULL,
    PRIMARY KEY (organization_id),
    CONSTRAINT organizations_display_name UNIQUE (display_name)
);

ALTER TABLE system.organizations
    OWNER to isb_admin;
```

### 4.3. Workspaces
```sql
CREATE TABLE IF NOT EXISTS system.workspaces
(
    workspace_id serial NOT NULL,
    display_name character varying(100) NOT NULL,
    organization_id integer,
    db_schema character varying(50) NOT NULL,
    active boolean NOT NULL,
    instance_id integer NOT NULL,
    timeout_in_minutes integer NOT NULL,
    timeout_enabled boolean NOT NULL,
    workspace_token character varying(36) NOT NULL DEFAULT uuid_in((md5(((random())::text || (random())::text)))::cstring),
    workspace_asset_key character varying(36) NOT NULL DEFAULT uuid_in((md5(((random())::text || (random())::text)))::cstring),
    modified_at timestamp without time zone,
    created_at timestamp without time zone NOT NULL,
    modified_by character varying(50),
    created_by character varying(50) NOT NULL,
    CONSTRAINT workspace_pkey PRIMARY KEY (workspace_id),
    CONSTRAINT workspaces_db_schema UNIQUE (db_schema),
    CONSTRAINT workspaces_display_name_by_organization UNIQUE (organization_id, display_name),
    CONSTRAINT workspaces_workspace_asset_key UNIQUE (workspace_asset_key),
    CONSTRAINT workspaces_workspace_token UNIQUE (workspace_token),
	CONSTRAINT workspace_instance FOREIGN KEY (instance_id)
        REFERENCES system.instances (instance_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT workspace_organization FOREIGN KEY (organization_id)
        REFERENCES system.organizations (organization_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
);

ALTER TABLE system.workspaces
    OWNER to isb_admin;

CREATE INDEX workspaces_instance_id_index 
	  ON system.workspaces (instance_id);
```

### 4.4. Managers
```sql
CREATE TABLE IF NOT EXISTS system.managers
(
    username character varying(250) NOT NULL,
    prefix character varying(20),
    firstname character varying(250),
    lastname character varying(250),
    organization_id integer NOT NULL,
    password_hash text NOT NULL,
    is_registered boolean NOT NULL DEFAULT false,
    active boolean NOT NULL DEFAULT true,
    last_login timestamp without time zone,
    modified_at timestamp without time zone,
    created_at timestamp without time zone NOT NULL,
    modified_by character varying(50),
    created_by character varying(50) NOT NULL,
    PRIMARY KEY (username),
    CONSTRAINT manager_organisation FOREIGN KEY (organization_id)
        REFERENCES system.organizations (organization_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
);

ALTER TABLE system.managers
    OWNER to isb_admin;
```

### 4.5. Translations
```sql
CREATE TABLE IF NOT EXISTS system.translations
(
    translation_id serial NOT NULL,
    workspace_id integer NOT NULL,
    translations jsonb NOT NULL,
    active boolean NOT NULL,
    modified_at timestamp without time zone,
    created_at timestamp without time zone NOT NULL,
    modified_by character varying(50),
    created_by character varying(50) NOT NULL,
    PRIMARY KEY (translation_id),
    CONSTRAINT translation_workspace FOREIGN KEY (workspace_id)
        REFERENCES system.workspaces (workspace_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
);

ALTER TABLE system.translations
    OWNER to isb_admin;

CREATE INDEX translations_workspace_id_index 
	  ON system.translations (workspace_id);
```

### 4.6. Data_Types
```sql
CREATE TABLE IF NOT EXISTS system.data_types
(
    data_type character varying(20) NOT NULL,
    created_at timestamp without time zone NOT NULL,
    created_by character varying(50) NOT NULL,
    PRIMARY KEY (data_type)
);

ALTER TABLE system.data_types
    OWNER to isb_admin;
```

### 4.7. Custom_Fields
```sql
CREATE TABLE IF NOT EXISTS system.custom_fields
(
    field_id serial NOT NULL,
    workspace_id integer NOT NULL,
    display_name character varying(50) NOT NULL,
    data_type character varying(20) NOT NULL,
    modified_at timestamp without time zone,
    created_at timestamp without time zone NOT NULL,
    modified_by character varying(50),
    created_by character varying(50) NOT NULL,
    PRIMARY KEY (field_id),
    CONSTRAINT custom_fields_display_name_by_workspace UNIQUE (workspace_id, display_name),
    CONSTRAINT custom_field_workspace FOREIGN KEY (workspace_id)
        REFERENCES system.workspaces (workspace_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT custom_field_type FOREIGN KEY (data_type)
        REFERENCES system.data_types (data_type) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
);

ALTER TABLE system.custom_fields
    OWNER to isb_admin;

CREATE INDEX custom_fields_workspace_id_index 
	  ON system.custom_fields (workspace_id);

CREATE INDEX custom_fields_data_type_index 
	  ON system.custom_fields (data_type);
```

### 4.8. Languages
```sql
CREATE TABLE IF NOT EXISTS system.languages
(
    shortcode character varying(20) NOT NULL,
    display_name character varying(50) NOT NULL,
    command character varying(50) NOT NULL,
    created_at timestamp without time zone NOT NULL,
    created_by character varying(50) NOT NULL,
    PRIMARY KEY (shortcode)
);

ALTER TABLE system.languages
    OWNER to isb_admin;
```

### 4.9. Commands
```sql
CREATE TABLE IF NOT EXISTS system.commands
(
    command_type character varying(20) NOT NULL,
    created_at timestamp without time zone NOT NULL,
    created_by character varying(50) NOT NULL,
    PRIMARY KEY (command_type)
);

ALTER TABLE system.commands
    OWNER to isb_admin;
```

### 4.10. Mandatory_Translations
```sql
CREATE TABLE IF NOT EXISTS system.mandatory_translations
(
    translation_key character varying(50) NOT NULL,
    display_name character varying(50) NOT NULL,
    created_at timestamp without time zone NOT NULL,
    created_by character varying(50) NOT NULL,
    PRIMARY KEY (translation_key)
);

ALTER TABLE system.mandatory_translations
    OWNER to isb_admin;
```

### 4.11. Webhooks
```sql
CREATE TABLE IF NOT EXISTS system.webhooks
(
    webhook_id serial NOT NULL,
    workspace_id integer NOT NULL,
    display_name character varying NOT NULL,
    url text NOT NULL,
    include_token boolean NOT NULL,
    modified_at timestamp without time zone,
    created_at timestamp without time zone NOT NULL,
    modified_by character varying(50),
    created_by character varying(50) NOT NULL,
    PRIMARY KEY (webhook_id),
    CONSTRAINT workspace_display_name_unique UNIQUE (display_name, workspace_id),
    CONSTRAINT webhook_workspace FOREIGN KEY (workspace_id)
        REFERENCES system.workspaces (workspace_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
);

ALTER TABLE system.webhooks
    OWNER to isb_admin;

CREATE INDEX webhooks_workspace_id_index 
	  ON system.webhooks (workspace_id);
```

### 4.12. Enabled_Languages
```sql
CREATE TABLE IF NOT EXISTS system.enabled_languages
(
    workspace_id integer NOT NULL,
    shortcode character varying(20) NOT NULL,
    is_default boolean NOT NULL,
    created_at timestamp without time zone NOT NULL,
    created_by character varying(50) NOT NULL,
    PRIMARY KEY (workspace_id, shortcode),
    CONSTRAINT enabled_languages_workspace FOREIGN KEY (workspace_id)
        REFERENCES system.workspaces (workspace_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT enabled_languages_language FOREIGN KEY (shortcode)
        REFERENCES system.languages (shortcode) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
);

ALTER TABLE system.enabled_languages
    OWNER to isb_admin;
```

### 4.13. Command_Aliases
```sql
CREATE TABLE IF NOT EXISTS system.command_aliases
(
    alias_id serial NOT NULL,
    workspace_id integer NOT NULL,
    command_type character varying(20) NOT NULL,
    alias character varying(50) NOT NULL,
    is_static boolean NOT NULL,
    created_at timestamp without time zone NOT NULL,
    created_by character varying(50) NOT NULL,
    PRIMARY KEY (alias_id),
    CONSTRAINT command_aliases_alias_by_workspace UNIQUE (workspace_id, alias),
    CONSTRAINT command_alias_workspace FOREIGN KEY (workspace_id)
        REFERENCES system.workspaces (workspace_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT command_alias_command FOREIGN KEY (command_type)
        REFERENCES system.commands (command_type) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
);

ALTER TABLE system.command_aliases
    OWNER to isb_admin;

CREATE INDEX command_aliases_workspace_id_index
    ON system.command_aliases (workspace_id);

CREATE INDEX command_aliases_command_type_index
    ON system.command_aliases (command_type);
```

### 4.14. Participants
```sql
CREATE TABLE IF NOT EXISTS shared.participants
(
    participant_id serial NOT NULL,
    identifier character varying(50),
    prefix character varying(50),
    firstname character varying(250),
    lastname character varying(250),
    is_registered boolean NOT NULL,
    description text,
    verification_code character varying(50),
    active boolean NOT NULL,
    language character varying(20),
    custom_fields jsonb NOT NULL,
    modified_at timestamp without time zone,
    created_at timestamp without time zone NOT NULL,
    modified_by character varying(50),
    created_by character varying(50) NOT NULL,
    PRIMARY KEY (participant_id),
    CONSTRAINT verification_code_unique UNIQUE (verification_code)
);

ALTER TABLE shared.participants
    OWNER to isb_admin;

CREATE INDEX participants_identifier_index
    ON shared.participants (identifier);

ALTER TABLE shared.participants ENABLE ROW LEVEL SECURITY;

CREATE POLICY isb_client_participants ON shared.participants
TO isb_client
USING (True);
```

### 4.15. Groups
```sql
CREATE TABLE IF NOT EXISTS shared.groups
(
    group_id serial NOT NULL,
    name character varying(50) NOT NULL,
    display_name character varying(50) NOT NULL,
    workspace_id integer NOT NULL,
    modified_at timestamp without time zone,
    created_at timestamp without time zone NOT NULL,
    modified_by character varying(50),
    created_by character varying(50) NOT NULL,
    PRIMARY KEY (group_id),
    CONSTRAINT groups_names_by_workspace UNIQUE (name, workspace_id, display_name),
    CONSTRAINT group_workspace FOREIGN KEY (workspace_id)
        REFERENCES system.workspaces (workspace_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
);

ALTER TABLE shared.groups
    OWNER to isb_admin;

CREATE INDEX groups_workspace_id_index
    ON shared.groups (workspace_id);

ALTER TABLE shared.groups ENABLE ROW LEVEL SECURITY;

CREATE POLICY isb_client_groups ON shared.groups
TO isb_client
USING (True);
```

### 4.16. Group_Allocation
```sql
CREATE TABLE IF NOT EXISTS shared.group_allocation
(
    group_id integer NOT NULL,
    participant_id integer NOT NULL,
    created_at timestamp without time zone NOT NULL,
    created_by character varying(50) NOT NULL,
    PRIMARY KEY (group_id, participant_id),
    CONSTRAINT group_allocation_participant FOREIGN KEY (participant_id)
        REFERENCES shared.participants (participant_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT group_allocation_group FOREIGN KEY (group_id)
        REFERENCES shared.groups (group_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
);

ALTER TABLE shared.group_allocation
    OWNER to isb_admin;

ALTER TABLE shared.group_allocation ENABLE ROW LEVEL SECURITY;

CREATE POLICY isb_client_group_allocation ON shared.group_allocation
TO isb_client
USING (True);
```

### 4.17. Sessions
```sql
CREATE TABLE IF NOT EXISTS system.sessions
(
    session_uuid character varying(50) NOT NULL,
    logs jsonb NOT NULL,
    created_at timestamp without time zone NOT NULL,
    created_by character varying(50) NOT NULL,
    PRIMARY KEY (session_uuid)
);

ALTER TABLE system.sessions
    OWNER to isb_admin;

GRANT INSERT ON system.sessions TO isb_client;
```

### 4.18. Surveys
```sql
CREATE TABLE IF NOT EXISTS system.surveys
(
    survey_id serial NOT NULL,
    display_name character varying(100) NOT NULL,
    is_registration boolean NOT NULL,
    survey jsonb NOT NULL,
    invalid boolean NOT NULL,
    active boolean NOT NULL,
    modified_at timestamp without time zone,
    created_at timestamp without time zone NOT NULL,
    modified_by character varying(50),
    created_by character varying(50) NOT NULL,
    CONSTRAINT surveys_pkey PRIMARY KEY (survey_id)
);

ALTER TABLE system.surveys
    OWNER to isb_admin;
```

### 4.19. Survey_Visibility
```sql
CREATE TABLE IF NOT EXISTS system.survey_visibility
(
    group_id integer NOT NULL,
    survey_id integer NOT NULL,
    created_at timestamp without time zone NOT NULL,
    created_by character varying(50) NOT NULL,
    CONSTRAINT survey_visibility_pkey PRIMARY KEY (group_id, survey_id),
    CONSTRAINT survey_visibility_group FOREIGN KEY (group_id)
        REFERENCES shared.groups (group_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT survey_visibility_survey FOREIGN KEY (survey_id)
        REFERENCES system.surveys (survey_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
);

ALTER TABLE system.survey_visibility
    OWNER to isb_admin;
```

### 4.20. Group_Invitations
```sql
CREATE TABLE IF NOT EXISTS system.group_invitations
(
    invitation_token character varying(36) NOT NULL,
    group_id integer NOT NULL,
    expires timestamp without time zone,
    modified_at timestamp without time zone,
    created_at timestamp without time zone NOT NULL,
    modified_by character varying(50),
    created_by character varying(50) NOT NULL,
    CONSTRAINT group_invitations_pkey PRIMARY KEY (invitation_token),
		CONSTRAINT group_invitations_group FOREIGN KEY (group_id)
        REFERENCES shared.groups (group_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
);

ALTER TABLE system.group_invitations
    OWNER to isb_admin;

CREATE INDEX group_invitations_group_id_index 
	ON system.group_invitations (group_id);
```

## 5. Content of Static Tables / Data

>These can be adaped to whatever needs you have.

### 5.1. Mandatory_Translations
```sql
INSERT INTO system.mandatory_translations(
	display_name, translation_key, created_at, created_by)
	VALUES ('session-ended', 'SESSION_ENDED', current_timestamp, 'admin'),
	('no-help-available', 'NO_HELP_AVAILABLE', current_timestamp, 'admin'),
	('back-not-available', 'BACK_NOT_AVAILABLE', current_timestamp, 'admin'),
	('selection-not-available', 'SELECTION_NOT_AVAILABLE', current_timestamp, 'admin'),
	('invalid-input-text-100', 'INVALID_INPUT_TEXT_100', current_timestamp, 'admin'),
	('invalid-input-integer', 'INVALID_INPUT_INTEGER', current_timestamp, 'admin'),
	('invalid-input-decimal', 'INVALID_INPUT_DECIMAL', current_timestamp, 'admin'),
	('invalid-input-number', 'INVALID_INPUT_NUMBER', current_timestamp, 'admin'),
	('invalid-input-location', 'INVALID_INPUT_LOCATION', current_timestamp, 'admin'),
	('invalid-input-picture', 'INVALID_INPUT_PICTURE', current_timestamp, 'admin'),
	('invalid-input-audio', 'INVALID_INPUT_AUDIO', current_timestamp, 'admin'),
	('invalid-input-video', 'INVALID_INPUT_VIDEO', current_timestamp, 'admin'),
	('invalid-input-document', 'INVALID_INPUT_DOCUMENT', current_timestamp, 'admin'),
	('invalid-input-date', 'INVALID_INPUT_DATE', current_timestamp, 'admin'),
	('invalid-input-time', 'INVALID_INPUT_TIME', current_timestamp, 'admin'),
	('invalid-input-time12', 'INVALID_INPUT_TIME12', current_timestamp, 'admin'),
	('invalid-input-time24', 'INVALID_INPUT_TIME24', current_timestamp, 'admin'),
	('invalid-input-url', 'INVALID_INPUT_URL', current_timestamp, 'admin'),
	('invalid-input-phone-number', 'INVALID_INPUT_PHONE_NUMBER', current_timestamp, 'admin'),
	('survey-selection', 'SURVEY_SELECTION', current_timestamp, 'admin');
```

### 5.2. Languages
```sql
INSERT INTO system.languages(
	shortcode, display_name, command, created_at, created_by)
	VALUES 
    ('en', 'English', '/english', current_timestamp, 'admin'),
    ('fr', 'French', '/french', current_timestamp, 'admin'),
    ('sw', 'Swahili', '/swahili', current_timestamp, 'admin');
```

### 5.3. Commands
```sql
INSERT INTO system.commands(
	command_type, created_at, created_by)
	VALUES ('start', current_timestamp, 'admin'),
	('end', current_timestamp, 'admin'),
	('help', current_timestamp, 'admin'),
	('back', current_timestamp, 'admin'),
	('skip', current_timestamp, 'admin');
```

### 5.4. Data_Types
```sql
INSERT INTO system.data_types(
	data_type, created_at, created_by)
	VALUES ('text-100', current_timestamp, 'admin'),
	('text-long', current_timestamp, 'admin'),
	('integer', current_timestamp, 'admin'),
	('decimal', current_timestamp, 'admin'),
	('number', current_timestamp, 'admin'),
	('location', current_timestamp, 'admin'),
	('picture', current_timestamp, 'admin'),
	('audio', current_timestamp, 'admin'),
	('video', current_timestamp, 'admin'),
	('document', current_timestamp, 'admin'),
	('date', current_timestamp, 'admin'),
	('time', current_timestamp, 'admin'),
	('time-12', current_timestamp, 'admin'),
	('time-24', current_timestamp, 'admin'),
	('url', current_timestamp, 'admin'),
	('phonenumber', current_timestamp, 'admin');
```

## 6. Content of Other Tables

> The following queries should be only seen as templates. Please fill out with care! Only the order of the query matters due to table constraints.

### 6.1. Organizations
```sql
INSERT INTO system.organizations(
	display_name, street, house_number, postal_code, city, country, state, modified_at, created_at, modified_by, created_by)
	VALUES ('', '', '', '', '', '', '', current_timestamp, current_timestamp, 'admin', 'admin');
```

### 6.2. Instances
```sql
INSERT INTO system.instances(
	display_name, url, phone_number, is_development, modified_at, created_at, modified_by, created_by)
	VALUES ('', 'https://', '+123456789', false, current_timestamp, current_timestamp, 'admin', 'admin');
```

### 6.3. Workspaces
```sql
INSERT INTO system.workspaces(
	display_name, organization_id, db_schema, active, instance_id, timeout_in_minutes, timeout_enabled, modified_at, created_at, modified_by, created_by)
	VALUES ('', 0, '', true, 0, 0, true, current_timestamp, current_timestamp, 'admin', 'admin');
```

### 6.4. Managers
```sql
INSERT INTO system.managers(
	prefix, firstname, lastname, organization_id, password_hash, last_login, modified_at, created_at, modified_by, created_by)
	VALUES ('', '', '', 0, '', current_timestamp, current_timestamp, current_timestamp, 'admin', 'admin');
```

### 6.5. Webhooks
```sql
INSERT INTO system.webhooks(
	workspace_id, display_name, url, include_token, modified_at, created_at, modified_by, created_by)
	VALUES (0, '', '', false, current_timestamp, current_timestamp, 'admin', 'admin');
```

### 6.6. Enabled_Languages
```sql
INSERT INTO system.enabled_languages(
	workspace_id, shortcode, is_default, created_at, created_by)
	VALUES (1, 'en', true, current_timestamp, 'admin'),
	(1, 'fr', false, current_timestamp, 'admin'),
	(1, 'sw', false, current_timestamp, 'admin');
```

### 6.7. Command_Aliases
```sql
INSERT INTO system.command_aliases(
	workspace_id, command_type, alias, is_static, created_at, created_by)
	VALUES (1, 'start', '/start', true, current_timestamp, 'admin'),
	(1, 'end', '/end', true, current_timestamp, 'admin'),
	(1, 'help', '/help', true, current_timestamp, 'admin'),
	(1, 'back', '/back', true, current_timestamp, 'admin'),
	(1, 'skip', '/skip', true, current_timestamp, 'admin');
```

### 6.8. Groups
*"Default" Group:*
```sql
INSERT INTO shared.groups(
	name, display_name, workspace_id, modified_at, created_at, modified_by, created_by)
	VALUES ('default', 'Default', 1, current_timestamp, current_timestamp, 'admin', 'admin');
```

*Custom Group:*
```sql
INSERT INTO shared.groups(
	name, display_name, workspace_id, modified_at, created_at, modified_by, created_by)
	VALUES ('', '', 0, current_timestamp, current_timestamp, 'admin', 'admin');
```

### 6.9. Participants
```sql
INSERT INTO shared.participants(
	identifier, prefix, firstname, lastname, is_registered, description, verification_code, active, language, custom_fields, modified_at, created_at, modified_by, created_by)
	VALUES ('', '', '', '', false, '', '', true, '', '{}', current_timestamp, current_timestamp, 'admin', 'admin');
```

### 6.10. Group_Allocation
```sql
INSERT INTO shared.group_allocation(
	group_id, participant_id, created_at, created_by)
	VALUES (0, 0, current_timestamp, 'admin');
```

### 6.11. Translations
```sql
INSERT INTO system.translations(
	workspace_id, translations, active, modified_at, created_at, modified_by, created_by)
	VALUES (0, '{}', true, current_timestamp, current_timestamp, 'admin', 'admin');
```

### 6.12. Surveys
*"Registration" Survey:*
```sql
INSERT INTO system.surveys(
	display_name, is_registration, survey, invalid, active, modified_at, created_at, modified_by, created_by)
	VALUES
	('registration', true, '{}', false, true, current_timestamp, current_timestamp, 'admin', 'admin');
```

*Custom Survey:*
```sql
INSERT INTO system.surveys(
	display_name, is_registration, survey, invalid, active, modified_at, created_at, modified_by, created_by)
	VALUES
	('', false, '{}', false, true, current_timestamp, current_timestamp, 'admin', 'admin');
```

### 6.12. Survey_Visability
*"Registration" Survey:*
```sql
INSERT INTO system.survey_visibility(
	group_id, survey_id, created_at, created_by)
	VALUES (0, 1, current_timestamp, current_timestamp);
```

*Custom Survey:*
```sql
INSERT INTO system.survey_visibility(
	group_id, survey_id, created_at, created_by)
	VALUES (0, 2, current_timestamp, current_timestamp);
```

### 6.13. Group_Invitations
```sql
INSERT INTO system.group_invitations(
	invitation_token, group_id, expires, modified_at, created_at, modified_by, created_by)
	VALUES (uuid_in(md5(random()::text || random()::text)::cstring), 0, '2025-12-31 23:59:59', current_timestamp, current_timestamp, 'admin', 'admin');
```


## 7. Create Workspace Role
### 7.1 Create Role
```sql
CREATE ROLE <role_name> WITH
	LOGIN
	NOSUPERUSER
	NOCREATEDB
	NOCREATEROLE
	INHERIT
	NOREPLICATION
	CONNECTION LIMIT -1
	PASSWORD '<password>';

GRANT CONNECT ON DATABASE isb TO <role_name>;
GRANT CONNECT ON DATABASE postgres TO <role_name>;
```

### 7.2 Grant Permissions for Workspace Schema
```sql
GRANT USAGE ON SCHEMA <schema_name> TO <role_name>;

ALTER DEFAULT PRIVILEGES IN SCHEMA <schema_name>
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLES TO <role_name>;

ALTER DEFAULT PRIVILEGES IN SCHEMA <schema_name>
GRANT USAGE ON SEQUENCES TO <role_name>;
```

### 7.3 Grant Permissions for Shared Schema
```sql
GRANT USAGE ON SCHEMA shared TO <role_name>;

GRANT SELECT ON TABLE shared.participants TO <role_name>;
GRANT SELECT ON TABLE shared.group_allocation TO <role_name>;
GRANT SELECT ON TABLE shared.groups TO <role_name>;
```

### 7.4 Create RLS Policy on Groups Table
```sql
CREATE POLICY <role_name>_groups ON shared.groups
TO <role_name>
USING (
    workspace_id = <workspace_id>
);
```

### 7.5 Create RLS Policy on Group_Allocation Table
```sql
CREATE POLICY <role_name>_group_allocation ON shared.group_allocation
TO <role_name>
USING (
	EXISTS (
		SELECT 1 FROM shared.groups
		WHERE shared.groups.group_id = shared.group_allocation.group_id
	)
);
```

### 7.6 Create RLS Policy on Participants Table
```sql
CREATE POLICY <role_name>_participants ON shared.participants
TO <role_name>
USING (
	EXISTS (
		SELECT 1 FROM shared.group_allocation
		WHERE shared.group_allocation.participant_id = shared.participants.participant_id
	)
);
```