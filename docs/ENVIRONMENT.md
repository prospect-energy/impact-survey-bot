# Environmental Variables <!-- omit in toc -->

The Impact-Survey-Bot instance requires multiple environmental variables which are briefly described in this document. If you want to use an `.env-file` to set them locally, you can copy and rename the `.env.template` file in the root-directory of this project.

## Contents <!-- omit in toc -->

- [Variables for Instance](#variables-for-instance)
- [Variables for CI/CD](#variables-for-cicd)

## Variables for Instance

- `TWILIO_ENDPOINTS_ENABLED`
  - Optional variable to enable the Twilio webhook endpoints. Set to `True` to enable.
- `TWILIO_AUTH_ENABLED`
   - Optional variable to determine if the signature of Twilio requests should be validated. Set to `True` to enable.
- `TWILIO_AUTH_TOKEN`
   - Token to verify Twilio requests. Only necessary for full deployment.
- `INSTANCE_ID`
   - Id of the ISB instance.
- `REGISTRATION_SURVEY`
   - Name of the registration survey. Should be `registration` unless needed otherwise.
- `API_TOKEN`
   - The API-Token for securing the "/update"-Route.
- `DB_ADDRESS`
   - Address of the production/development database server.
- `DB_PORT`
   - Port of the production/development database server.
- `DB_NAME`
   - Name of the production/development database (e.g. `main`).
- `DB_USER`
   - Client-User for the production/development database (e.g. `isb_client`).
- `DB_PASSWORD`
   - Client-Password for the production/development database.
- `DB_TESTING_ADDRESS`
   - Address of the testing database server.
- `DB_TESTING_PORT`
   - Port of the testing database server.
- `DB_TESTING_USER`
   - Client-User for the testing database (e.g. `isb_client`).
- `DB_TESTING_PASSWORD`
   - Client-Password for the testing database.
- `DB_TEST_SETUP_USER`
   - Postgres-User that can create databases on the testing server.
- `DB_TEST_SETUP_PASSWORD`
   - Password of Postgres-User that can create databases on the testing server.
- `DB_SYSTEM_SCHEMA`
   - Name of the "system" schema (e.g. `system`).
- `DB_SHARED_SCHEMA`
   - Name of the "shared" schema (e.g. `shared`).
- `S3_ENABLED`
   - Is the file upload to AWS S3 enabled? (accepts `true` and `false`)
- `S3_ACCESS_KEY`
   - Access-Key for the AWS User with S3 read/write-access.
- `S3_SECRET_ACCESS_KEY`
   - Secret-Access-Key for the AWS User with S3 read/write-access.
- `S3_ENDPOINT`
   - The S3 endpoint (AWS S3 or MinIO). SSL/TLS connection required. For AWS S3 this should be `https://s3.<region>.amazonaws.com`.
- `S3_BUCKET`
   - The S3 Bucket used in the production/development environment for file-uploads.
- `S3_TESTING_BUCKET`
   - The S3 Bucket used in the testing environment for file-uploads.
- `DOWNLOAD_PATH`
   - Local directory where files should be saved temporarily during file-uploads (e.g. `download`).
- `WAB_ENDPOINTS_ENABLED`
  - Optional variable to enable the Whatsapp Business API webhook endpoints. Set to `True` to enable.
- `WAB_AUTH_ENABLED`
   - Optional variable to determine if the signature of whatsapp business requests should be validated. Set to `True` to enable.
- `WAB_APP_SECRET`
   - App Secret of Facebook App.
- `WAB_API_TOKEN`
   - API Token for WhatsApp Business API.
- `WAB_PHONE_NUMBER_ID`
   - Phone Number ID of the phone number this is connected to the WhatsApp Business API.
- `WAB_VERIFY_TOKEN`
   - Token to verify webhook for WhatsApp Business API. Only necessary for full deployment.
- `TELEGRAM_ENDPOINTS_ENABLED`
  - Optional variable to enable the Telegram webhook endpoints. Set to `True` to enable.
- `TELEGRAM_AUTH_ENABLED`
   - Optional variable to determine if the signature of telegram requests should be validated. Set to `True` to enable.
- `TELEGRAM_SECRET_TOKEN`
   - Secret Token for Telegram API (set on bot creation).
- `TELEGRAM_API_TOKEN`
   - API/Bot Token for Telegram API.
- `TIMEOUT_INTERVAL`
   - The interval in minutes to check for timeouts (e.g. `5`).
- `LOG_LEVEL`
   - The log-level of the ISB instance (accepts `fatal`, `error`, `info`, `debug`).

## Variables for CI/CD

> These variables are only required for the remote repository (GitLab/GitHub).

- `AWS_EB_ACCESS_KEY`
   - Access-Key for the AWS User with permissions to deploy to Elastic Beanstalk.
- `AWS_EB_SECRET_ACCESS_KEY`
   - Secret-Access-Key for the AWS User with permissions to deploy to Elastic Beanstalk.
- `AWS_EB_ENVIRONMENT_DEV`
   - Name of the Elastic Beanstalk environment for the `development` branch.
- `AWS_EB_ENVIRONMENT_MAIN`
   - Name of the Elastic Beanstalk environment for the `main` branch.