# SaaS-Platform

This document contains preliminary planning for the frontend and backend of the SaaS-Platform. The goal of this platform is to provide an easy-to-use web interface for creating and managing chatbot surveys. The platform-user should not be exposed to the infrastructure anymore and by adding a login-system, more granular permissions should be possible.
An overview of the structure can be found in the figure below:

![SaaS-Platform Structure](assets/SaaS-Platform.png)
(outdated)

---

## Backend

The purpose of the backend mainly is to connect the frontend with the database and handle workspace creation as well as updates.

## Routes:

>General Structure of Routes
>
>→ some routes might become split up to allow some more specific frontend functionality

- __Workspace__
    - `GET /workspace/{workspace_id}`
        - contents of dashboard
        - name/display-name
        - survey list
    - `GET /workspace/{workspace_id}/config`
        - get workspace-config
            - timeout
            - commands
            - enabled languages
            - webhooks
            - db-access
            - identifier
    - `POST /workspace/{workspace_id}/update`
        - update workspace config
        - → save changes
        - trigger isb-update
    - `POST /workspace/create`
        - create new workspace
        - run sql queries to set defaults
        - trigger isb-update
- __Survey__
    - `GET /survey/{survey_id}`
        - survey json
        - further processed data
    - `POST /survey/{survey_id}/update`
        - convert survey
        - save to database
        - trigger isb-update
    - `POST /survey/create`
        - create new survey entry
- __User__
    - `GET /user/{user_id}`
        - user data for settings
    - `GET /user/{user_id}/workspaces`
        - all workspaces of a user
        - list (id, name, display_name)
    - `POST /user/{user_id}/update`
        - update user data in database
    - `DELETE /user/{user_id}`
        - deactivate user in database
        - make anonymous?
        - deletion period?
- __Participants__
    - `GET /participants/{workspace_id}`
        - all participants of workspace
        - data for overview table
    - `POST /participants/{workspace_id}/add`
        - add / invite participant
        - might need two routes
        - add to database
    - `POST /participants/{workspace_id}/update`
        - update participant data
        - update changes in database
- __Groups__
    - `GET /groups/{workspace_id}`
        - get all groups of workspace
    - `POST /groups/{workspace_id}/create`
        - create new group
        - update database
    - `POST /groups/{workspace_id}/update`
        - update existing group in database
- __Translations__
    - `GET /translations/{workspace_id}`
        - get translation json for workspace
    - `POST /translations/{workspace_id}/update`
        - update translation json
        - save to database
        - trigger isb-update
- __Login__
    - `POST /login`
    - `POST /register`

---

## Frontend

Demonstration of the Survey-Editor: https://isb-survey-editor-prototype.netlify.app/

*Demo-Repository: https://github.com/m-lukas/isb-survey-editor-prototype*

__Wireframe of Dashboard:__
![Wireframe Dashboard](assets/Wireframe_Dashboard.png)

__Breadboard of Frontend:__
![Frontend Breadboard](assets/Frontend_Breadboard.png)