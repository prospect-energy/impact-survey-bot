"""Contains TransactionContext class."""
from __future__ import annotations

from logging import Logger
from typing import TYPE_CHECKING, Any, Optional, Union

if TYPE_CHECKING:
    from database.database import Postgres

from database.components import FetchPolicy
from database.models import ComponentQuery, PlainQuery
from database.query_interface import QueryInterface


class TransactionContext(QueryInterface):
    """Class to transport variables for query execution and provide execution interface in transactions."""

    def __init__(
        self, db: Postgres, logger: Logger, connection: Any, cursor: Any
    ) -> None:
        super().__init__()

        self.db = db
        self.logger = logger
        self.connection = connection
        self.cursor = cursor

    def execute(
        self,
        query: Union[ComponentQuery, PlainQuery],
        fetch: Optional[FetchPolicy] = None,
    ) -> Optional[Any]:
        """Executes a sql query as part of transaction."""
        return self._QueryInterface__execute(
            self.logger, self.connection, self.cursor, query, fetch
        )
