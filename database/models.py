"""Contains query wrapper classes and construction."""
from typing import Any, Dict, List, Optional, Union

from psycopg2 import sql

from database.components import (
    ArrayAggregate,
    ArrayColumn,
    Column,
    GroupBy,
    JoinTable,
    OrderBy,
    QueryColumn,
    Table,
    WhereClause,
)


class PlainQuery:
    def __init__(self, query_str: str, *args, **kwargs) -> None:
        self.query = sql.SQL(query_str).format(*args, **kwargs)

    def get_sql_str(self) -> sql.SQL:
        """Constructs and return sql object of query."""
        return self.query


class ComponentQuery:
    """Class containing shared attributes and functions for component queries."""

    def __init__(self, table: Table) -> None:
        self.table = table

    def get_sql_str(self) -> sql.SQL:
        """Constructs and return sql object of query."""
        pass


class SelectQuery(ComponentQuery):
    """Class to construct SELECT query."""

    def __init__(
        self,
        table: Table,
        all_columns: bool,
        columns: Optional[
            List[Union[str, Column, ArrayAggregate, ArrayColumn, QueryColumn]]
        ] = [],
        joined_tables: Optional[List[JoinTable]] = [],
        where_clauses: Optional[List[WhereClause]] = [],
        order_by: Optional[OrderBy] = None,
        group_by: Optional[GroupBy] = None,
    ) -> None:
        super().__init__(table=table)

        self.all_columns = all_columns
        self.columns = columns
        self.joined_tables = joined_tables
        self.where_clauses = where_clauses
        self.order_by = order_by
        self.group_by = group_by

    def __get_column_identifier(
        self, column: Union[str, Column, ArrayAggregate, QueryColumn]
    ) -> sql.SQL:
        if isinstance(column, str):
            return Column(self.table, column).get_db_identifier()

        return column.get_db_identifier()

    def add_where_clause(self, where_clause: WhereClause) -> None:
        """Adds where clause to query."""
        self.where_clauses.append(where_clause)

    def get_sql_str(self) -> sql.SQL:
        """Constructs and return sql object of query."""

        column_str = None

        if self.all_columns:
            column_str = sql.SQL("*")
        else:
            column_str = sql.SQL(",").join(
                [self.__get_column_identifier(c) for c in self.columns]
            )

        sql_str = sql.SQL("SELECT {columns} FROM {table}").format(
            columns=column_str, table=self.table.get_db_identifier()
        )

        join_strings = (
            [jt.get_sql_str() for jt in self.joined_tables]
            if isinstance(self.joined_tables, list)
            else []
        )
        for join_str in join_strings:
            sql_str = sql.SQL(" ").join([sql_str, join_str])

        if self.where_clauses:
            where_condition = sql.SQL(" AND ").join(
                [c.get_sql_str() for c in self.where_clauses]
            )
            where_clause_str = sql.SQL(" ").join([sql.SQL("WHERE"), where_condition])

            sql_str = sql.SQL(" ").join([sql_str, where_clause_str])

        if self.order_by:
            sql_str = sql.SQL(" ").join([sql_str, self.order_by.get_sql_str()])

        if self.group_by:
            sql_str = sql.SQL(" ").join([sql_str, self.group_by.get_sql_str()])

        return sql_str


class InsertQuery(ComponentQuery):
    """Class to construct INSERT query."""

    def __init__(
        self,
        table: Table,
        data: List[Dict[str, Any]],
        error_on_conflict: bool = True,
        return_column: Optional[str] = None,
    ) -> None:
        super().__init__(table=table)

        self.data = data
        self.error_on_conflict = error_on_conflict
        self.return_column = return_column

    def get_sql_str(self) -> sql.SQL:
        """Constructs and return sql object of query."""

        columns = self.data[0].keys()
        columns_str = sql.SQL(",").join([sql.Identifier(c) for c in columns])

        formated_rows = []
        for row in self.data:
            attributes = []
            for attribute in row.values():
                attributes.append(attribute)
            attributes_str = sql.SQL("({attributes})").format(
                attributes=sql.SQL(",").join([sql.Literal(a) for a in attributes])
            )
            formated_rows.append(attributes_str)

        values_str = sql.SQL(",").join(formated_rows)

        sql_str = sql.SQL("INSERT INTO {table} ({columns}) VALUES {values}").format(
            table=self.table.get_db_identifier(),
            columns=columns_str,
            values=values_str,
        )

        returning_str = ""
        if self.return_column:
            returning_str = sql.SQL("RETURNING {return_column}").format(
                return_column=sql.Identifier(self.return_column)
            )
            sql_str = sql.SQL(" ").join([sql_str, returning_str])

        on_conflict_str = ""
        if not self.error_on_conflict:
            on_conflict_str = sql.SQL("ON CONFLICT DO NOTHING")
            sql_str = sql.SQL(" ").join([sql_str, on_conflict_str])

        return sql_str


class UpdateQuery(ComponentQuery):
    """Class to construct UPDATE query."""

    def __init__(
        self,
        table: Table,
        where_clauses: List[WhereClause],
        data: Dict[str, Any],
    ) -> None:
        super().__init__(table=table)

        self.where_clauses = where_clauses
        self.data = data

    def get_sql_str(self) -> sql.SQL:
        """Constructs and return sql object of query."""

        statements = []

        for column, attribute in self.data.items():

            key_value_composed = sql.SQL("=").join(
                [sql.Identifier(column), sql.Literal(attribute)]
            )
            statements.append(key_value_composed)

        statements_str = sql.SQL(",").join(statements)

        sql_str = sql.SQL("UPDATE {table} SET {statements}").format(
            table=self.table.get_db_identifier(), statements=statements_str
        )

        where_condition = sql.SQL(" AND ").join(
            [c.get_sql_str() for c in self.where_clauses]
        )
        where_clause_str = sql.SQL(" ").join([sql.SQL("WHERE"), where_condition])

        sql_str = sql.SQL(" ").join([sql_str, where_clause_str])

        return sql_str


class DeleteQuery(ComponentQuery):
    """Class to construct DELETE query."""

    def __init__(
        self,
        table: Table,
        where_clauses: List[WhereClause],
    ) -> None:
        super().__init__(table=table)

        self.where_clauses = where_clauses

    def get_sql_str(self) -> sql.SQL:
        """Constructs and return sql object of query."""

        where_condition = sql.SQL(" AND ").join(
            [c.get_sql_str() for c in self.where_clauses]
        )
        where_clause_str = sql.SQL(" ").join([sql.SQL("where"), where_condition])

        return sql.SQL("DELETE FROM {table} {where_clauses}").format(
            table=self.table.get_db_identifier(), where_clauses=where_clause_str
        )


class CreateQuery(ComponentQuery):
    """Class to construct CREATE query."""

    def __init__(
        self,
        table: Table,
        sql_column_dict: Dict[str, str],
        primary_key: str,
    ) -> None:
        super().__init__(table=table)

        self.sql_column_dict = sql_column_dict
        self.primary_key = primary_key

    def get_sql_str(self) -> sql.SQL:
        """Constructs and return sql object of query."""

        primary_key_column = sql.SQL(" ").join(
            [sql.Identifier(self.primary_key), sql.SQL("serial NOT NULL")]
        )

        columns_list = [primary_key_column]

        for c_name, c_type in self.sql_column_dict.items():
            columns_list.append(
                sql.SQL("{column} {type}").format(
                    column=sql.Identifier(c_name), type=sql.SQL(c_type)
                )
            )

        columns_list.append(
            sql.SQL("PRIMARY KEY ({column})").format(
                column=sql.Identifier(self.primary_key)
            )
        )

        sql_str = sql.SQL("CREATE TABLE IF NOT EXISTS {table} ({columns})").format(
            table=self.table.get_db_identifier(),
            columns=sql.SQL(",").join(columns_list),
        )

        return sql_str


class DropQuery(ComponentQuery):
    """Class to construct DROP query."""

    def __init__(
        self,
        table: Table,
    ) -> None:
        super().__init__(table=table)

    def get_sql_str(self) -> sql.SQL:
        """Constructs and return sql object of query."""

        return sql.SQL("DROP TABLE IF EXISTS {table}").format(
            table=self.table.get_db_identifier()
        )


class AlterQuery(ComponentQuery):
    """Class containing shared attributes and functions for ALTER queries."""

    def __init__(self, table: Table, column: str, column_type: str) -> None:
        super().__init__(table=table)

        self.column = column
        self.column_type = column_type


class AddColumnQuery(AlterQuery):
    """Class to construct ALTER ADD query."""

    def __init__(self, table: Table, column: str, column_type: str) -> None:
        super().__init__(table=table, column=column, column_type=column_type)

    def get_sql_str(self) -> sql.SQL:
        """Constructs and return sql object of query."""

        return sql.SQL("ALTER TABLE {table} ADD {column} {column_type}").format(
            table=self.table.get_db_identifier(),
            column=sql.Identifier(self.column),
            column_type=sql.SQL(self.column_type),
        )


class ChangeColumnQuery(AlterQuery):
    """Class to construct ALTER COLUMN query."""

    def __init__(self, table: Table, column: str, column_type: str) -> None:
        super().__init__(table=table, column=column, column_type=column_type)

    def get_sql_str(self) -> sql.SQL:
        """Constructs and return sql object of query."""

        return sql.SQL(
            "ALTER TABLE {table} ALTER COLUMN {column} TYPE {column_type} USING {column}::{column_type}"
        ).format(
            table=self.table.get_db_identifier(),
            column=sql.Identifier(self.column),
            column_type=sql.SQL(self.column_type),
        )
