# Module: Database

The database module contains the `Postgres` class which wraps a `psycopg2` postgres client.
It contains various functions to manage the database connection and build safe queries.

## When to use it?

This module should be used for all operations on the Postgres database.
If other database clients are added, these should be located in this module as well.

## How to use it?

```python
postgres_config = {
    "host": <postgres address including port>,
    "database": <name of database>,
    "user": <name of user>,
    "password": <password of user>,
    "system_schema": <name of system schema>,
    "shared_schema": <name of shared schema>
}

postgres = Postgres(
    postgres_config,
    <Logger obj>
)
```

__Make sure to use environmental variables for user and password!__
