"""Contains wrapper functions for creating queries using class blocks."""
import json
from datetime import datetime
from typing import Any, Dict, List, Optional, Tuple, Union

from psycopg2 import DatabaseError

from database.components import (
    ArrayAggregate,
    ArrayColumn,
    Column,
    FetchPolicy,
    GroupBy,
    JoinTable,
    OrderBy,
    Table,
    WhereClause,
)
from database.database import Postgres
from database.models import QueryColumn, SelectQuery
from database.transaction import TransactionContext
from result.status import StatusCode


def insert_workspace_database_entry(
    postgres: Postgres,
    workspace_schema: str,
    table_name: str,
    id_column: str,
    data: Dict[str, Any],
) -> StatusCode:
    """Inserts data into table. Limited to workspace schema."""

    table = Table(workspace_schema, table_name)
    return insert_database_entry(postgres, table, id_column, data)


def insert_database_entry(
    postgres: Postgres,
    table: Table,
    id_column: str,
    data: Dict[str, Any],
) -> StatusCode:
    """Wrapper for database insert."""

    try:
        postgres.insert(table, [data], True)
        return StatusCode.SUCCESS
    except DatabaseError:
        postgres.fix_column_sequence(table, id_column)

    try:
        postgres.insert(table, [data], True)
    except DatabaseError:
        return StatusCode.FAIL

    return StatusCode.SUCCESS


def get_participant_entry_by_verification_code(
    postgres: Postgres, verification_code: str
) -> Optional[Tuple[str, int]]:
    """Returns tuple with participant identifier and workspace_id
    for provided verification code if it exists.
    """
    participants_table = Table(postgres.shared_schema, "participants")
    group_allocation_table = Table(postgres.shared_schema, "group_allocation")
    groups_table = Table(postgres.shared_schema, "groups")

    return postgres.select(
        table=participants_table,
        all_columns=False,
        columns=[
            Column(participants_table, "identifier"),
            Column(groups_table, "workspace_id"),
        ],
        joined_tables=[
            JoinTable(
                group_allocation_table,
                Column(participants_table, "participant_id"),
                Column(group_allocation_table, "participant_id"),
            ),
            JoinTable(
                groups_table,
                Column(group_allocation_table, "group_id"),
                Column(groups_table, "group_id"),
            ),
        ],
        where_clauses=[
            WhereClause(
                Column(participants_table, "verification_code"), verification_code
            )
        ],
    )


def get_participant_data(
    postgres: Postgres,
    workspace_id: int,
    identifier: str,
    columns: List[Union[Column, ArrayAggregate]],
) -> Optional[tuple]:
    """Executes query to select participant data based on identifier and workspace_id.
    Returns raw participant data or empty array.
    """
    participants_table = Table(postgres.shared_schema, "participants")
    group_allocation_table = Table(postgres.shared_schema, "group_allocation")
    groups_table = Table(postgres.shared_schema, "groups")

    return postgres.select(
        table=participants_table,
        all_columns=False,
        columns=columns,
        joined_tables=[
            JoinTable(
                group_allocation_table,
                Column(participants_table, "participant_id"),
                Column(group_allocation_table, "participant_id"),
            ),
            JoinTable(
                groups_table,
                Column(group_allocation_table, "group_id"),
                Column(groups_table, "group_id"),
            ),
        ],
        where_clauses=[
            WhereClause(Column(participants_table, "identifier"), identifier),
            WhereClause(Column(groups_table, "workspace_id"), workspace_id),
            WhereClause(Column(participants_table, "active"), True),
        ],
        group_by=GroupBy([Column(participants_table, "participant_id")]),
        fetch_all=False,
    )


def get_group_and_workspace_id_by_invitation_token(
    postgres: Postgres, invitation_token: str
) -> Tuple[int, int]:
    """Executes query to check invitation token and return associated group_id if it exists."""

    group_invitations_table = Table(postgres.system_schema, "group_invitations")
    groups_table = Table(postgres.shared_schema, "groups")

    return postgres.select(
        table=group_invitations_table,
        all_columns=False,
        columns=[
            Column(groups_table, "group_id"),
            Column(groups_table, "workspace_id"),
        ],
        joined_tables=[
            JoinTable(
                groups_table,
                Column(group_invitations_table, "group_id"),
                Column(groups_table, "group_id"),
            )
        ],
        where_clauses=[
            WhereClause(
                Column(group_invitations_table, "invitation_token"), invitation_token
            ),
            WhereClause(
                Column(group_invitations_table, "expires"), datetime.now(), ">"
            ),
        ],
    )


def get_participant_id_by_workspace_and_identifier(
    postgres: Postgres, workspace_id: str, identifier: str
) -> Optional[int]:
    """Returns participant_id based on identifier and workspace_id if it exists."""

    participants_table = Table(postgres.shared_schema, "participants")
    group_allocation_table = Table(postgres.shared_schema, "group_allocation")
    groups_table = Table(postgres.shared_schema, "groups")

    return postgres.select(
        participants_table,
        False,
        [Column(participants_table, "participant_id")],
        [
            JoinTable(
                group_allocation_table,
                Column(participants_table, "participant_id"),
                Column(group_allocation_table, "participant_id"),
            ),
            JoinTable(
                groups_table,
                Column(group_allocation_table, "group_id"),
                Column(groups_table, "group_id"),
            ),
        ],
        [
            WhereClause(Column(participants_table, "identifier"), identifier),
            WhereClause(Column(groups_table, "workspace_id"), workspace_id),
        ],
        None,
        GroupBy([Column(participants_table, "participant_id")]),
    )


def create_participant_entry_and_return_id(
    postgres: Postgres,
    identifier: str,
    created_by: str,
) -> int:
    """Inserts new entry with identifier in participants table. Returns participant_id."""

    participants_table = Table(postgres.shared_schema, "participants")

    return postgres.insert(
        participants_table,
        [
            {
                "identifier": identifier,
                "active": True,
                "is_registered": False,
                "custom_fields": json.dumps({}),
                "created_at": datetime.now(),
                "created_by": created_by,
            }
        ],
        True,
        "participant_id",
    )


def is_empty(
    postgres: Postgres, table: Table, where_column: str, where_value: Any, column: str
) -> bool:
    """Formats and executes query to check if a database field is empty."""

    res = postgres.select(
        table,
        all_columns=False,
        columns=[Column(table, where_column)],
        where_clauses=[
            WhereClause(Column(table, where_column), where_value),
            WhereClause(Column(table, column), None, "IS"),
        ],
        fetch_all=False,
    )

    return res != [] and res


def migrate_table_transaction(
    transaction: TransactionContext,
    table: Table,
    sql_column_dict: Dict[str, str],
    li_sql_column_dict: Dict[str, str],
) -> None:
    """Manages database migration for specific table."""
    table_schema: Dict[str, str] = {}

    information_schema_table = Table("information_schema", "columns")
    rows = transaction.select(
        table=information_schema_table,
        all_columns=False,
        columns=["column_name", "data_type"],
        where_clauses=[
            WhereClause(Column(information_schema_table, "table_schema"), table.schema),
            WhereClause(Column(information_schema_table, "table_name"), table.name),
        ],
        fetch_all=True,
    )

    if not rows:
        raise DatabaseError(f"No schema available for table {table.get_full_name()}")

    for row in rows:
        column_name = row[0]
        column_type = row[1]

        table_schema[column_name] = column_type

    for column_name, local_column_type in li_sql_column_dict.items():
        if column_name not in table_schema:
            transaction.add_column(table, column_name, sql_column_dict[column_name])
            continue

        db_column_type = table_schema[column_name]

        if db_column_type != local_column_type:
            try:
                transaction.change_column(table, column_name, local_column_type)
            except DatabaseError as error:
                raise DatabaseError(
                    f"Type of column {column_name} can't be converted. Please use another db_column name."
                ) from error


def migrate_table(
    postgres: Postgres,
    table: Table,
    sql_column_dict: Dict[str, str],
    li_sql_column_dict: Dict[str, str],
) -> None:
    """Executes table migration as transaction."""

    postgres.transaction(
        migrate_table_transaction, table, sql_column_dict, li_sql_column_dict
    )


def get_survey_data(
    postgres: Postgres,
    workspace_id: int,
    include_invalid: bool,
) -> List[tuple]:
    """Executes query to select list of all surveys of workspace including their data."""

    survey_table = Table(postgres.system_schema, "surveys")
    survey__visibility_table = Table(postgres.system_schema, "survey_visibility")
    groups_table = Table(postgres.shared_schema, "groups")

    query = SelectQuery(
        survey_table,
        False,
        [
            Column(survey_table, "display_name"),
            Column(survey_table, "is_registration"),
            Column(survey_table, "survey"),
            Column(groups_table, "name"),
        ],
        [
            JoinTable(
                survey__visibility_table,
                Column(survey_table, "survey_id"),
                Column(survey__visibility_table, "survey_id"),
            ),
            JoinTable(
                groups_table,
                Column(survey__visibility_table, "group_id"),
                Column(groups_table, "group_id"),
            ),
        ],
        [
            WhereClause(Column(survey_table, "active"), True),
            WhereClause(Column(groups_table, "workspace_id"), workspace_id),
        ],
        OrderBy(Column(survey_table, "display_name"), "ASC"),
        None,
    )

    if not include_invalid:
        query.add_where_clause(WhereClause(Column(survey_table, "invalid"), False))

    res = postgres.execute(query=query, fetch=FetchPolicy.FETCH_ALL)

    if res is None:
        return []

    return res


def get_workspace_data(
    postgres: Postgres,
    columns: List[Union[str, Column, ArrayAggregate]],
    instance_id: Optional[int] = None,
    workspace_id: Optional[int] = None,
) -> List[tuple]:
    """Executes query to select list of all workspaces including their data
    for specific instance_id or workspace_id.
    Returns list of tuples with workspace data or empty array.
    """

    workspaces_table = Table("system", "workspaces")

    enabled_languages_table = Table("system", "enabled_languages")
    default_language_column = QueryColumn(
        query=SelectQuery(
            table=enabled_languages_table,
            all_columns=False,
            columns=[Column(enabled_languages_table, "shortcode")],
            joined_tables=None,
            where_clauses=[
                WhereClause(Column(enabled_languages_table, "is_default"), True),
                WhereClause(
                    Column(enabled_languages_table, "workspace_id"),
                    Column(workspaces_table, "workspace_id"),
                ),
            ],
        ),
        name_as="default_language",
    )

    groups_table = Table("shared", "groups")
    default_group_column = QueryColumn(
        query=SelectQuery(
            table=groups_table,
            all_columns=False,
            columns=[Column(groups_table, "group_id")],
            joined_tables=None,
            where_clauses=[
                WhereClause(Column(groups_table, "name"), "default"),
                WhereClause(
                    Column(groups_table, "workspace_id"),
                    Column(workspaces_table, "workspace_id"),
                ),
            ],
        ),
        name_as="default_group",
    )

    command_aliases_table = Table("system", "command_aliases")
    commands_table = Table("system", "commands")
    command_aliases_query = SelectQuery(
        table=command_aliases_table,
        all_columns=False,
        columns=[
            ArrayColumn(
                [
                    Column(command_aliases_table, "command_type"),
                    Column(command_aliases_table, "alias"),
                ]
            )
        ],
        joined_tables=[
            JoinTable(
                commands_table,
                Column(command_aliases_table, "command_type"),
                Column(commands_table, "command_type"),
            )
        ],
        where_clauses=[
            WhereClause(
                Column(command_aliases_table, "workspace_id"),
                Column(workspaces_table, "workspace_id"),
            ),
        ],
    )
    command_aliases_column = QueryColumn(
        query=command_aliases_query, name_as="command_aliases", as_array=True
    )

    enabled_languages_table = Table("system", "enabled_languages")
    languages_table = Table("system", "languages")
    enabled_languages_query = SelectQuery(
        table=enabled_languages_table,
        all_columns=False,
        columns=[
            ArrayColumn(
                [
                    Column(enabled_languages_table, "shortcode"),
                    Column(languages_table, "display_name"),
                    Column(languages_table, "command"),
                ]
            )
        ],
        joined_tables=[
            JoinTable(
                languages_table,
                Column(enabled_languages_table, "shortcode"),
                Column(languages_table, "shortcode"),
            )
        ],
        where_clauses=[
            WhereClause(
                Column(enabled_languages_table, "workspace_id"),
                Column(workspaces_table, "workspace_id"),
            )
        ],
    )
    enabled_languages_column = QueryColumn(
        query=enabled_languages_query, name_as="enabled_languages", as_array=True
    )

    webhooks_table = Table("system", "webhooks")
    webhooks_query = SelectQuery(
        table=webhooks_table,
        all_columns=False,
        columns=[
            ArrayColumn(
                [
                    Column(webhooks_table, "display_name"),
                    Column(webhooks_table, "url"),
                    Column(webhooks_table, "include_token", "TEXT"),
                ]
            )
        ],
        where_clauses=[
            WhereClause(
                Column(webhooks_table, "workspace_id"),
                Column(workspaces_table, "workspace_id"),
            )
        ],
    )
    webhooks_column = QueryColumn(
        query=webhooks_query, name_as="webhooks", as_array=True
    )

    mandatory_translations_table = Table("system", "mandatory_translations")
    mandatory_translations_query = SelectQuery(
        table=mandatory_translations_table,
        all_columns=False,
        columns=[
            ArrayColumn(
                [
                    Column(mandatory_translations_table, "display_name"),
                    Column(mandatory_translations_table, "translation_key"),
                ]
            )
        ],
    )
    mandatory_translations_column = QueryColumn(
        query=mandatory_translations_query,
        name_as="mandatory_translations",
        as_array=True,
    )

    columns.extend(
        [
            default_language_column,
            default_group_column,
            command_aliases_column,
            enabled_languages_column,
            webhooks_column,
            mandatory_translations_column,
        ]
    )

    translations_table = Table("system", "translations")

    query = SelectQuery(
        table=workspaces_table,
        all_columns=False,
        columns=columns,
        joined_tables=[
            JoinTable(
                translations_table,
                Column(workspaces_table, "workspace_id"),
                Column(translations_table, "workspace_id"),
            )
        ],
        where_clauses=[WhereClause(Column(workspaces_table, "active"), True)],
    )

    if instance_id:
        query.add_where_clause(
            WhereClause(Column(workspaces_table, "instance_id"), instance_id)
        )

    if workspace_id:
        query.add_where_clause(
            WhereClause(Column(workspaces_table, "workspace_id"), workspace_id)
        )

    return postgres.execute(query=query, fetch=FetchPolicy.FETCH_ALL)
