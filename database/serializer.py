"""Contains functions for serializing database input."""
from datetime import datetime
from typing import Any


def default_json_serializer(obj: Any):
    """Default serializer that converts types that are not compatible with json."""

    if isinstance(obj, datetime):
        return obj.isoformat()
    raise TypeError("Type %s not serializable" % type(obj))
