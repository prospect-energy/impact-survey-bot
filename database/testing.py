"""Contains special database operations that are only relevant for testing."""
from typing import Dict

from psycopg2 import DatabaseError, connect, sql
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT


def create_database(conn_params: Dict[str, str], db_name: str, template: str) -> None:
    """Executes query to create a database from a template in a seperate connection.
    USE WITH CAUTION!
    Template database must have no active connections at the time of creation.
    """
    create_connection = connect(**conn_params)
    create_connection.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
    with create_connection.cursor() as cursor:
        query = (
            sql.SQL("CREATE DATABASE {db_name} WITH TEMPLATE {template}")
            .format(db_name=sql.Identifier(db_name), template=sql.Identifier(template))
            .as_string(create_connection)
        )

        try:
            cursor.execute(query)
        except DatabaseError:
            raise
        finally:
            create_connection.close()


def drop_database(conn_params: Dict[str, str], db_name: str) -> None:
    """Executes query to drop a database in a seperate connection.
    USE WITH CAUTION!
    """
    drop_connection = connect(**conn_params)
    drop_connection.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
    with drop_connection.cursor() as cursor:
        query = (
            sql.SQL("DROP DATABASE {db_name}")
            .format(db_name=sql.Identifier(db_name))
            .as_string(drop_connection)
        )

        try:
            cursor.execute(query)
        except DatabaseError:
            raise
        finally:
            drop_connection.close()
