"""Contains QueryInterface class."""
from logging import Logger
from typing import Any, Dict, List, Optional, Union

from database.components import (
    ArrayAggregate,
    ArrayColumn,
    Column,
    FetchPolicy,
    GroupBy,
    JoinTable,
    OrderBy,
    QueryColumn,
    Table,
    WhereClause,
)
from database.models import (
    AddColumnQuery,
    ChangeColumnQuery,
    ComponentQuery,
    CreateQuery,
    DeleteQuery,
    DropQuery,
    InsertQuery,
    PlainQuery,
    SelectQuery,
    UpdateQuery,
)


class QueryInterface:
    """Class containing functions for query execution."""

    def __init__(self) -> None:
        pass

    def __execute(
        self,
        logger: Logger,
        connection: Any,
        cursor: Any,
        query: Union[ComponentQuery, PlainQuery],
        fetch: Optional[FetchPolicy] = None,
    ) -> Optional[Any]:
        """Executes query using connection and cursor. Returns result of query."""

        query_str = query.get_sql_str().as_string(connection)
        logger.debug(f"Executed query: '{query_str}'")
        cursor.execute(query_str)

        if fetch == FetchPolicy.FETCH_ONE:
            return cursor.fetchone()

        if fetch == FetchPolicy.FETCH_ALL:
            return cursor.fetchall()

        return None

    def execute(
        self,
        query: Union[ComponentQuery, PlainQuery],
        fetch: Optional[FetchPolicy] = None,
    ) -> Optional[Any]:
        """Placeholder function for general query execution."""

        pass

    def select(
        self,
        table: Table,
        all_columns: bool,
        columns: Optional[
            List[Union[str, Column, ArrayAggregate, ArrayColumn, QueryColumn]]
        ] = [],
        joined_tables: Optional[List[JoinTable]] = [],
        where_clauses: Optional[List[WhereClause]] = [],
        order_by: Optional[OrderBy] = None,
        group_by: Optional[GroupBy] = None,
        fetch_all: bool = False,
    ) -> Optional[tuple]:
        """Formats and executes select query"""

        query = SelectQuery(
            table=table,
            all_columns=all_columns,
            columns=columns,
            joined_tables=joined_tables,
            where_clauses=where_clauses,
            order_by=order_by,
            group_by=group_by,
        )

        return self.execute(
            query=query,
            fetch=(FetchPolicy.FETCH_ALL if fetch_all else FetchPolicy.FETCH_ONE),
        )

    def insert(
        self,
        table: Table,
        data: List[Dict[str, Any]],
        error_on_conflict: bool = True,
        return_column: Optional[str] = None,
    ) -> Optional[Any]:
        """Formats and executes insert query."""

        query = InsertQuery(
            table=table,
            data=data,
            error_on_conflict=error_on_conflict,
            return_column=return_column,
        )

        return self.execute(
            query=query, fetch=(FetchPolicy.FETCH_ONE if return_column else None)
        )

    def update(
        self,
        table: Table,
        where_clauses: List[WhereClause],
        data: Dict[str, Any],
    ) -> None:
        """Formats and executes update query for database row with data being the updates."""

        query = UpdateQuery(table=table, where_clauses=where_clauses, data=data)
        self.execute(query=query, fetch=None)

    def delete(self, table: Table, where_clauses: List[WhereClause]) -> None:
        """Executes query to delete row based on where clause."""

        query = DeleteQuery(table=table, where_clauses=where_clauses)
        self.execute(query=query, fetch=None)

    def create_table(
        self, table: Table, sql_column_dict: Dict[str, str], primary_key: str
    ) -> None:
        """Formats and executes create query to create new database table."""

        query = CreateQuery(
            table=table, sql_column_dict=sql_column_dict, primary_key=primary_key
        )
        self.execute(query=query, fetch=None)

    def drop_table(self, table: Table) -> None:
        """Executes query to drop a table. USE WITH CAUTION!"""

        query = DropQuery(table=table)
        self.execute(query=query, fetch=None)

    def add_column(self, table: Table, column: str, column_type: str) -> None:
        """Formats and executes create query to add column to table."""

        query = AddColumnQuery(table=table, column=column, column_type=column_type)
        self.execute(query=query, fetch=None)

    def change_column(self, table: Table, column: str, column_type: str) -> None:
        """Formats and executes create query to change column of table."""

        query = ChangeColumnQuery(table=table, column=column, column_type=column_type)
        self.execute(query=query, fetch=None)
