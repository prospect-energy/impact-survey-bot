"""Contains type-checking functions for strings."""
import re


def can_be_int_str(value: str) -> bool:
    """Returns whether string is valid integer."""
    try:
        int(str(value))
    except ValueError:
        return False

    return True


def can_be_float_str(value: str) -> bool:
    """Returns whether string is valid float."""
    try:
        float(str(value))
    except ValueError:
        return False

    return True


def has_coordinates_str(lat: str, lng: str) -> bool:
    """Returns whether lat and lng are not empty strings."""
    if lat != "" and lng != "":
        return True

    return False


def is_phone_number_str(phone_number: str) -> bool:
    """Returns whether string has valid phonenumber format.
    Does not check whether phonenumber is assigned.

    Required format:
        - +<country code><5-20 digit number> (e.g. +11234567890)
        - 00<country code><5-20 digit number> (e.g. 0011234567890)
    """
    # https://regex101.com/r/NhAKhQ/3
    regex = r"^(\+|0{2})\d{5,20}$"

    return bool(re.match(regex, phone_number))


def is_url_str(url: str) -> bool:
    """Returns whether url has valid format.
    Does not check whether url is working.
    """

    if not isinstance(url, str):
        return False

    # https://regex101.com/r/9dsU9S/1
    regex = r"^https?:\/\/[a-zA-Z0-9.\-_]+\.[a-zA-Z0-9.\-_\/]+(?:\?\S+)?$"

    return bool(re.match(regex, url))
