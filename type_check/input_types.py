"""Contains custom typing objects for input-types."""
from typing import NewType

Url = NewType("Url", str)
Coordinate = NewType("Coordinate", str)
PhoneNumber = NewType("PhoneNumber", str)
Number = NewType("Number", float)
