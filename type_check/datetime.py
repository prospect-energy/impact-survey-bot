"""Contains functions to parse and verify date and time formats."""
from datetime import datetime
from typing import List


def strp_time(
    value: str, allowed_patterns: List[str] = ["%I:%M%p", "%H:%M"]
) -> datetime:
    """Parses time using provided datetime patterns.
    Returns ValueError if value does not match any pattern.

    Default patterns:
        - %I:%M%p (12-hour clock)
        - %H:%M (24-hour clock)
    """
    return check_datetime_format(value, allowed_patterns)


def strp_date(
    value: str,
    allowed_patterns: List[str] = ["%Y-%m-%d", "%d-%m-%Y", "%d/%m/%Y", "%d.%m.%Y"],
) -> datetime:
    """Parses date using provided datetime patterns.
    Returns ValueError if value does not match any pattern.

    Default patterns:
        - "%Y-%m-%d" (e.g. 2000-12-01)
        - "%d-%m-%Y" (e.g. 01-12-2000)
        - "%d/%m/%Y" (e.g. 01/12/2000)
        - "%d/%m/%Y" (e.g. 01.12.2000)
    """
    return check_datetime_format(value, allowed_patterns)


def check_datetime_format(value: str, allowed_patterns: List[str]) -> datetime:
    """Parses datetime using first matching pattern.
    Returns ValueError if value does not match any pattern."""

    if isinstance(value, datetime):
        return value

    for pattern in allowed_patterns:
        try:
            return datetime.strptime(str(value), pattern)
        except ValueError:
            pass
    raise ValueError


def can_be_date(value) -> bool:
    """Returns if value can be parsed as date using defaults."""
    if isinstance(value, datetime):
        return True

    try:
        strp_date(value)
    except ValueError:
        return False

    return True


def can_be_time(value) -> bool:
    """Returns if value can be parsed as time using defaults."""
    if isinstance(value, datetime):
        return True

    try:
        strp_time(value)
    except ValueError:
        return False

    return True
