"""Contains validation functions for twilio messages."""
from typing import Literal

from structs.messages import IncommingMessage
from type_check.datetime import strp_date, strp_time
from type_check.media import (
    is_audio_file,
    is_document_file,
    is_image_file,
    is_video_file,
)
from type_check.typed_string import (
    can_be_float_str,
    can_be_int_str,
    has_coordinates_str,
    is_phone_number_str,
    is_url_str,
)


def is_text(*_) -> Literal[True]:
    """Mock function to return always True."""
    return True


def is_text100(msg: IncommingMessage) -> bool:
    """Returns True if message message is less or equal 100 characters."""
    return len(str(msg.message)) <= 100


def can_be_int(msg: IncommingMessage) -> bool:
    """Returns whether message message is valid integer."""
    return can_be_int_str(msg.message)


def can_be_float(msg: IncommingMessage) -> bool:
    """Returns whether message message is valid float."""
    return can_be_float_str(msg.message)


def has_coordinates(msg: IncommingMessage) -> bool:
    """Returns whether message has values for latitude and longitude."""
    return has_coordinates_str(msg.latitude, msg.longitude)


def has_image(msg: IncommingMessage) -> bool:
    """Returns whether media object is not None and links to valid image type."""
    return is_image_file(msg.media)


def has_audio(msg: IncommingMessage) -> bool:
    """Returns whether media object is not None and links to valid audio type."""
    return is_audio_file(msg.media)


def has_video(msg: IncommingMessage) -> bool:
    """Returns whether media object is not None and links to valid video type."""
    return is_video_file(msg.media)


def has_document(msg: IncommingMessage) -> bool:
    """Returns whether media object is not None and links to valid document type."""
    return is_document_file(msg.media)


def is_date(msg) -> bool:
    """Returns whether message message can be parsed as date."""
    try:
        strp_date(msg.message)
    except ValueError:
        return False

    return True


def is_time(msg) -> bool:
    """Returns whether message message can be parsed as time."""
    try:
        strp_time(msg.message)
    except ValueError:
        return False

    return True


def is_time12(msg) -> bool:
    """Returns whether message message can be parsed as 12-hour time.

    msguired format:
        - %I:%M%p (3:00pm)
    """
    try:
        strp_time(msg.message, ["%I:%M%p"])
    except ValueError:
        return False

    return True


def is_time24(msg) -> bool:
    """Returns whether message message can be parsed as 24-hour time.

    msguired format:
        - %H:%M (15:00)
    """
    try:
        strp_time(msg.message, ["%H:%M"])
    except ValueError:
        return False

    return True


def is_url(msg) -> bool:
    """Returns whether message message has valid url format.
    Does not check whether url is working.
    """
    return is_url_str(msg.message)


def is_phone_number(msg) -> bool:
    """Returns whether message message has valid phonenumber format.
    Does not check whether phonenumber is assigned.

    msguired format:
        - +<country code><5-20 digit number> (e.g. +11234567890)
        - 00<country code><5-20 digit number> (e.g. 0011234567890)
    """
    return is_phone_number_str(msg.message)
