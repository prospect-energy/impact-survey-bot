"""Contains functions for downloading and managing downloaded files."""
import os
from typing import Any, Dict, Optional, Tuple
from uuid import uuid4

import requests

from result.error import FileException
from result.status import StatusCode


def download_file(
    local_directory: str, url: str, download_headers: Dict[str, Any], filename: str
) -> Tuple[str, str]:
    """Downloads file from url and stores it local_directory.
    Returns FileException if url is invalid or file can't be downloaded.
    """
    local_path = "/".join([local_directory, filename])
    content_type = ""

    os.makedirs(os.path.dirname(local_path), exist_ok=True)

    try:
        resp = requests.get(url, headers=download_headers, allow_redirects=True)
        if resp.status_code != 200:
            raise FileException("Url for download returned a non-200 statuscode")

        content_type = resp.headers.get("content-type")

        with open(local_path, "wb") as file:
            file.write(resp.content)

    except Exception as error:
        raise FileException(
            f"Cannot download or write file '{local_path}' - error: {error}"
        ) from error

    return local_path, content_type


def delete_file(path: str) -> StatusCode:
    """Deletes local file if path exists.
    Use with caution!
    """
    if os.path.exists(path):
        os.remove(path)
        return StatusCode.SUCCESS

    return StatusCode.FAIL
