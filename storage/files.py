"""Contains functions for file(name) operations."""

content_type_extension_dict = {
    "image/jpeg": "jpeg",
    "image/png": "png",
    "audio/ogg": "ogg",
    "video/ogg": "ogg",
    "audio/mpeg": "mpeg",
    "audio/aac": "acc",
    "audio/mp4": "mp4",
    "audio/amr": "amr",
    "video/mp4": "mp4",
    "video/3sp": "3sp",
    "application/pdf": "pdf",
    "text/plain": "txt",
    "application/msword": "doc",
    "application/vnd.openxmlformats-officedocument.wordprocessingml.document": "docx",
    "application/vnd.ms-excel": "xls",
    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet": "xlsx",
    "application/vnd.ms-powerpoint": "ppt",
    "application/vnd.openxmlformats-officedocument.presentationml.presentation": "pptx",
}


def get_extension_from_content_type(content_type: str) -> str:
    """Returns the file extension from content type. Returns empty string if content type is not known."""
    return content_type_extension_dict.get(content_type, "")
