# ImpactSurveyBot (ISB)

<h3 align="center"><b>Scalable solution to host surveys and forms on both WhatsApp and Telegram.</b></h3>
&nbsp;
<p align="center">
⚙️ Supports extensive customization of surveys.
</p>

<p align="center">
⚡️ Fast and scalable.
</p>

<p align="center">
📀 Uses Postgres and S3 for data storage.
</p>

&nbsp;

- [ImpactSurveyBot (ISB)](#impactsurveybot-isb)
  - [💡 Introduction](#-introduction)
  - [🔍 Features](#-features)
  - [📐 How does it work?](#-how-does-it-work)
    - [Request Flow](#request-flow)
    - [Workspace Update](#workspace-update)
    - [Database Structure](#database-structure)
    - [SaaS-Platform](#saas-platform)
  - [❓Usage](#usage)
    - [Setting it up locally](#setting-it-up-locally)
    - [Usage \& Development](#usage--development)
    - [Deployment](#deployment)
  - [🏗 Limitations](#-limitations)
  - [🔑 License](#-license)

&nbsp;
- [Docs](/docs/)
  - [Database-Setup](/docs/DATABASE_SETUP.md)
  - [Environmental Variables](/docs/ENVIRONMENT.md)
  - [Programming in JSON](/docs/PROGRAMMING.md)
  - [SaaS Platform](/docs/SAAS_PLATFORM.md)

---

## 💡 Introduction

This project provides a flexible and scalable way to host surveys and forms on both WhatsApp and Telegram. Originally developed for research surveys and maintenance forms in the energy-access sector, ISB developed into a comprehensive tool which can cover a big variety of tasks. Hereby, the project is one of three components required to run the full SaaS-Platform. Contained in this repository is the code for the bot instances that interact with the incoming chat messages. The bot-instances can be also run without the other two components but doing so requires careful management of the database.

---
## 🔍 Features
- __Multi-Platform Support:__
  - support of WhatsApp, Telegram as well as Twilio
  - one instance can handle all chat providers at the same time
  - further integrations of SMS (Twilio) and Facebook Messenger (Business API) possible 
- __Programmable Surveys/Forms:__
  - programming of surveys/forms in both JSON and a Visual Editor
  - support for conditions, comparisons, logic, database operations, POST requests and variables (explanation of JSON programming: [link]())
  - extensive validation of surveys
- __Multi-Tenancy Architecture:__
  - single bot instance can host multiple workspaces
  - each workspace contains configuration, surveys and translations of a company/project
  - database access with separate workspace-schema is created for each workspace
  - participants can be part of multiple workspaces at the same time
- __Grouping of Participants:__
  - participants can be grouped for management and access-control
  - ability to restrict surveys to specific groups
  - groups can also be used for conditional sections in surveys
  - participants can be added to groups manually, via verification tokens (single) or invitation tokens (multiple)
- __Multiple Languages Support:__
  - bot supports multiple languages within the same survey
  - localization-keys are used to define multiple translations for a message
  - workspace-owner can configure default and enabled languages
- __Commands:__
  - next to commands to switch the language, the bot allows for six different commands:
    - /start - to start/restart session
    - /end - to end session
    - /help - to get help for the current question (if defined)
    - /back - to get to the previous question
    - /skip - to skip the current question (if enabled)
    - /invitation=<token> - to apply an invitation token (mainly for usage via urls)
  - aliases for these commands can be defined by the workspace-owner
- __Customizations:__
  - extensive customization-options within a workspaces
  - including:
    - session timeout
    - command-aliases
    - enabled languages
    - participant registration
- __Data Storage in Postgres and S3:__
  - data from the surveys can be stored in [Postgres](https://www.postgresql.org/) and buckets ([AWS S3](https://aws.amazon.com/s3) or [MinIO](https://min.io/))
  - storage in Postgres allows for the usage on SQL queries on the data
  - export to CSV possible
  - each workspace is provided with a seperate database-access

---
## 📐 How does it work?

The Impact-Survey-Bot works similar to tools like Google Forms and Typeform, except that it has a chatbot as user interface. The participants of each survey need to write a message to either a specific phone number on WhatsApp or the bot handle on Telegram. Both platforms offer chat links to quickly switch to the chat with the bot. When using WhatsApp, invitation links can be created to add participants to groups programmatically. As soon as they have sent the first message to the chatbot, they will be guided through the questions/steps. To accommodate various types of surveys, ISB provides different types of questions and a wide range of other configuration options to the survey creator. In addition, surveys are grouped into workspaces that belong to a manager/organization. These workspaces provide further profound configuration options.
Within the surveys, the bot uses the concept of `Steps` to allow for modularity and to track the participant's progress. The `Steps` can be envisioned as a kind of linked list where each item refers to one or multiple subsequent items. These items are all `Step` objects which can have different types such as responses (message/selection) and actions (for logic). The distinction of which subsequent step (`"successor"`) will follow depends on the result of the action or the selected option. A simple visualization can be found below:

![Linked List of Steps](docs/assets/Linked_Steps.png)

Besides simply replying to the questions/messages, the participant has also the option to use commands to navigate through the survey or change their preferred language. More information on this can be found in the [Features](#🔍-features) section of this document.

And how to use it when you are a survey creator? Creating surveys is currently only possible in `.json`-files. A Visual Editor is under way but for the moment, programming the surveys in JSON is the only possible option. If you want to learn more about how you can create/programm surveys, please refer to [this document](/docs/PROGRAMMING.md).

### Request Flow

Currently, the Impact-Survey-Bot supports both Twilio and WhatsApp Business API for WhatsApp as well as the Telegram Bot API for Telegram chats. Hereby, three webhook routes `POST /twilio`, `POST /whatsapp` and `POST /telegram` are available to accept incomming messages/updates and to handle them. Each messaging platform maintains the phone numbers/user id and configuration for the profiles. For WhatsApp, we recomment using the WhatsApp Business API instead of Twilio as this is the most cost-efficient option.
Inside the bot, each incoming request is validated and converted to a `IncommingMessage` object which is used subsequently for accessing the message data. Because neither platform shares information about conversation sessions, the bot keeps track of sessions on its own. This is necessary to store any progress and have a conversation across multiple messages. However, for each incoming request, the bot must connect the message to the session which is done via the identifier (phone number / telegram user id). To verify the sender, there is a signature check on every request.
While the `WorkspaceHandler` mainly loads and manages all workspace instances, it also keeps track of the active sessions in a dictionary object. If there is already an active session connected to the identifier, the `Session` object that includes the participant data and progress is retrieved and the message is processed regularly by the `SessionHandler`.
If there isn't any active session for the participant, the bot needs to create a new session. Hereby, there are three possible cases:

1. Participant belongs to exactly one workspace.
2. Participant does not belong to any workspace yet.
3. Participant has records in multiple workspaces.

For the first case, the participant data is retrieved from the workspace and a new `Session` object is created. This is the simplest of the three options and should occur most of the time if there is no active session.
If the participant isn't connected to any workspace yet (case two), they will be prompted with a message that asks for a verification code.
The verification code needs to be contained in the `participants` table of the database already and is connected to a workspace. As soon as the participant enters a valid verification code, the identifier is added to the respective database column and a new `Session` is created. This is usually followed by the registration survey for this workspace.
In the third case, the participant was added to multiple workspaces (e.g. there are multiple participant entries with the same identifier in the database). If this case occurs, the participant is prompted for a selection of the workspaces they are part of. After selecting one of the available workspaces, a `Session` object using the respective workspace configuration is created. This `WorkspaceHandler`-related request flow is depicted in the figure below.

![Visualisation of the Request-Flow](docs/assets/Request_Flow.png)

As soon as there is a session connected to the incoming message, both the session and message data is used to send the next response to the participant. The `Session` object includes references to the current step and survey that are used in combination with the participant input to run along the survey until the next response step (message/selection) is reached. The response is then converted to XML and returned to Twilio.
This entire process is managed by the `SessionHandler`.

To enable a conversation across multiple questions/messages, the session stays active unless it is terminated. Termination can be caused by a command (start/end), because the participant reached the end of the survey or because the session timed out since it was idle for too long.

### Workspace Update

The Impact-Survey-Bot offers live updates of surveys, translations and configurations. Because multiple workspaces are hosted on the same bot instance, restarting the instance is not possible without deleting the session data of the other workspaces. Therefore, updates need to be processed in parallel to the regular request flow.
To accomodate this functionality, the ISB has a second functional route: `/update`.
During the start of the bot instance, all workspaces associated with an instance id are retrieved from the database and a `Workspace` object is created for each. These objects are then referenced and managed by the `WorkspaceHandler`. Each time when a new session is created, the `Session` object is provided with the respective `Workspace` object which contains all the parsed configuration and surveys. This enables the bot to distinguish between different workspace configurations on a session-level. Because the `WorkspaceHandler` only saves references to the latest version of each workspace, new sessions will always be assigned with the latest configuration. Hereby, all of the necessary workspace data and configuration is saved in the `Workspace` object and later database queries are only necessary to retrieve the participant data as well as for survey operations.

When the configuration should be updated, changes (e.g. survey, translation updates) can be written to the database and won't become effective immediately. In order to notify the bot instance of these changes, a POST-Request containing the workspace id needs to be sent to the `/update` route. This causes the `UpdateHandler` to load the data for the workspace from the database and to create a new `Workspace` object. The `Workspace` object then replaces the old `Workspace` reference in the `WorkspaceHandler`. While this will make the changes effective for newly created sessions, active sessions won't be affected by these changes and will continue using the previous workspace configuration. Because the old `Workspace` object is not referenced by the `WorkspaceHandler` anymore but only by the `Session` objects, it will be removed by the garbage collector as soon as all sessions referencing the `Workspace` are terminated/removed.

![Visualisation of a Workspace-Update](docs/assets/Update_Flow.png)

If some exception occurs during the update process while loading the workspace data or initiating the `Workspace` object, the error will be returned as a well-formatted response by the `/update` route. 

### Database Structure

The Postgres Database accommodates both the ISB data and the survey data.
To ensure sufficient security and isolation, the database is divided into 2+n schemas:

- System
- Shared
- n x Workspace

The names of the first two can be changed in the [configuration of the bot instance](/docs/ENVIRONMENT.md). `System` and `Shared` schemas contain all the workspace, participant, survey, translation and other configuration data that is needed to run the ISB. Hereby, `System` should only be accessible by the system (bot instance, backend) while `Shared` can also be accessed by platform-users to use the participant data for queries. To ensure that platform-users cannot access data of other workspaces unauthorized, tables in the `Shared` schema use "Row Security Level Policy" to limit access to the participant data and groups on a workspace-level.
Next to these two static schemas, there is a variable amount of workspace-schemas that host the input data for the surveys of a workspace.
On the creation of each workspace, a new schema and database role must be created. The `workspaces` table in the database contains a column for the name of this new schema. To prevent confusion, the names of the schema and workspace should match.
As soon as a survey is loaded by a bot instance, the survey-table is auto-migrated (if enabled in the survey). Hereby, it uses the configuration section of the survey as well as the `db-column` and `input-type` fields to map the survey to the database table structure. To prevent any loss of data, no columns are removed and only compatible column types are altered. A simplified Entity-Relationship Model can be found below:

![Simplified Version of the ERD](docs/assets/ERD_small.png)

For a more extensive Entity-Relationship Model and more information about the database setup, please refer to: [Database-Setup](/docs/DATABASE_SETUP.md).

### SaaS-Platform

This project is only one component of a planned SaaS-Platform that allows easy survey creation and management through a web interface. You can find more information about the plans here: [SaaS Platform](/docs/SAAS_PLATFORM.md). While backend and frontend are still under construction, this project already contains all interfaces and functionality to accommodate the platform.

---
## ❓Usage

### Setting it up locally

If you want to run your own instance of the Impact-Survey-Bot, you can follow the steps below. Setting up your own instance requires some effort but is necessary if you want to contribute to the project or want to have more flexibility regarding the usage.

__Requires:__
- Python (`version: >= 3.9`)
- Postgres-Server (`version: >= 12`)
- AWS S3 (https://aws.amazon.com/de/s3/) or MinIO buckets (https://min.io/)

__Basic Setup:__
1. Fork repository.
2. Clone repository and check it out locally.
3. Setup the database. You can find more information about the process [here](/docs/DATABASE_SETUP.md). While both production application and test-cases can run on one database server, it's recommended to create two separate database servers to prevent collision.
4. Setup two AWS S3 / MinIO buckets, one for the main application and one for testing. You can find more information on this for [AWS](https://aws.amazon.com/en/s3/) or [MinIO](https://min.io/).
5. If you use this the ISB instance without backend, make sure to set the following policy for both buckets in order to make the links publically available. If you use the backend, secure links will be created automatically and a service account with `readonly` access is sufficient. Using this policy, you can create URLs using the base url `https://<bucket>.s3.amazonaws.com/<workspace_asset_key>/<filename from survey entry in database>`.
    ```json
    {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Effect": "Allow",
                "Principal": {
                    "AWS": [
                        "*"
                    ]
                },
                "Action": [
                    "s3:GetObject"
                ],
                "Resource": [
                    "arn:aws:s3:::<bucket_name>/*"
                ]
            }
        ]
    }
    ```
6. Create an role with read and write access to the buckets. An example for the policy can be found [here](https://docs.aws.amazon.com/IAM/latest/UserGuide/reference_policies_examples_s3_rw-bucket.html). The same policy can be used for MinIO althrough the user creation is different.
7.  Set the necessary environmental variables. You can find more information on the required variables [here](/docs/ENVIRONMENT.md).

__With Docker:__

7. Install [Docker](https://www.docker.com/).
8. You are ready to start the server. 🎉

__Without Docker:__

7. Install [Poetry Dependency Manager](https://python-poetry.org/docs).
8. Start virtual environment:
    ```sh
    poetry shell
    ```
9. Install all dependencies:
    ```sh
    poetry install
    ```
10. You are ready to start the server. 🎉


### Usage & Development

The most important branches for this repository are `main`, `development` and `release/<version>`.

__Start the server (with Docker):__
  ```sh
  docker compose up
  ```

*You can rebuild/restart the Docker Image after changes using:*
```sh
docker compose up --build
```

__Start the server (without Docker):__
  ```sh
  uvicorn app:app --host 0.0.0.0 --port 8000
  ```

__Information on the available routes:__
  - http://0.0.0.0:8000/docs/

__Testing:__
- If you want to test the ISB manually, you can use the Bot-CLI ("Botli"). You can find more information on the usage [here](/botli/README.md).
- Run test-cases:
  ```sh
  python -m unittest discover
  ```

### Deployment

So far the project uses [AWS Elastic Beanstalk](https://aws.amazon.com/elasticbeanstalk/) for hosting the instances. However, you can choose another hosting option as well. For that case, you just need to adapt the [CI/CD pipeline](/.gitlab-ci.yml).

![CI/CD Pipeline](/docs/assets/ISB_CICD_Diagram.png)

If you made some changes to the application, test your changes locally first (see [Usage & Development](#usage--development)). For deploying to Elastic Beanstalk, you can use the CD pipeline which runs for push/merge events on the `development` and `main` branches. To use this, you need to configure the following environmental variables in you remote repository first:
- DB_TESTING_ADDRESS
- DB_TESTING_PORT
- DB_TEST_SETUP_USER
- DB_TEST_SETUP_PASSWORD
- DB_TESTING_USER
- DB_TESTING_PASSWORD
- S3_ACCESS_KEY
- S3_SECRET_ACCESS_KEY
- S3_ENDPOINT
- S3_TESTING_BUCKET

You can find more information on them [here](/docs/ENVIRONMENT.md).
Next time, when you push your local changes to one of the two branches, they will be deployed to Elastic Beanstalk under the condition that the test-cases pass and everything is configured correctly.
Alternatively, you can also use the [Elastic Beanstalk CLI](https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/eb-cli3.html) to deploy your changes directly.

As soon as you have a running instance, you can configure the choosen messenging platform to redirect incoming messages for a specific phone number / user id to the ISB instance. Using the following links you can find more information for the setup:

- [Twilio](https://www.twilio.com/messaging/whatsapp)
- [Telegram](https://core.telegram.org/bots)
- [WhatsApp Business](https://developers.facebook.com/docs/whatsapp/cloud-api/)

---
## 🏗 Limitations

- There is no automigration for the system database in place. The database structure must be set up manually.
- The bot can only reply to a message sent by the participant. Right now it is not possible to send a message proactively (without a request) nor to send multiple messages at once.
- Using this component (repository) solely for running a complete chatbot requires careful management of the database to add and modify workspaces, surveys and participants.

---
## 🔑 License

Impact-Survey-Bot is published under the GNU GPLv3 license. Please be aware that all distributions including modifications of this source code must be made publicly available and that copyright as well as license must be preserved.
