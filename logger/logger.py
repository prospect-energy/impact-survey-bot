"""Contains logger class."""
import logging
import sys
import time
from typing import Any, Dict


class Logger:
    """Singleton to handle logging."""

    def __init__(self, config: Dict[str, Any]) -> None:
        logger_level = config["level"]

        self.__logger_obj = logging.getLogger("ISB")
        self.__logger_obj.setLevel(get_logger_level(logger_level))

        main_formatter = logging.Formatter(
            "%(asctime)-15s %(levelname)s %(message)s", "%Y-%m-%d %H:%M:%S"
        )
        main_formatter.converter = time.gmtime

        stream_handler = logging.StreamHandler(sys.stdout)
        stream_handler.setFormatter(main_formatter)
        self.__logger_obj.addHandler(stream_handler)

        logging.getLogger().setLevel(logging.ERROR)

    def info(self, msg: object, *args, **kwargs) -> None:
        """Prints log with level: info."""
        self.__logger_obj.info(msg, *args, **kwargs)

    def debug(self, msg: object, *args, **kwargs) -> None:
        """Prints log with level: debug."""
        self.__logger_obj.debug(msg, *args, **kwargs)

    def error(self, msg: object, *args, **kwargs) -> None:
        """Prints log with level: error."""
        self.__logger_obj.error(msg, *args, **kwargs)

    def fatal(self, msg: object, *args, **kwargs) -> None:
        """Prints log with level: fatal."""
        self.__logger_obj.fatal(msg, *args, **kwargs)

    def exception(self, msg: object, *args, **kwargs) -> None:
        """Prints error with exception information."""
        self.__logger_obj.exception(msg, *args, **kwargs)


def get_logger_level(level: str) -> int:
    """Returns log level integer from log level string."""
    level = str(level).lower().strip()

    if level == "fatal":
        return logging.FATAL

    if level == "error":
        return logging.ERROR

    if level == "info":
        return logging.INFO

    return logging.DEBUG
