# Module: Logger

The logger module contains the `Logger` class which wraps the standard `logging` library.

## When to use it?

The `Logger` class is used as a singleton across the entire project.
The reason for wrapping the `logging` library is to enable dependency injection (e.g. for test cases).
When multiple instances of the `Logger` class are initialized, it can lead to duplicate logs and unwanted behavior.

## How to use it?

```python
logger_config = {
    "level": <"one of: fatal, error, info or debug">
}

logger = Logger(config = logger_config)
```
