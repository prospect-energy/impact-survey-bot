from unittest import TestCase

from parsing import validation
from parsing.functions import set_database_value, set_variable
from result.error import ValidationException
from result.status import StatusCode
from structs.messages import TranslatedMessage
from structs.sections import (
    CompareSection,
    ConditionSection,
    ConfigSection,
    SkipSection,
    SpecifySection,
)
from structs.selection import SelectionOption
from survey.steps import ActionStep, MessageStep, SelectionStep
from survey.survey import Survey
from type_check.request_validation import is_text100


class MockPostgres:
    def select(*args, **_):
        return [("test_group",), ("default",)]


class MockLogger:
    def info(*args, **_):
        return

    def debug(*args, **_):
        return

    def error(*args, **_):
        return

    def fatal(*args, **_):
        return

    def exception(*args, **_):
        return


class FakeGeneralStep:
    def __init__(self, step_type):
        self.type = step_type


class FakeActionStep:
    def __init__(self, var_name):
        self.type = "action"
        self.function = set_variable
        self.params = {"var_name": var_name, "var_value": "test"}


mock_survey = Survey(
    "mock_survey",
    ConfigSection(False, None, None, False, None),
    {},
    {
        "START": FakeGeneralStep("message"),
        "1": FakeActionStep("some_variable"),
        "2": FakeGeneralStep("selection"),
        "3": FakeActionStep("other_variable"),
    },
)


def mock_compare_function(
    value: str,
    compare_with: float,
) -> bool:
    return value == str(compare_with)


def mock_status_function(value: str) -> StatusCode:
    if value:
        return StatusCode.SUCCESS
    return StatusCode.FAIL


def mock_float_function(number: int) -> str:
    return float(number)


class TestSurveyValidation(TestCase):
    def test_verify_variable_exists(self):
        # variables exist
        validation.verify_variable_exists(mock_survey, "{{ step.START }}", "START")
        validation.verify_variable_exists(
            mock_survey, "{{ request.media }}", "request.media"
        )
        validation.verify_variable_exists(
            mock_survey, "{{ session.uuid }}", "session.uuid"
        )
        validation.verify_variable_exists(mock_survey, "{{ time.now }}", "time.now")
        validation.verify_variable_exists(
            mock_survey, "{{ var.some_variable }}", "var.some_variable"
        )
        validation.verify_variable_exists(
            mock_survey, "{{ var.other_variable }}", "var.other_variable"
        )

        with self.assertRaises(ValidationException):
            # variables do not exist
            validation.verify_variable_exists(mock_survey, "{{ step.5 }}", "5")
            validation.verify_variable_exists(
                mock_survey, "{{ request.uuid }}", "request.uuid"
            )
            validation.verify_variable_exists(
                mock_survey, "{{ session.firstname }}", "session.firstname"
            )
            validation.verify_variable_exists(
                mock_survey, "{{ time.tomorrow }}", "time.tomorrow"
            )
            validation.verify_variable_exists(
                mock_survey, "{{ var.result }}", "var.result"
            )

    def test_validate_action_successors(self):
        with self.assertRaises(ValidationException):
            # output does not match successor keys
            annotations = mock_compare_function.__annotations__
            validation.validate_action_successors(
                annotations, {"success": "2", "fail": "3"}
            )

            annotations = mock_float_function.__annotations__
            validation.validate_action_successors(annotations, {"3.14": "END"})

        # output matches successor keys
        annotations = mock_compare_function.__annotations__
        validation.validate_action_successors(
            annotations, {"True": "2", "False": "END"}
        )

        annotations = mock_status_function.__annotations__
        validation.validate_action_successors(
            annotations, {"success": "1", "failure": "START", "invalid": "START"}
        )

        annotations = mock_float_function.__annotations__
        validation.validate_action_successors(annotations, {"*": "5"})

    def test_validate_action_variables(self):
        with self.assertRaises(ValidationException):
            # variable does not exist
            variables = {"value": ["{{ var.not_existing }}", "var.not_existing"]}

            validation.validate_action_variables(mock_survey, variables)

        # variables exist
        variables = {
            "value": ["{{ var.some_variable }}", "var.some_variable"],
            "dict": {
                "key": ["{{ var.other_variable }}", "var.other_variable"],
                "name": ["{{ step.3 }}", "3"],
            },
        }

        validation.validate_action_variables(mock_survey, variables)

    def test_validate_options(self):
        with self.assertRaises(ValidationException):
            # group is not available / does not exist
            options = {"not_existing_group": [], "default": []}
            validation.validate_options(mock_survey, options, ["default", "some_group"])

        with self.assertRaises(ValidationException):
            # variable in option output does not exist
            options = {
                "default": [
                    SelectionOption(None, "some_value", False, "12"),
                    SelectionOption(None, "step.10", True, "12"),
                ]
            }
            validation.validate_options(mock_survey, options, ["default"])

    def test_validate_params(self):
        with self.assertRaises(ValidationException):
            # function misses parameter
            annotations = mock_compare_function.__annotations__
            params = {"value": "test"}

            validation.validate_params(annotations, params)

        with self.assertRaises(ValidationException):
            # type of parameter doesn't match function annotation
            annotations = mock_compare_function.__annotations__
            params = {
                "value": "{{ request.input }}",
                "compare_with": "this is not a float",
            }

            validation.validate_params(annotations, params)

    def test_validate_compare(self):
        # No exception because compare is None
        validation.validate_compare(mock_survey, None)

        with self.assertRaises(ValidationException):
            # compare misses parameter
            compare = CompareSection(mock_compare_function, {"value": "test"}, {}, None)

            validation.validate_compare(mock_survey, compare)

        with self.assertRaises(ValidationException):
            # variable in compare params does not exist
            compare = CompareSection(
                mock_compare_function,
                {"value": "{{ var.not_existing }}", "compare_with": 3.14},
                {"value": ["{{ var.not_existing }}", "var.not_existing"]},
                None,
            )

            validation.validate_compare(mock_survey, compare)

        with self.assertRaises(ValidationException):
            # variable in compare message does not exist
            compare = CompareSection(
                mock_compare_function,
                {"value": "test", "compare_with": 3.14},
                {},
                TranslatedMessage({"en": "Hello {{ session.does_not_exist }}!"}),
            )

            compare.fail_message.variables = {
                "{{ session.does_not_exist }}": "session.does_not_exist"
            }

            validation.validate_compare(mock_survey, compare)

    def test_validate_condition(self):
        # No exception because condition is None
        validation.validate_condition(mock_survey, None)

        with self.assertRaises(ValidationException):
            # condition misses parameter
            condition = ConditionSection(
                mock_compare_function, {"value": "test"}, {}, None
            )

            validation.validate_condition(mock_survey, condition)

        with self.assertRaises(ValidationException):
            # variable in condition params does not exist
            condition = ConditionSection(
                mock_compare_function,
                {"value": "{{ var.not_existing }}", "compare_with": 3.14},
                {"value": ["{{ var.not_existing }}", "var.not_existing"]},
                None,
            )

            validation.validate_condition(mock_survey, condition)

    def test_validate_please_specify(self):
        # No exception because please_specify is None
        validation.validate_please_specify(mock_survey, None)

        with self.assertRaises(ValidationException):
            # {{ var.not_exisiting }} is not a valid variable
            message = TranslatedMessage(
                {
                    "en": "Hello {{ var.not_exisiting }}, how are you?",
                }
            )
            message.variables = {
                "{{ var.not_exisiting }}": "var.not_exisiting",
            }

            please_specify = SpecifySection(
                None, message, None, None, None, None, None, None, None
            )

            validation.validate_please_specify(mock_survey, please_specify)

        with self.assertRaises(ValidationException):
            # {{ session.user }} is not a valid variable
            message = TranslatedMessage(
                {
                    "en": "Hello!",
                }
            )
            help_ = TranslatedMessage(
                {
                    "en": "Hello {{ session.user }}!",
                }
            )

            help_.variables = {
                "{{ session.user }}": "session.user",
            }

            please_specify = SpecifySection(
                None, message, help_, None, None, None, None, None, None
            )

            validation.validate_please_specify(mock_survey, please_specify)

    def test_validate_variable_in_text(self):
        # No exception because translated_message is None
        validation.validate_variable_in_text(mock_survey, None)

        with self.assertRaises(ValidationException):
            # {{ session.user }} is not a valid variable
            message_1 = TranslatedMessage(
                {
                    "en": "Hello {{ session.name }}, how are you?",
                    "fr": "Hello {{ session.user }}, how are you?",
                }
            )
            message_1.variables = {
                "{{ session.name }}": "session.name",
                "{{ session.user }}": "session.user",
            }

            validation.validate_variable_in_text(mock_survey, message_1)

        message_2 = TranslatedMessage(
            {
                "en": "You sent: {{request.input}}",
                "fr": "You sent: {{ request.input }}",
            }
        )
        message_2.variables = {
            "{{request.input}}": "request.input",
            "{{ request.input }}": "request.input",
        }

        # {{ request.input }} is a valid variable
        validation.validate_variable_in_text(mock_survey, message_2)

    def test_get_linked_ids(self):
        steps = {
            "START": MessageStep(
                id_="START",
                variable_id="START",
                message=TranslatedMessage(messages={"en": "Something."}),
                modifiers=[],
                input_validation=is_text100,
                input_type="text-100",
                invalid_input_message=TranslatedMessage(messages={"en": "Error."}),
                successor="1",
                help_=TranslatedMessage(messages={"en": ""}),
                skip=SkipSection(True, "9"),
                condition=ConditionSection(lambda: True, {}, {}, "7"),
            ),
            "1": SelectionStep(
                id_="1",
                message=TranslatedMessage(messages={"en": "Selection:"}),
                options={
                    "default": [
                        SelectionOption(
                            TranslatedMessage(messages={"en": "Selection:"}),
                            "",
                            False,
                            "3",
                        )
                    ]
                },
                successor="4",
                help_=TranslatedMessage(messages={"en": ""}),
                skip=SkipSection(True, "8"),
                condition=ConditionSection(lambda: True, {}, {}, "5"),
            ),
            "3": ActionStep(
                id_="3",
                is_selection_action=False,
                function=set_database_value,
                params={"value": "{{ var.test_var }}"},
                variables={"value": ["{{ var.test_var }}", "var.test_var"]},
                condition=ConditionSection(lambda: True, {}, {}, "6"),
                successors={"success": "10", "fail": None},
            ),
        }

        res = validation.get_linked_ids(steps)
        self.assertDictEqual(
            res,
            {
                "1": "START",
                "10": "3",
                "3": "1",
                "4": "1",
                "5": "1",
                "6": "3",
                "7": "START",
                "8": "1",
                "9": "START",
            },
        )

    def test_has_missing_id(self):
        steps = {"START": None, "1": None, "3": None, "END": None}

        linked_ids = {
            "START": "test-dir/survey2.json/5",
            "1": "START",
            "2": "1",
            "3": "1",
            "END": "3",
        }

        step_id, linked_by_id = validation.has_missing_id(steps, linked_ids)
        self.assertEqual(step_id, "2")
        self.assertEqual(linked_by_id, "1")

        steps = {
            "1": None,
        }

        linked_ids = {
            "1": "START",
        }

        step_id, linked_by_id = validation.has_missing_id(steps, linked_ids)
        self.assertIsNone(step_id)
        self.assertIsNone(linked_by_id)

    def test_get_redundant_ids(self):
        steps = {"START": None, "1": None, "2": None, "END": None}

        linked_ids = {"START": "", "2": ""}

        res = validation.get_redundant_ids(steps, linked_ids)
        self.assertListEqual(res, ["1"])
