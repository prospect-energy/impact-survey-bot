from datetime import datetime
from unittest import TestCase

from type_check.datetime import (
    can_be_date,
    can_be_time,
    check_datetime_format,
    strp_date,
    strp_time,
)


class TestDatetime(TestCase):
    def test_strp_time(self):
        val = "12:00am"
        pattern = "%I:%M%p"
        expected = datetime.strptime(val, pattern)
        res = strp_time(val, [pattern])
        self.assertEqual(
            res, expected, f"{str(val)} can be parsed using pattern {pattern}"
        )

        val = "20:00"
        pattern = "%H:%M"
        expected = datetime.strptime(val, pattern)
        res = strp_time(val, [pattern])
        self.assertEqual(
            res, expected, f"{str(val)} can be parsed using pattern {pattern}"
        )

        with self.assertRaises(ValueError):
            val = "20:00"
            pattern = "%S"
            strp_time(val, [pattern])

    def test_strp_date(self):
        val = "15/02/1999"
        pattern = "%d/%m/%Y"
        expected = datetime.strptime(val, pattern)
        res = strp_date(val, [pattern])
        self.assertEqual(
            res, expected, f"{str(val)} can be parsed using pattern {pattern}"
        )

        val = "15.02.1999"
        pattern = "%d.%m.%Y"
        expected = datetime.strptime(val, pattern)
        res = strp_date(val, [pattern])
        self.assertEqual(
            res, expected, f"{str(val)} can be parsed using pattern {pattern}"
        )

        with self.assertRaises(ValueError):
            val = "20:00"
            pattern = "%S"
            strp_date(val, [pattern])

    def test_check_datetime_format(self):
        val = "12:00am"
        patterns = ["%d.%m.%Y", "%I:%M%p"]
        expected = datetime.strptime(val, "%I:%M%p")
        res = check_datetime_format(val, patterns)
        self.assertEqual(
            res, expected, f"{str(val)} can be parsed using pattern %I:%M%p"
        )

        time = "12:00am"
        val = datetime.strptime(time, "%I:%M%p")
        res = check_datetime_format(val, [])
        self.assertEqual(res, val, f"{str(val)} is already a datetime")

        with self.assertRaises(ValueError):
            val = "20:00"
            pattern = "%S"
            strp_date(val, [pattern])

        with self.assertRaises(ValueError):
            val = 5.32
            pattern = "%S"
            strp_date(val, [pattern])

    def test_can_be_date(self):
        val = datetime.strptime("15/02/1999", "%d/%m/%Y")
        res = can_be_date(val)
        self.assertTrue(res, f"{str(val)} is datetime")

        val = "15/02/1999"
        res = can_be_date(val)
        self.assertTrue(res, f"{str(val)} can be converted")

        val = "5.32"
        res = can_be_date(val)
        self.assertFalse(res, f"{str(val)} can't be converted to date")

    def test_can_be_time(self):
        val = datetime.strptime("12:00am", "%I:%M%p")
        res = can_be_time(val)
        self.assertTrue(res, f"{str(val)} is a datetime")

        val = "12:00am"
        res = can_be_time(val)
        self.assertTrue(res, f"{str(val)} can be converted")

        val = "5.32"
        res = can_be_time(val)
        self.assertFalse(res, f"{str(val)} can't be converted to date")
