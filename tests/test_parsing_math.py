from unittest import TestCase

from parsing import math


class TestMath(TestCase):
    def test_add(self):
        val1 = 16
        val2 = 32.1
        expected = 48.1
        res = math.add(val1, val2)
        self.assertEqual(res, expected, f"Result should be {expected}.")

        val1 = "16"
        val2 = "32.1"
        expected = 48.1
        res = math.add(val1, val2)
        self.assertEqual(res, expected, f"Result of strings should be {expected}.")

        val1 = ["32.1"]
        val2 = "16"
        expected = ["32.1"]
        res = math.add(val1, val2)
        self.assertEqual(res, expected, "Return val1 because can't convert a type.")

    def test_substract(self):
        val1 = 16.2
        val2 = 2
        expected = 14.2
        res = math.substract(val1, val2)
        self.assertEqual(res, expected, f"Result should be {expected}.")

        val1 = "16.2"
        val2 = "2"
        expected = 14.2
        res = math.substract(val1, val2)
        self.assertEqual(res, expected, f"Result of strings should be {expected}.")

        val1 = ["32.1"]
        val2 = "16"
        expected = ["32.1"]
        res = math.substract(val1, val2)
        self.assertEqual(res, expected, "Return val1 because can't convert a type.")

    def test_multiply(self):
        val1 = 16.2
        val2 = 2
        expected = 32.4
        res = math.multiply(val1, val2)
        self.assertEqual(res, expected, f"Result should be {expected}.")

        val1 = "16.2"
        val2 = "2"
        expected = 32.4
        res = math.multiply(val1, val2)
        self.assertEqual(res, expected, f"Result of strings should be {expected}.")

        val1 = ["32.1"]
        val2 = "16"
        expected = ["32.1"]
        res = math.multiply(val1, val2)
        self.assertEqual(res, expected, "Return val1 because can't convert a type.")

    def test_devide(self):
        val1 = 16.2
        val2 = 2
        expected = 8.1
        res = math.devide(val1, val2)
        self.assertEqual(res, expected, f"Result should be {expected}.")

        val1 = "16.2"
        val2 = "2"
        expected = 8.1
        res = math.devide(val1, val2)
        self.assertEqual(res, expected, f"Result of strings should be {expected}.")

        val1 = ["32.1"]
        val2 = "16"
        expected = ["32.1"]
        res = math.devide(val1, val2)
        self.assertEqual(res, expected, "Return val1 because can't convert a type.")
