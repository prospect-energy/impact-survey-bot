from unittest import TestCase

from type_check import typed_string


class TestTypedString(TestCase):
    def test_can_be_int_str(self):
        val = "4"
        res = typed_string.can_be_int_str(val)
        self.assertTrue(res, f"{val} can be converted to int.")

        val = "3.14"
        res = typed_string.can_be_int_str(val)
        self.assertFalse(res, f"{val} can't be converted to int.")

    def test_can_be_float_str(self):
        val = "4"
        res = typed_string.can_be_float_str(val)
        self.assertTrue(res, f"{val} can be converted to float.")

        val = "test"
        res = typed_string.can_be_float_str(val)
        self.assertFalse(res, f"{val} can't be converted to float.")

    def test_has_coordinates_str(self):
        lat = "5.34"
        lng = "2.35"
        res = typed_string.has_coordinates_str(lat, lng)
        self.assertTrue(res, "Lat and Lng are not empty.")

        lat = ""
        lng = ""
        res = typed_string.has_coordinates_str(lat, lng)
        self.assertFalse(res, "Lat or Lng are empty.")

    def test_is_url_str(self):
        val = "http://www.website.abc/news?search=test"
        res = typed_string.is_url_str(val)
        self.assertTrue(res, f"{val} is valid url.")

        val = "https://www.website.abc"
        res = typed_string.is_url_str(val)
        self.assertTrue(res, f"{val} is valid url.")

        val = "htps://www.website.abc"
        res = typed_string.is_url_str(val)
        self.assertFalse(res, f"{val} isn't a valid url.")

        val = "https://website/"
        res = typed_string.is_url_str(val)
        self.assertFalse(res, f"{val} isn't a valid url.")

    def test_phone_number_str(self):
        val = "+4900000000000"
        res = typed_string.is_phone_number_str(val)
        self.assertTrue(res, f"{val} is valid phone number.")

        val = "0049110"
        res = typed_string.is_phone_number_str(val)
        self.assertTrue(res, f"{val} is valid phone number.")

        val = "4900000000000"
        res = typed_string.is_phone_number_str(val)
        self.assertFalse(res, f"{val} isn't a valid phone number.")
