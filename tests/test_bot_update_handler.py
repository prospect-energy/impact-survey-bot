from unittest import TestCase

from bot import update_handler


class TestUpdateHandler(TestCase):
    def test_get_dict_from_db_array(self):
        db_array = [
            ("key1", "value1"),
            ("key2", "value2"),
            ("key3", "value3"),
        ]
        expected = {"key1": "value1", "key2": "value2", "key3": "value3"}

        res = update_handler.get_dict_from_db_array(db_array)
        self.assertDictEqual(res, expected)

    def test_convert_command_aliases(self):
        db_array = [
            ("start", "restart"),
            ("end", "end"),
            ("end", "fin"),
        ]
        expected = {
            "start": ["restart"],
            "end": ["end", "fin"],
        }

        res = update_handler.convert_command_aliases(db_array)
        self.assertDictEqual(res, expected)

    def test_convert_enabled_languages(self):
        db_array = [
            ("en", "English", "/english"),
            ("fr", "French", "/french"),
            ("sw", "Swahili", "/swahili"),
        ]
        expected = {
            "en": {"display_name": "English", "command": "/english"},
            "fr": {"display_name": "French", "command": "/french"},
            "sw": {"display_name": "Swahili", "command": "/swahili"},
        }

        res = update_handler.convert_enabled_languages(db_array)
        self.assertDictEqual(res, expected)
