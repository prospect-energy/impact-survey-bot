from unittest import TestCase

from parsing import parsing
from parsing.functions import (
    can_be_float,
    has_selected,
    is_between_including,
    set_database_value,
)
from parsing.modifier import comma_to_dot, remove_units, to_lower, to_upper
from parsing.survey_components import RawConditionStruct, RawSkipStruct
from result.error import SurveyException, ValidationException
from structs.context import WorkspaceContext
from structs.messages import TranslatedMessage

DEMO_IMAGE_1 = "http://a2ei.org/image1.png"
DEMO_IMAGE_2 = "http://a2ei.org/image2.png"
REQUEST_VARIABLE_TOKEN = "{{ request.input }}"


class MockPostgres:
    system_schema = "system"
    shared_schema = "shared"

    def create_table(*args, **_):
        return

    def migrate_table(*args, **_):
        return

    def select(*args, **_):
        return [("default",)]


class MockLogger:
    def info(*args, **_):
        return

    def debug(*args, **_):
        return

    def error(*args, **_):
        return

    def fatal(*args, **_):
        return

    def exception(*args, **_):
        return


class MockTranslations:
    default_language = "en"

    def get_translated_message(self, key: str):

        if key == "TEST_KEY_1":
            return TranslatedMessage({"en": "Message_1"})
        if key == "TEST_KEY_2":
            return TranslatedMessage({"en": "Message_2"})

        return TranslatedMessage({"en": "Message_3"})

    def get_invalid_input_tm(self, input_type: str):
        return TranslatedMessage({"en": f"Invalid {input_type}"})

    def get_survey_selection_tm(self):
        return TranslatedMessage({"en": "What is your aim?"})

    invalid_input_keys = {
        "text-100": "",
        "text-long": "",
        "integer": "",
        "decimal": "",
        "number": "",
        "location": "",
        "picture": "",
        "audio": "",
        "video": "",
        "document": "",
        "date": "",
        "time": "",
        "time-12": "",
        "time-24": "",
        "url": "",
        "phonenumber": "",
    }


class TestParsing(TestCase):
    def test_parse(self):
        workspace_ctx = WorkspaceContext(
            MockLogger, MockPostgres, None, 0, "", "", 0, "test_workspace", "en", 0, ""
        )
        config = {
            "registration": "registration",
            "selection": "selection",
            "surveys": {
                "registration": {
                    "data": {
                        "config": {
                            "create-db-entry": False,
                            "has-finish-message": False,
                        },
                        "steps": [
                            {
                                "id": "START",
                                "type": "message",
                                "message": "TEST_MESSAGE_1",
                                "help": "TEST_HELP_1",
                                "input-type": "text-long",
                                "successor": "$survey",
                            }
                        ],
                    },
                    "display_name": "Registration",
                    "group": "default",
                },
                "survey": {
                    "data": {
                        "config": {
                            "create-db-entry": True,
                            "table": "test_table",
                            "id-column": "test_id",
                            "has-finish-message": True,
                            "finish-message": "TEST_MESSAGE_3",
                        },
                        "steps": [
                            {
                                "id": "START",
                                "type": "message",
                                "message": "TEST_MESSAGE_1",
                                "help": "TEST_HELP_1",
                                "input-type": "text-long",
                                "db-column": "column_1",
                                "successor": "2",
                            },
                            {
                                "id": "2",
                                "type": "selection",
                                "message": "TEST_MESSAGE_2",
                                "help": "TEST_HELP_2",
                                "please-specify": {
                                    "option": "TEST_OPTION_1",
                                    "message": "TEST_PLEASE_SPECIFY_1",
                                    "input-type": "date",
                                    "successor": "3",
                                },
                                "options": [
                                    ["TEST_OPTION_2", "3", "{{ time.now }}"],
                                    ["TEST_OPTION_3", "3", "{{ time.yesterday }}"],
                                ],
                                "db-column": "column_2",
                            },
                            {
                                "id": "3",
                                "type": "action",
                                "function": "is-equal",
                                "params": {"compare_with": "test", "value": "test"},
                                "successors": {"*": "3"},
                            },
                        ],
                    },
                    "display_name": "Survey",
                    "group": "default",
                },
            },
        }

        surveys = parsing.parse(workspace_ctx, MockTranslations(), config)
        self.assertEqual(len(surveys), 3)

        test_survey = surveys["survey"]
        self.assertEqual(len(test_survey.steps), 20)
        self.assertDictEqual(
            test_survey.db_columns,
            {
                "column_1": "text-long",
                "column_2": "date",
                "created_at": "timestamp",
                "participant_id": "integer",
                "survey_duration": "decimal",
                "completed": "boolean",
            },
        )

    def test_parse_empty_survey(self):
        workspace_ctx = WorkspaceContext(
            MockLogger, MockPostgres, None, 0, "", "", 0, "test_workspace", "en", 0, ""
        )
        config = {
            "registration": "registration",
            "selection": "selection",
            "surveys": {
                "registration": {
                    "data": {
                        "config": {
                            "create-db-entry": False,
                            "has-finish-message": False,
                        },
                        "steps": [],
                    },
                    "display_name": "Registration",
                    "group": "default",
                }
            },
        }

        with self.assertRaises(SurveyException):
            parsing.parse(workspace_ctx, MockTranslations(), config)

    def test_parse_message(self):
        workspace_ctx = WorkspaceContext(
            MockLogger, MockPostgres, None, 0, "", "", 0, "test_workspace", "en", 0, ""
        )
        translations = MockTranslations()

        step_dict = {
            "id": "26-epc",
            "type": "message",
            "message": "TEST_KEY_1",
            "help": "TEST_KEY_2",
            "image": DEMO_IMAGE_1,
            "help-image": DEMO_IMAGE_2,
            "modify": ["to-lower", "comma-to-dot", "remove-units"],
            "input-type": "decimal",
            "skip": {"enabled": True, "jump-to": "30-epc"},
            "condition": {
                "operator": "has-selected",
                "value": ["electric hotplate"],
                "compare-with": "{{ step.22 }}",
                "jump-to": "35-epc-reason",
            },
            "compare": {
                "operator": ">=<",
                "compare-range": ["{{ step.20-epc }}", 20],
                "fail": "TEST_KEY_3",
            },
            "db-column": "epc_meter_start",
            "successor": "30-epc",
        }

        parsed_step = parsing.parse_message(workspace_ctx, translations, step_dict)

        message_step = parsed_step.step
        support_steps = parsed_step.support_steps
        db_column = parsed_step.db_column

        self.assertEqual(message_step.id_, "26-epc")
        self.assertEqual(message_step.variable_id, "26-epc")
        self.assertIsInstance(message_step.message, TranslatedMessage)
        self.assertIsInstance(message_step.help_, TranslatedMessage)
        self.assertEqual(
            message_step.message.get_message(workspace_ctx.default_language, "en"),
            "Message_1",
        )
        self.assertEqual(
            message_step.help_.get_message(workspace_ctx.default_language, "en"),
            "Message_2",
        )
        self.assertEqual(message_step.image, DEMO_IMAGE_1)
        self.assertEqual(message_step.help_image, DEMO_IMAGE_2)
        self.assertListEqual(
            message_step.modifiers, [to_lower, comma_to_dot, remove_units]
        )
        self.assertEqual(message_step.input_validation.__name__, can_be_float.__name__)
        self.assertEqual(message_step.input_type, "decimal")
        self.assertEqual(
            message_step.invalid_input_message.get_message(
                workspace_ctx.default_language, "en"
            ),
            "Invalid decimal",
        )
        self.assertIsNotNone(message_step.skip)
        self.assertIsNotNone(message_step.condition)
        self.assertTrue(message_step.skip.enabled)
        self.assertEqual(message_step.skip.successor, "30-epc")
        self.assertEqual(message_step.condition.func.__name__, has_selected.__name__)
        self.assertDictEqual(
            message_step.condition.params,
            {"value": ["electric hotplate"], "compare_with": "{{ step.22 }}"},
        )
        self.assertDictEqual(
            message_step.condition.variables, {"compare_with": ["{{ step.22 }}", "22"]}
        )
        self.assertEqual(message_step.condition.successor, "35-epc-reason")
        self.assertEqual(message_step.successor, "26-epc-comparison-action")
        self.assertEqual(db_column.name, "epc_meter_start")

        support_steps_dict = {}
        for step in support_steps:
            support_steps_dict[step.id_] = step.type

        self.assertDictEqual(
            support_steps_dict,
            {
                "26-epc-comparison-action": "action",
                "26-epc-comparison-fail": "message",
                "26-epc-set_db_value": "action",
            },
        )

    def test_parse_action(self):
        step_dict = {
            "id": "11",
            "type": "action",
            "function": "set-database-value",
            "params": {
                "table": "epc_urban",
                "where_column": "log_id",
                "where_value": "{{ var.log_id }}",
                "column": "nr_adults",
                "value": {"some_key": REQUEST_VARIABLE_TOKEN},
            },
            "condition": {
                "operator": "has-selected",
                "value": ["electric hotplate"],
                "compare-with": "{{ step.22 }}",
                "jump-to": "35-epc-reason",
            },
            "successors": {"success": "$TestSurvey", "invalid": None, "failure": "-1"},
        }

        action_obj = parsing.parse_action(step_dict)

        self.assertEqual(action_obj.id_, "11")
        self.assertFalse(action_obj.is_selection_action)
        self.assertEqual(action_obj.function.__name__, set_database_value.__name__)
        self.assertDictEqual(
            action_obj.params,
            {
                "table": "epc_urban",
                "where_column": "log_id",
                "where_value": "{{ var.log_id }}",
                "column": "nr_adults",
                "value": {"some_key": REQUEST_VARIABLE_TOKEN},
            },
        )
        self.assertDictEqual(
            action_obj.variables,
            {
                "where_value": ["{{ var.log_id }}", "var.log_id"],
                "value": {"some_key": [REQUEST_VARIABLE_TOKEN, "request.input"]},
            },
        )
        self.assertEqual(action_obj.condition.func.__name__, has_selected.__name__)
        self.assertDictEqual(
            action_obj.condition.params,
            {"value": ["electric hotplate"], "compare_with": "{{ step.22 }}"},
        )
        self.assertDictEqual(
            action_obj.condition.variables, {"compare_with": ["{{ step.22 }}", "22"]}
        )
        self.assertEqual(action_obj.condition.successor, "35-epc-reason")
        self.assertDictEqual(
            action_obj.successors,
            {"success": "$TestSurvey", "invalid": None, "failure": "-1"},
        )

    def test_parse_selection(self):
        workspace_ctx = WorkspaceContext(
            MockLogger, MockPostgres, None, 0, "", "", 0, "test_workspace", "en", 0, ""
        )
        translations = MockTranslations()

        step_dict = {
            "id": "35-epc-reason",
            "type": "selection",
            "message": "TEST_KEY_1",
            "image": DEMO_IMAGE_1,
            "skip": {"enabled": True, "jump-to": "32-epc"},
            "multiple-choice": True,
            "condition": {
                "operator": "has-selected",
                "value": ["lpg stove", "kerosene stove"],
                "compare-with": "{{ step.20 }}",
                "jump-to": "29-wood",
            },
            "help": "TEST_KEY_2",
            "help-image": DEMO_IMAGE_2,
            "please-specify": {
                "option": "TEST_KEY_3",
                "message": "TEST_KEY_1",
                "input-type": "time-12",
                "successor": "30-wood",
            },
            "options": [
                ["TEST_KEY_1", "26-wood", "don't know how", "aam"],
                ["TEST_KEY_2", "27-wood", "{{ step.1 }}", "smartmeters"],
                ["TEST_KEY_3", "28-wood", "multiple cooking devices", "epc"],
            ],
            "db-column": "non-electric-reason",
        }

        parsed_step = parsing.parse_selection(workspace_ctx, translations, step_dict)

        selection_step = parsed_step.step
        support_steps = parsed_step.support_steps
        db_column = parsed_step.db_column

        self.assertEqual(selection_step.id_, "35-epc-reason")
        self.assertIsInstance(selection_step.message, TranslatedMessage)
        self.assertIsInstance(selection_step.help_, TranslatedMessage)
        self.assertEqual(
            selection_step.message.get_message(workspace_ctx.default_language, "en"),
            "Message_1",
        )
        self.assertEqual(
            selection_step.help_.get_message(workspace_ctx.default_language, "en"),
            "Message_2",
        )
        self.assertEqual(selection_step.image, DEMO_IMAGE_1)
        self.assertEqual(selection_step.help_image, DEMO_IMAGE_2)
        self.assertEqual(
            selection_step.options["aam"][0].text.get_message(
                workspace_ctx.default_language, "en"
            ),
            "Message_1",
        )
        self.assertEqual(selection_step.options["aam"][0].output, "don't know how")
        self.assertFalse(selection_step.options["aam"][0].output_is_variable)
        self.assertEqual(
            selection_step.options["aam"][0].successor,
            "35-epc-reason-1-please-specify-check",
        )
        self.assertEqual(
            selection_step.options["smartmeters"][0].text.get_message(
                workspace_ctx.default_language, "en"
            ),
            "Message_2",
        )
        self.assertEqual(selection_step.options["smartmeters"][0].output, "step.1")
        self.assertTrue(selection_step.options["smartmeters"][0].output_is_variable)
        self.assertEqual(
            selection_step.options["smartmeters"][0].successor,
            "35-epc-reason-2-please-specify-check",
        )
        self.assertEqual(
            selection_step.options["epc"][0].text.get_message(
                workspace_ctx.default_language, "en"
            ),
            "Message_3",
        )
        self.assertEqual(
            selection_step.options["epc"][0].output, "multiple cooking devices"
        )
        self.assertFalse(selection_step.options["epc"][0].output_is_variable)
        self.assertEqual(
            selection_step.options["epc"][0].successor,
            "35-epc-reason-3-please-specify-check",
        )
        self.assertIsNotNone(selection_step.skip)
        self.assertIsNotNone(selection_step.condition)
        self.assertTrue(selection_step.skip.enabled)
        self.assertEqual(selection_step.skip.successor, "32-epc")
        self.assertEqual(selection_step.condition.func.__name__, has_selected.__name__)
        self.assertDictEqual(
            selection_step.condition.params,
            {"value": ["lpg stove", "kerosene stove"], "compare_with": "{{ step.20 }}"},
        )
        self.assertDictEqual(
            selection_step.condition.variables,
            {"compare_with": ["{{ step.20 }}", "20"]},
        )
        self.assertEqual(selection_step.condition.successor, "29-wood")
        self.assertEqual(selection_step.successor, "35-epc-reason-selection-action")
        self.assertEqual(db_column.name, "non-electric-reason")

        support_steps_dict = {}
        for step in support_steps:
            support_steps_dict[step.id_] = step.type

        self.assertDictEqual(
            support_steps_dict,
            {
                "35-epc-reason-selection-action": "action",
                "35-epc-reason-1-please-specify-check": "action",
                "35-epc-reason-1-please-specify": "message",
                "35-epc-reason-1-please-specify-output": "action",
                "35-epc-reason-1-please-specify-overwrite": "action",
                "35-epc-reason-1-please-specify-database": "action",
                "35-epc-reason-2-please-specify-check": "action",
                "35-epc-reason-2-please-specify": "message",
                "35-epc-reason-2-please-specify-output": "action",
                "35-epc-reason-2-please-specify-overwrite": "action",
                "35-epc-reason-2-please-specify-database": "action",
                "35-epc-reason-3-please-specify-check": "action",
                "35-epc-reason-3-please-specify": "message",
                "35-epc-reason-3-please-specify-output": "action",
                "35-epc-reason-3-please-specify-overwrite": "action",
                "35-epc-reason-3-please-specify-database": "action",
                "35-epc-reason-4-please-specify-check": "action",
                "35-epc-reason-4-please-specify": "message",
                "35-epc-reason-4-please-specify-output": "action",
                "35-epc-reason-4-please-specify-overwrite": "action",
                "35-epc-reason-4-please-specify-database": "action",
            },
        )

    def test_process_condition(self):
        invalid_operator = RawConditionStruct(
            {
                "operator": "some_fake_operator",
                "value": "{{ var.test_value }}",
                "compare-with": "{{ step.20 }}",
                "jump-to": "29-wood",
            }
        )

        with self.assertRaises(SurveyException):
            parsing.process_condition(invalid_operator)

        correct_condition = RawConditionStruct(
            {
                "operator": ">=<",
                "value": "{{ var.test_value }}",
                "compare-range": [0, "{{ step.20 }}"],
                "jump-to": "29-wood",
            }
        )

        res = parsing.process_condition(correct_condition)
        self.assertIsNotNone(res)
        self.assertEqual(res.func.__name__, is_between_including.__name__)
        self.assertDictEqual(
            res.params,
            {
                "value": "{{ var.test_value }}",
                "lower_bound": 0,
                "upper_bound": "{{ step.20 }}",
            },
        )
        self.assertDictEqual(
            res.variables,
            {
                "value": ["{{ var.test_value }}", "var.test_value"],
                "upper_bound": ["{{ step.20 }}", "20"],
            },
        )
        self.assertEqual(res.successor, "29-wood")

        condition_none = None

        res = parsing.process_condition(condition_none)
        self.assertIsNone(res)

    def test_process_action_function(self):
        res = parsing.get_action_function("set-database-value")
        self.assertEqual(res.__name__, set_database_value.__name__)

        with self.assertRaises(ValidationException):
            parsing.get_action_function("some-fake-function")

    def test_get_skip_section(self):
        skip_enabled = RawSkipStruct({"enabled": True, "jump-to": "32-epc"})

        res = parsing.get_skip_section(skip_enabled)
        self.assertIsNotNone(res)
        self.assertTrue(res.enabled)
        self.assertEqual(res.successor, "32-epc")

        skip_disabled = RawSkipStruct({"enabled": False, "jump-to": "32-epc"})

        res = parsing.get_skip_section(skip_disabled)
        self.assertIsNotNone(res)
        self.assertFalse(res.enabled)
        self.assertEqual(res.successor, "32-epc")

        skip_none = None

        res = parsing.get_skip_section(skip_none)
        self.assertIsNone(res)

    def test_get_modifier_functions(self):
        res = parsing.get_modifier_functions(["comma-to-dot", "to-upper"])
        self.assertEqual(res[0].__name__, comma_to_dot.__name__)
        self.assertEqual(res[1].__name__, to_upper.__name__)

        with self.assertRaises(ValidationException):
            parsing.get_modifier_functions(["comma-to-dot", "fake-modifier"])

    def test_get_var_token_and_name_incl_step_var(self):
        val = "{{ test.name        }}"
        expected_token = val
        expected_name = "test.name"
        res_token, res_name = parsing.get_var_token_and_name_incl_step_var(val)
        self.assertEqual(res_token, expected_token)
        self.assertEqual(res_name, expected_name)

        val = "{{ step.54-24/de }}"
        expected_token = val
        expected_name = "54-24/de"
        res_token, res_name = parsing.get_var_token_and_name_incl_step_var(val)
        self.assertEqual(res_token, expected_token)
        self.assertEqual(res_name, expected_name)
