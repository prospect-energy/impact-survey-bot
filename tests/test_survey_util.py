from datetime import datetime, timedelta
from unittest import TestCase

from result.status import NotSet
from survey import util


class TestSurveyUtil(TestCase):
    def test_get_variable_value(self):
        variables = {"session.name": "Test"}
        val = "Hello {{ session.name }}"
        var_name = "session.name"
        var_token = "{{ session.name }}"
        expected = "Hello Test"

        res = util.get_variable_value(variables, val, var_name, var_token)
        self.assertEqual(res, expected, "Variable is in dict and value is string.")

        variables = {}
        val = "{{ time.now }}"
        var_name = "time.now"
        var_token = "{{ time.now }}"
        date_format = "%d-%m-%Y %H:%M"
        expected = datetime.now().strftime(date_format)

        res = util.get_variable_value(variables, val, var_name, var_token)
        self.assertIsInstance(
            res, datetime, f"{var_name} should be resolved to datetime."
        )
        self.assertEqual(
            res.strftime(date_format),
            expected,
            f"{var_name} should resolve to current date and time.",
        )

        variables = {}
        val = "{{ time.yesterday }}"
        var_name = "time.yesterday"
        var_token = "{{ time.yesterday }}"
        date_format = "%d-%m-%Y %H:%M"
        expected = (datetime.now() - timedelta(days=1)).strftime(date_format)

        res = util.get_variable_value(variables, val, var_name, var_token)
        self.assertIsInstance(
            res, datetime, f"{var_name} should be resolved to datetime."
        )
        self.assertEqual(
            res.strftime(date_format),
            expected,
            f"{var_name} should resolve to yesterday's date and time.",
        )

        variables = {"session.uuid": 5}
        val = "Hello {{ session.uuid }}"
        var_name = "session.uuid"
        var_token = "{{ session.uuid }}"
        expected = "Hello 5"

        res = util.get_variable_value(variables, val, var_name, var_token)
        self.assertEqual(
            res, expected, "Return replaced string althrough variable isn't string."
        )

        variables = {"session.uuid": "Test"}
        val = "Hello {{ var.whatever }}"
        var_name = "var.whatever"
        var_token = "{{ var.whatever }}"

        res = util.get_variable_value(variables, val, var_name, var_token)
        self.assertIsInstance(
            res, NotSet, "Variable is not in dict - should return NotSet object."
        )
