import datetime
import unittest
import uuid
from decimal import Decimal
from typing import List

from psycopg2 import DatabaseError

from bot.bot import ImpactSurveyBot
from database.components import Column, OrderBy, Table, WhereClause
from database.database import Postgres
from database.testing import create_database, drop_database
from structs.messages import IncommingMessage, Media, MediaOrigin, ResponseMessage
from util import get_env

CREATED_BY = "integration test"
NO_SELECTION_OPTION = "Unfortunately, there are no selection options available for you. If you think this is by accident, please contact isb@a2ei.org."
DEMO_IMAGE = "https://apple_image.abc/apple.jpg"


class MockLogger:
    def info(*args, **_):
        return

    def debug(*args, **_):
        return

    def error(*args, **_):
        return

    def fatal(*args, **_):
        return

    def exception(*args, **_):
        return


class TestBot(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        create_database(
            cls.test_management_connection,
            cls.testing_database_name,
            "testing_template",
        )

    @classmethod
    def tearDownClass(cls):
        cls.setup_db.close()
        drop_database(cls.test_management_connection, cls.testing_database_name)

    system_schema = "system"
    shared_schema = "shared"

    testing_database_name = str(uuid.uuid4())
    test_management_connection = {
        "host": get_env("DB_TESTING_ADDRESS"),
        "port": get_env("DB_TESTING_PORT"),
        "database": "postgres",
        "user": get_env("DB_TEST_SETUP_USER"),
        "password": get_env("DB_TEST_SETUP_PASSWORD"),
    }

    setup_db = Postgres(
        {
            "host": get_env("DB_TESTING_ADDRESS"),
            "port": get_env("DB_TESTING_PORT"),
            "database": testing_database_name,
            "user": get_env("DB_TESTING_USER"),
            "password": get_env("DB_TESTING_PASSWORD"),
            "system_schema": system_schema,
            "shared_schema": shared_schema,
        },
        MockLogger(),
    )

    workspace_schema = "test_workspace"
    sessions_table = Table(system_schema, "sessions")
    participants_table = Table(shared_schema, "participants")
    group_allocation_table = Table(shared_schema, "group_allocation")
    group_invitations_table = Table(system_schema, "group_invitations")
    general_table = Table(workspace_schema, "test_survey_general")
    actions_table = Table(workspace_schema, "test_survey_actions")
    survey_tables = [general_table, actions_table]
    current_timestamp = datetime.datetime.now()

    s3_endpoint = get_env("S3_ENDPOINT")
    s3_bucket = get_env("S3_TESTING_BUCKET")

    bot_config = {
        "instance": {"instance_id": 1},
        "surveys": {"registration_survey": "registration"},
        "postgres": {
            "host": get_env("DB_TESTING_ADDRESS"),
            "port": get_env("DB_TESTING_PORT"),
            "database": testing_database_name,
            "user": get_env("DB_TESTING_USER"),
            "password": get_env("DB_TESTING_PASSWORD"),
            "system_schema": system_schema,
            "shared_schema": shared_schema,
        },
        "s3": {
            "enabled": "true",
            "access_key": get_env("S3_ACCESS_KEY"),
            "secret_key": get_env("S3_SECRET_ACCESS_KEY"),
            "endpoint": s3_endpoint,
            "bucket": s3_bucket,
            "localpath": "download",
        },
        "whatsapp_business": {
            "api_token": "",
            "phone_number_id": "",
        },
        "telegram": {
            "api_token": "",
        },
        "timeout": {"timeout_interval": 5},
        "logging": {"level": "ERROR"},
        "workspace_asset_key": "",
    }

    participants = [
        {
            "participant_id": 1,
            "identifier": None,
            "firstname": None,
            "lastname": None,
            "verification_code": "available_code",
            "is_registered": False,
            "language": "",
            "active": True,
            "custom_fields": "{}",
            "created_at": current_timestamp,
            "created_by": CREATED_BY,
        },
        {
            "participant_id": 2,
            "identifier": "english_user_2",
            "firstname": "english_user_2",
            "lastname": "name",
            "verification_code": "used_code",
            "is_registered": True,
            "language": "en",
            "active": True,
            "custom_fields": "{}",
            "created_at": current_timestamp,
            "created_by": CREATED_BY,
        },
        {
            "participant_id": 3,
            "identifier": "swahili_user_3",
            "firstname": "swahili_user_3's name",
            "lastname": "name",
            "verification_code": "some_code",
            "is_registered": True,
            "language": "sw",
            "active": True,
            "custom_fields": "{}",
            "created_at": current_timestamp,
            "created_by": CREATED_BY,
        },
    ]

    group_allocation = [
        {
            "group_id": 4,
            "participant_id": 1,
            "created_at": current_timestamp,
            "created_by": CREATED_BY,
        },
        {
            "group_id": 4,
            "participant_id": 2,
            "created_at": current_timestamp,
            "created_by": CREATED_BY,
        },
        {
            "group_id": 4,
            "participant_id": 3,
            "created_at": current_timestamp,
            "created_by": CREATED_BY,
        },
        {
            "group_id": 1,
            "participant_id": 2,
            "created_at": current_timestamp,
            "created_by": CREATED_BY,
        },
        {
            "group_id": 2,
            "participant_id": 3,
            "created_at": current_timestamp,
            "created_by": CREATED_BY,
        },
        {
            "group_id": 3,
            "participant_id": 3,
            "created_at": current_timestamp,
            "created_by": CREATED_BY,
        },
    ]

    group_invitations = [
        {
            "invitation_token": "not_expired",
            "group_id": 1,
            "expires": datetime.datetime(2099, 1, 1),
            "created_at": current_timestamp,
            "created_by": CREATED_BY,
        },
        {
            "invitation_token": "expired",
            "group_id": 1,
            "expires": datetime.datetime(2000, 1, 1),
            "created_at": current_timestamp,
            "created_by": CREATED_BY,
        },
    ]

    def setUp(self):
        self.remove_survey_tables()
        self.remove_session_entries()
        self.remove_group_allocation_entries()
        self.remove_participant_entries()
        self.remove_group_invitation_entries()

        self.setup_db.insert(self.participants_table, self.participants)
        self.setup_db.insert(self.group_allocation_table, self.group_allocation)
        self.setup_db.insert(self.group_invitations_table, self.group_invitations)

    def test_invitation_token_not_in_group(self):
        identifier = "swahili_user_3"
        bot = ImpactSurveyBot(self.bot_config)

        test_messages = [
            (
                IncommingMessage(identifier, "/invitation=expired"),
                ResponseMessage(
                    "message",
                    "Invitation token is invalid or expired.\n\nLe jeton d'invitation n'est pas valide ou a expiré.",
                ),
            ),
            (
                IncommingMessage(identifier, "/invitation=not_expired"),
                ResponseMessage(
                    "selection",
                    "What is your aim?\n Please reply with the number for the specific mode:",
                    None,
                    [
                        "Test-Survey: Actions",
                        "Test-Survey: General 2",
                        "Test-Survey: General 1",
                    ],
                    NO_SELECTION_OPTION,
                ),
            ),
        ]

        self.compare_responses(bot, test_messages)

        table = Table(self.shared_schema, "group_allocation")

        group_allocation_entries = self.setup_db.select(
            table=table,
            all_columns=False,
            columns=[Column(table, "group_id")],
            joined_tables=None,
            where_clauses=[WhereClause(Column(table, "participant_id"), 3)],
            order_by=None,
            group_by=None,
            fetch_all=True,
        )

        expected = [(1,), (2,), (3,), (4,)]

        self.assertCountEqual(group_allocation_entries, expected)

    def test_invitation_token_already_in_group(self):
        identifier = "english_user_2"
        bot = ImpactSurveyBot(self.bot_config)

        test_messages = [
            (
                IncommingMessage(identifier, "/invitation=not_expired"),
                ResponseMessage(
                    "message",
                    "Hello english_user_2 name! How many apples do you have?",
                    DEMO_IMAGE,
                ),
            ),
            (
                IncommingMessage(identifier, "3"),
                ResponseMessage(
                    "selection",
                    "What is your gender?",
                    None,
                    ["female", "male", "do not want to tell"],
                    NO_SELECTION_OPTION,
                ),
            ),
            (
                IncommingMessage(identifier, "/invitation=not_expired"),
                ResponseMessage(
                    "message",
                    "Invitation was successful. You can continue your current survey. To see the new survey, restart the chat by sending *start*.\n\n"
                    "L'invitation a été acceptée. Vous pouvez poursuivre l'enquête en cours. Pour voir la nouvelle enquête, redémarrez le chat en envoyant *start*.",
                ),
            ),
        ]

        self.compare_responses(bot, test_messages)

        table = Table(self.shared_schema, "group_allocation")

        group_allocation_entries = self.setup_db.select(
            table=table,
            all_columns=False,
            columns=[Column(table, "group_id")],
            joined_tables=None,
            where_clauses=[WhereClause(Column(table, "participant_id"), 2)],
            order_by=None,
            group_by=None,
            fetch_all=True,
        )

        expected = [(1,), (4,)]

        self.assertCountEqual(group_allocation_entries, expected)

    def test_registration_new_user(self):
        identifier = "new_user_1"
        bot = ImpactSurveyBot(self.bot_config)

        test_messages = [
            (
                IncommingMessage(identifier, "/start"),
                ResponseMessage(
                    "message",
                    "What is your verification code?\n\nQuel est votre code de vérification?",
                ),
            ),
            (
                IncommingMessage(identifier, "123"),
                ResponseMessage(
                    "message",
                    "Verification code is invalid, please try again:\n\n"
                    "Le code de vérification n'est pas valide, veuillez réessayer:",
                ),
            ),
            (
                IncommingMessage(identifier, "used_code"),
                ResponseMessage(
                    "message",
                    "Verification code is already in use, please contact the project supervisor.\n\nLe code de vérification est déjà utilisé, veuillez contacter le superviseur du projet.",
                ),
            ),
            (
                IncommingMessage(identifier, "available_code"),
                ResponseMessage(
                    "selection",
                    "I agree to the data protection declaration as described on the website a2ei.org/datenschutz \n \n In short: I allow the storage of my personal data (telephone number, name) on the A2EI servers, as well as the storage of the data given in the chat with the WA bot. \nPlease reply with one of the following options (numbers):",
                    None,
                    ["Yes", "No"],
                    NO_SELECTION_OPTION,
                ),
            ),
            (
                IncommingMessage(identifier, "2"),
                ResponseMessage(
                    "message",
                    "Unfortunately, under these circumstances we cannot guarantee you access to the bot.",
                ),
            ),
            (
                IncommingMessage(identifier, "ok"),
                ResponseMessage(
                    "message",
                    "Current session has been ended. Please type *start* to start a new session.",
                ),
            ),
            (
                IncommingMessage(identifier, "/start"),
                ResponseMessage(
                    "selection",
                    "I agree to the data protection declaration as described on the website a2ei.org/datenschutz \n \n In short: I allow the storage of my personal data (telephone number, name) on the A2EI servers, as well as the storage of the data given in the chat with the WA bot. \nPlease reply with one of the following options (numbers):",
                    None,
                    ["Yes", "No"],
                    NO_SELECTION_OPTION,
                ),
            ),
            (
                IncommingMessage(identifier, "1"),
                ResponseMessage("message", "Please send your first name:"),
            ),
            (
                IncommingMessage(identifier, "Test"),
                ResponseMessage("message", "Please send your last name:"),
            ),
            (
                IncommingMessage(identifier, "Participant"),
                ResponseMessage(
                    "selection",
                    "What is your prefered language? Please respond with one of the following *numbers*:",
                    None,
                    ["English", "Swahili", "French"],
                    NO_SELECTION_OPTION,
                ),
            ),
            (
                IncommingMessage(identifier, "/help"),
                ResponseMessage(
                    "message",
                    "Please answer with one of the given answers or the respective number.",
                ),
            ),
            (
                IncommingMessage(identifier, "4"),
                ResponseMessage(
                    "selection",
                    "What is your prefered language? Please respond with one of the following *numbers*:",
                    None,
                    ["English", "Swahili", "French"],
                    NO_SELECTION_OPTION,
                ),
            ),
            (
                IncommingMessage(identifier, "3"),
                ResponseMessage(
                    "selection",
                    "What is your aim?\n Please reply with the number for the specific mode:",
                    None,
                    [],
                    NO_SELECTION_OPTION,
                ),
            ),
        ]

        self.compare_responses(bot, test_messages)

        table = Table(self.shared_schema, "participants")
        columns = [
            Column(table, c)
            for c in ["identifier", "firstname", "lastname", "language"]
        ]

        participant_entry = self.setup_db.select(
            table=table,
            all_columns=False,
            columns=columns,
            joined_tables=None,
            where_clauses=[
                WhereClause(Column(table, "verification_code"), "available_code")
            ],
        )
        self.assertTupleEqual(
            participant_entry, ("new_user_1", "Test", "Participant", "fr")
        )

    def test_survey_general_english(self):
        identifier = "english_user_2"
        bot = ImpactSurveyBot(self.bot_config)

        test_messages = [
            (
                IncommingMessage(identifier, "."),
                ResponseMessage(
                    "message",
                    "Hello english_user_2 name! How many apples do you have?",
                    DEMO_IMAGE,
                ),
            ),
            (
                IncommingMessage(identifier, "35"),
                ResponseMessage(
                    "message",
                    "The number must be in the range of 0 and 20.",
                    DEMO_IMAGE,
                ),
            ),
            (
                IncommingMessage(identifier, "/back"),
                ResponseMessage(
                    "message",
                    "Hello english_user_2 name! How many apples do you have?",
                    DEMO_IMAGE,
                ),
            ),
            (
                IncommingMessage(identifier, "3.5"),
                ResponseMessage(
                    "message",
                    "Invalid input. Make sure that your message is a valid whole number such as *1* or *20*.",
                ),
            ),
            (
                IncommingMessage(identifier, "3"),
                ResponseMessage(
                    "selection",
                    "What is your gender?",
                    None,
                    ["female", "male", "do not want to tell"],
                    NO_SELECTION_OPTION,
                ),
            ),
            (
                IncommingMessage(identifier, "2,3"),
                ResponseMessage(
                    "selection",
                    "What is your gender?",
                    None,
                    ["female", "male", "do not want to tell"],
                    NO_SELECTION_OPTION,
                ),
            ),
            (
                IncommingMessage(identifier, "/skip"),
                ResponseMessage(
                    "selection",
                    "What is your gender?",
                    None,
                    ["female", "male", "do not want to tell"],
                    NO_SELECTION_OPTION,
                ),
            ),
            (
                IncommingMessage(identifier, "3"),
                ResponseMessage("message", "When is your birthday?"),
            ),
            (
                IncommingMessage(identifier, "10/02/2100"),
                ResponseMessage("message", "The birthday must be in the past."),
            ),
            (
                IncommingMessage(identifier, "10/02/20010"),
                ResponseMessage(
                    "message",
                    "Invalid input. Make sure that the date has the format dd/mm/yyyy or dd.mm.yyyy.",
                ),
            ),
            (
                IncommingMessage(identifier, "10/02/2001"),
                ResponseMessage(
                    "selection",
                    "What dish did you cook?",
                    None,
                    ["Water", "Beans", "Egg", "Other, please specify:"],
                    NO_SELECTION_OPTION,
                ),
            ),
            (
                IncommingMessage(identifier, "1,2,4"),
                ResponseMessage("message", "Please specify:"),
            ),
            (
                IncommingMessage(identifier, "something"),
                ResponseMessage(
                    "selection",
                    "For what did you heat up the water?",
                    None,
                    ["Tea", "Bath"],
                    NO_SELECTION_OPTION,
                ),
            ),
            (
                IncommingMessage(identifier, "3"),
                ResponseMessage(
                    "selection",
                    "For what did you heat up the water?",
                    None,
                    ["Tea", "Bath"],
                    NO_SELECTION_OPTION,
                ),
            ),
            (
                IncommingMessage(identifier, "1"),
                ResponseMessage("message", "What is the starting time?"),
            ),
            (
                IncommingMessage(identifier, "  14: 00"),
                ResponseMessage(
                    "message",
                    "Invalid input. Make sure that the time has the format hh:mm am/pm.",
                ),
            ),
            (
                IncommingMessage(identifier, "  2: 00 PM"),
                ResponseMessage("message", "What is the ending time?"),
            ),
            (
                IncommingMessage(identifier, "/help"),
                ResponseMessage("message", "Some time in the format 24:00."),
            ),
            (
                IncommingMessage(identifier, " 13:00 "),
                ResponseMessage("message", "Ending time must be after starting time."),
            ),
            (
                IncommingMessage(identifier, "23:00"),
                ResponseMessage(
                    "selection",
                    "Which appliance did you use?",
                    None,
                    ["Microwave", "Wood", "Electric Pressure Cooker"],
                    NO_SELECTION_OPTION,
                ),
            ),
            (
                IncommingMessage(identifier, "1,2,5"),
                ResponseMessage(
                    "selection",
                    "Which appliance did you use?",
                    None,
                    ["Microwave", "Wood", "Electric Pressure Cooker"],
                    NO_SELECTION_OPTION,
                ),
            ),
            (
                IncommingMessage(identifier, "     /HeLP "),
                ResponseMessage(
                    "message",
                    "Please reply with the applicable numbers separated by comma.",
                    "http://images.abc/appliance.png",
                ),
            ),
            (
                IncommingMessage(identifier, " 1.2"),
                ResponseMessage(
                    "message", "What is the appliance value before using it?"
                ),
            ),
            (
                IncommingMessage(identifier, "abc"),
                ResponseMessage(
                    "message",
                    "Invalid input. Make sure that your message is a valid number, including *.* for decimal.",
                ),
            ),
            (
                IncommingMessage(identifier, "/skip"),
                ResponseMessage(
                    "message", "What is the appliance value after using it?"
                ),
            ),
            (
                IncommingMessage(identifier, "2, 01 kg"),
                ResponseMessage("message", "Any comments?"),
            ),
            (
                IncommingMessage(
                    identifier,
                    "This text is way too long it must definetly exceed 100 characters which is 100 percent not allowed. Still I'm doing it to test something.",
                ),
                ResponseMessage(
                    "message",
                    "Invalid input. Make sure that your messages length is maximum 100 characters.",
                ),
            ),
            (
                IncommingMessage(identifier, "..."),
                ResponseMessage("message", "Please upload a photo:"),
            ),
            (
                IncommingMessage(
                    identifier,
                    "",
                    "",
                    "",
                    Media(
                        origin=MediaOrigin.TWILIO,
                        url="https://a2ei-image-hosting.s3.eu-central-1.amazonaws.com/TESTING/test_voice_message.ogg",
                        content_type="audio/ogg",
                    ),
                ),
                ResponseMessage(
                    "message",
                    "Invalid input. Please take or select a picture with your handy camera and send it to this chat.",
                ),
            ),
            (
                IncommingMessage(identifier, "aider"),
                ResponseMessage("message", "Just send it as photo in WhatsApp."),
            ),
            (
                IncommingMessage(
                    identifier,
                    "",
                    "",
                    "",
                    Media(
                        origin=MediaOrigin.TWILIO,
                        url="https://upload.wikimedia.org/wikipedia/commons/6/63/Wikipedia-logo.png",
                        content_type="image/png",
                    ),
                ),
                ResponseMessage("message", "Your serial code:"),
            ),
            (
                IncommingMessage(identifier, "09347"),
                ResponseMessage(
                    "message", "Must be a number in the format xxx or xxxx."
                ),
            ),
            (
                IncommingMessage(identifier, "9347"),
                ResponseMessage("message", "Please send your location."),
            ),
            (
                IncommingMessage(identifier, "", "53.1", "14.93"),
                ResponseMessage("message", "Thank you for filling out this survey!"),
            ),
        ]

        self.compare_responses(bot, test_messages)

        survey_table = Table(self.workspace_schema, "test_survey_general")

        db_entry = self.setup_db.select(
            table=survey_table,
            all_columns=True,
            columns=None,
            joined_tables=None,
            where_clauses=[WhereClause(Column(survey_table, "log_id"), 1)],
        )

        s3_enpoint_without_protocol = self.s3_endpoint.replace("https://", "")

        self.assertEqual(db_entry[0], 1)
        self.assertTrue(isinstance(db_entry[1], datetime.datetime))
        self.assertEqual(db_entry[2], 2)
        self.assertTrue(isinstance(db_entry[3], Decimal))
        self.assertTrue(db_entry[4])
        self.assertEqual(db_entry[5], 3)
        self.assertEqual(db_entry[6], "do not want to tell")
        self.assertEqual(db_entry[7], datetime.date(2001, 2, 10))
        self.assertEqual(db_entry[8], "heating water,beans,something")
        self.assertEqual(db_entry[9], "tea")
        self.assertEqual(db_entry[10], datetime.time(14, 0))
        self.assertEqual(db_entry[11], datetime.time(23, 0))
        self.assertEqual(db_entry[12], "microwave,wood")
        self.assertEqual(db_entry[13], None)
        self.assertEqual(db_entry[14], Decimal("2.01"))
        self.assertEqual(db_entry[15], "...")
        self.assertEqual(len(db_entry[16]), 40)
        self.assertTrue(db_entry[16].endswith(".png"))
        self.assertEqual(db_entry[17], Decimal("53.1"))
        self.assertEqual(db_entry[18], Decimal("14.93"))

    def test_actions_and_variables(self):
        identifier = "swahili_user_3"
        bot = ImpactSurveyBot(self.bot_config)

        test_messages = [
            (
                IncommingMessage(identifier, "."),
                ResponseMessage(
                    "selection",
                    "What is your aim?\n Please reply with the number for the specific mode:",
                    None,
                    ["Test-Survey: Actions", "Test-Survey: General 2"],
                    NO_SELECTION_OPTION,
                ),
            ),
            (
                IncommingMessage(identifier, "1"),
                ResponseMessage(
                    "message", "Which number do you want the faculty of? (0-20)"
                ),
            ),
            (
                IncommingMessage(identifier, "/help"),
                ResponseMessage(
                    "message", "Just send any real integer in a range of 0 to 20."
                ),
            ),
            (
                IncommingMessage(identifier, "21"),
                ResponseMessage(
                    "message", "Number must be in the range of 0 and 20! Try again:"
                ),
            ),
            (
                IncommingMessage(identifier, "-1"),
                ResponseMessage(
                    "message", "Number must be in the range of 0 and 20! Try again:"
                ),
            ),
            (
                IncommingMessage(identifier, "3"),
                ResponseMessage(
                    "message",
                    "The result is 6.0. Another number? Type *end* to end the session.",
                ),
            ),
            (
                IncommingMessage(identifier, "/end"),
                ResponseMessage(
                    "message",
                    "Kipindi cha sasa kimekamilika. Tafadhali andika *start* ili kuanza kipindi kipya.",
                ),
            ),
        ]

        self.compare_responses(bot, test_messages)

        with self.assertRaises(DatabaseError):
            self.setup_db.select(
                table=Table(self.workspace_schema, "test_survey_actions"),
                all_columns=True,
                columns=[],
                joined_tables=[],
                where_clauses=[],
            )

    def tearDown(self):
        self.remove_survey_tables()
        self.remove_session_entries()
        self.remove_group_allocation_entries()
        self.remove_participant_entries()

    def compare_responses(self, bot: ImpactSurveyBot, messages: List[tuple]):
        for idx, pair in enumerate(messages):
            req = pair[0]
            expected = pair[1]

            resp = bot.process(req)
            self.assertEqual(
                resp.type,
                expected.type,
                f"Types don't match in message pair {idx+1} | Returned message: {resp.message}",
            )
            self.assertEqual(
                resp.message,
                expected.message,
                f"Messages don't match in message pair {idx+1} | Returned message: {resp.message}",
            )
            self.assertEqual(
                resp.image,
                expected.image,
                f"Images don't match in message pair {idx+1} | Returned message: {resp.message}",
            )
            self.assertListEqual(
                resp.options,
                expected.options,
                f"Options don't match in message pair {idx+1} | Returned message: {resp.message}",
            )
            self.assertEqual(
                resp.option_error,
                expected.option_error,
                f"Option Errors don't match in message pair {idx+1} | Returned message: {resp.message}",
            )

    def remove_participant_entries(self):
        for participant_entry in self.participants:
            self.setup_db.delete(
                self.participants_table,
                [
                    WhereClause(
                        Column(self.participants_table, "verification_code"),
                        participant_entry["verification_code"],
                    )
                ],
            )

    def remove_session_entries(self):
        self.setup_db.delete(
            self.sessions_table,
            [WhereClause(Column(self.sessions_table, "logs"), None, "!=")],
        )

    def remove_group_allocation_entries(self):
        for group_entry in self.group_allocation:
            self.setup_db.delete(
                self.group_allocation_table,
                [
                    WhereClause(
                        Column(self.group_allocation_table, "group_id"),
                        group_entry["group_id"],
                    )
                ],
            )

    def remove_group_invitation_entries(self):
        for group_invitation_entry in self.group_invitations:
            self.setup_db.delete(
                self.group_invitations_table,
                [
                    WhereClause(
                        Column(self.group_invitations_table, "invitation_token"),
                        group_invitation_entry["invitation_token"],
                    )
                ],
            )

    def remove_survey_tables(self):
        for table in self.survey_tables:
            self.setup_db.drop_table(table)
