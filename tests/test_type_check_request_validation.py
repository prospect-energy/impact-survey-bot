from unittest import TestCase

from structs.messages import IncommingMessage, Media, MediaOrigin
from type_check import request_validation

TEST_VOICE_URL = "https://a2ei-image-hosting.s3.eu-central-1.amazonaws.com/TESTING/test_voice_message.ogg"
TEST_IMAGE_URL = (
    "https://a2ei-image-hosting.s3.eu-central-1.amazonaws.com/TESTING/testing_image.jpg"
)
TEST_VIDEO_URL = (
    "https://a2ei-image-hosting.s3.eu-central-1.amazonaws.com/TESTING/testing_video.mp4"
)
TEST_DOCUMENT_URL = "https://a2ei-image-hosting.s3.eu-central-1.amazonaws.com/TESTING/testing_document.pdf"


class TestRequestValidation(TestCase):
    def test_is_text100(self):
        msg = IncommingMessage("", "test", None, None, None)
        res = request_validation.is_text100(msg)
        self.assertTrue(res, "Message is less than 100 characters long.")

        msg = IncommingMessage(
            "",
            "test-test-test-test-test-test-test-test-test-test-test-test-test-test-test-test-test-test-test-test-test",
            None,
            None,
            None,
        )
        res = request_validation.is_text100(msg)
        self.assertFalse(res, "Message is more than 100 characters long.")

    def test_can_be_int(self):
        msg = IncommingMessage("", "4", None, None, None)
        res = request_validation.can_be_int(msg)
        self.assertTrue(res, f"{msg.message} can be converted to int.")

        msg = IncommingMessage("", "3.14", None, None, None)
        res = request_validation.can_be_int(msg)
        self.assertFalse(res, f"{msg.message} can't be converted to int.")

    def test_can_be_float(self):
        msg = IncommingMessage("", "4", None, None, None)
        res = request_validation.can_be_float(msg)
        self.assertTrue(res, f"{msg.message} can be converted to float.")

        msg = IncommingMessage("", "test", None, None, None)
        res = request_validation.can_be_float(msg)
        self.assertFalse(res, f"{msg.message} can't be converted to float.")

    def test_has_coordinates(self):
        lat = "5.34"
        lng = "2.35"
        msg = IncommingMessage("", "", lat, lng, None)
        res = request_validation.has_coordinates(msg)
        self.assertTrue(res, "Lat and Lng are not empty.")

        lat = ""
        lng = ""
        msg = IncommingMessage("", "", lat, lng, None)
        res = request_validation.has_coordinates(msg)
        self.assertFalse(res, "Lat or Lng are empty.")

    def test_has_image(self):
        media = Media(origin=MediaOrigin.TWILIO, content_type="image/jpeg")
        msg = IncommingMessage("", None, None, None, media)
        res = request_validation.has_image(msg)
        self.assertTrue(res, "Media parameter is jpg file.")

        media = Media(MediaOrigin.TWILIO, content_type="audio/ogg")
        msg = IncommingMessage("", None, None, None, media)
        res = request_validation.has_image(msg)
        self.assertFalse(res, "Media parameter is voice message.")

        media = Media(MediaOrigin.TWILIO, content_type=None)
        msg = IncommingMessage("", None, None, None, media)
        res = request_validation.has_image(msg)
        self.assertFalse(res, "Media parameter is empty.")

    def test_has_audio(self):
        media = Media(MediaOrigin.TWILIO, content_type="audio/ogg")
        msg = IncommingMessage("", None, None, None, media)
        res = request_validation.has_audio(msg)
        self.assertTrue(res, "Media parameter is ogg file.")

        media = Media(MediaOrigin.TWILIO, content_type="application/pdf")
        msg = IncommingMessage("", None, None, None, media)
        res = request_validation.has_audio(msg)
        self.assertFalse(res, "Media parameter is document.")

        media = Media(MediaOrigin.TWILIO, content_type=None)
        msg = IncommingMessage("", None, None, None, media)
        res = request_validation.has_audio(msg)
        self.assertFalse(res, "Media parameter is empty.")

    def test_has_video(self):
        media = Media(MediaOrigin.TWILIO, content_type="video/mp4")
        msg = IncommingMessage("", None, None, None, media)
        res = request_validation.has_video(msg)
        self.assertTrue(res, "Media parameter is mp4 file.")

        media = Media(MediaOrigin.TWILIO, content_type="image/jpeg")
        msg = IncommingMessage("", None, None, None, media)
        res = request_validation.has_video(msg)
        self.assertFalse(res, "Media parameter is picture.")

        media = Media(MediaOrigin.TWILIO, content_type=None)
        msg = IncommingMessage("", None, None, None, media)
        res = request_validation.has_video(msg)
        self.assertFalse(res, "Media parameter is empty.")

    def test_has_document(self):
        media = Media(MediaOrigin.TWILIO, content_type="application/pdf")
        msg = IncommingMessage("", None, None, None, media)
        res = request_validation.has_document(msg)
        self.assertTrue(res, "Media parameter is pdf file.")

        media = Media(MediaOrigin.TWILIO, content_type="audio/ogg")
        msg = IncommingMessage("", None, None, None, media)
        res = request_validation.has_document(msg)
        self.assertFalse(res, "Media parameter is voice message.")

        media = Media(MediaOrigin.TWILIO, content_type=None)
        msg = IncommingMessage("", None, None, None, media)
        res = request_validation.has_document(msg)
        self.assertFalse(res, "Media parameter is empty.")

    def test_is_date(self):
        val = "19/02/2000"
        msg = IncommingMessage("", val, None, None, None)
        res = request_validation.is_date(msg)
        self.assertTrue(res, f"{val} is a date.")

        val = "2000-02-19"
        msg = IncommingMessage("", val, None, None, None)
        res = request_validation.is_date(msg)
        self.assertTrue(res, f"{val} is a date.")

        val = "20-05-2000-1"
        msg = IncommingMessage("", val, None, None, None)
        res = request_validation.is_date(msg)
        self.assertFalse(res, f"{val} isn't a date.")

    def test_is_time(self):
        val = "12:00am"
        msg = IncommingMessage("", val, None, None, None)
        res = request_validation.is_time(msg)
        self.assertTrue(res, f"{val} is a time.")

        val = "21:00"
        msg = IncommingMessage("", val, None, None, None)
        res = request_validation.is_time(msg)
        self.assertTrue(res, f"{val} is a time.")

        val = "64:00"
        msg = IncommingMessage("", val, None, None, None)
        res = request_validation.is_time(msg)
        self.assertFalse(res, f"{val} isn't a time.")

    def test_is_time12(self):
        val = "12:00am"
        msg = IncommingMessage("", val, None, None, None)
        res = request_validation.is_time12(msg)
        self.assertTrue(res, f"{val} is a 12h time.")

        val = "20:00"
        msg = IncommingMessage("", val, None, None, None)
        res = request_validation.is_time12(msg)
        self.assertFalse(res, f"{val} isn't a 12h time.")

    def test_is_time24(self):
        val = "5:00"
        msg = IncommingMessage("", val, None, None, None)
        res = request_validation.is_time24(msg)
        self.assertTrue(res, f"{val} is a 24h time.")

        val = "5:00am"
        msg = IncommingMessage("", val, None, None, None)
        res = request_validation.is_time24(msg)
        self.assertFalse(res, f"{val} isn't a 24h time.")

    def test_is_url(self):
        val = "http://www.website.abc/news?search=test"
        msg = IncommingMessage("", val, None, None, None)
        res = request_validation.is_url(msg)
        self.assertTrue(res, f"{val} is valid url.")

        val = "htps://www.website.abc"
        msg = IncommingMessage("", val, None, None, None)
        res = request_validation.is_url(msg)
        self.assertFalse(res, f"{val} isn't a valid url.")

    def test_phone_number(self):
        val = "+4900000000000"
        msg = IncommingMessage("", val, None, None, None)
        res = request_validation.is_phone_number(msg)
        self.assertTrue(res, f"{val} is valid phone number.")

        val = "0049110"
        msg = IncommingMessage("", val, None, None, None)
        res = request_validation.is_phone_number(msg)
        self.assertTrue(res, f"{val} is valid phone number.")

        val = "4900000000000"
        msg = IncommingMessage("", val, None, None, None)
        res = request_validation.is_phone_number(msg)
        self.assertFalse(res, f"{val} isn't a valid phone number.")
