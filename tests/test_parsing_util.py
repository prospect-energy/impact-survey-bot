from unittest import TestCase

from parsing import util


class TestParsingUtil(TestCase):
    def test_get_var_token_and_name(self):
        val = "{{test.name}}"
        expected_token = val
        expected_name = "test.name"
        res_token, res_name = util.get_var_token_and_name(val)
        self.assertEqual(
            res_token, expected_token, f"Expected {expected_token} as token."
        )
        self.assertEqual(res_name, expected_name, f"Expected {expected_name} as name.")

        val = "         {{  test.name  }}     "
        expected_token = "{{  test.name  }}"
        expected_name = "test.name"
        res_token, res_name = util.get_var_token_and_name(val)
        self.assertEqual(
            res_token, expected_token, f"Expected {expected_token} as token."
        )
        self.assertEqual(res_name, expected_name, f"Expected {expected_name} as name.")

        val = "test.name"
        expected_token = None
        expected_name = None
        res_token, res_name = util.get_var_token_and_name(val)
        self.assertEqual(
            res_token,
            expected_token,
            f"Expected {expected_token} as token because no variable detected.",
        )
        self.assertEqual(
            res_name,
            expected_name,
            f"Expected {expected_name} as name because no variable detected.",
        )

    def test_get_id_from_step_variable_name(self):
        var_name = "step.heureka"
        expected = "heureka"
        res = util.get_id_from_step_variable_name(var_name)
        self.assertEqual(res, expected, f"Expected {expected} as new id.")

    def test_safe_get(self):
        list_ = ["test", "heureka", 5, "liebe"]

        idx = 2
        default = "default"
        expected = 5
        res = util.safe_get(list_, idx, default)
        self.assertEqual(res, expected, "Is a list and index exists.")

        idx = 5
        default = "default"
        expected = "default"
        res = util.safe_get(list_, idx, default)
        self.assertEqual(res, expected, "Is a list but index is out of range.")

        list_ = "list"

        idx = 2
        default = "default"
        expected = "s"
        res = util.safe_get(list_, idx, default)
        self.assertEqual(res, expected, "Is a string and index exists.")

        list_ = ("test1", "test2")

        idx = 1
        default = "default"
        expected = "test2"
        res = util.safe_get(list_, idx, default)
        self.assertEqual(res, expected, "Is a tuple and index exists.")

        list_ = 5

        with self.assertRaises(ValueError):
            idx = 2
            default = "default"
            util.safe_get(list_, idx, default)
