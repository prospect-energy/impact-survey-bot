"""Contains util functions for steps."""
from datetime import datetime, timedelta
from typing import Any, List

from result.status import NotSet


def get_variable_value(
    session_variables: List[str],
    value: str,
    var_name: str,
    var_token: str,
) -> Any:
    """Returns value with variable token replaced by value of variable.
    Returns NotSet object if variable is not contained in session-variables.
    """

    if var_name == "time.now":
        return datetime.now()
    if var_name == "time.yesterday":
        return datetime.now() - timedelta(days=1)

    replace_value = session_variables.get(var_name, NotSet())
    if type(replace_value) in [str, int, float]:
        return value.replace(var_token, str(replace_value))

    return replace_value
