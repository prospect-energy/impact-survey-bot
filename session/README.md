# Module: Session

The session module contains the `Session` and `SessionHandler` classes. Both classes are used to make the surveys interactable and manage both participant progress ("At which step is the participant currently?") and replies/input. Hereby, each participant sending a message to the bot is given a `Session` object which contains the participants data and progress.
The `SessionHandler` manages all incoming messages for a workspace and returns a response based on the session data, message content and step results. Depending on the workspace configuration, sessions will timeout if no participant interaction was detected for a specific amount of time.

## When to use it?

Both `SessionHandler` and `Session` are associated with a workspace. While `SessionHandler` is only used in this context, `Session` objects are also used in `SessionContext` objects and are therefore referred across the entire codebase.

## How to use it?


Examples for SessionHandler and Session initialization can be found here: 

* [workspace_handler.py](../bot/workspace_handler.py)
* [session_handler.py](./session_handler.py)

