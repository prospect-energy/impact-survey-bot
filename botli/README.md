# Module: Botli

`Botli` is the CLI ("command-line interface") for the ImpactSurveyBot. It is used to test the bot functionality manually and locally. While there are automatic test-cases to ensure the expected funtionality of both Bot and codebase, `Botli` allows for the direct chatting with the Bot (similar to a WhatsApp chat). It can be used to explore how the usage of surveys feels and if there are any undiscovered logical errors or misspellings. When using `Botli`, there are no additional costs created (unlike using WhatsApp).

## When to use it?

If you want to test out functionality which is not covered by a test-case yet or if you want to check how a survey feels when using it.

## How to use it?

1. Open a new console window and make sure that you're in the project folder.
2. Go to the botli directory (`cd botli`).
3. Open the python virtual environment (`poetry shell`).

> Botli allows for specifying the phone number (default: `+4900000000000`) and server-address (default: `http://localhost:8000/twilio`). If you want to use a different phone number, you can start Botli with the flags `-n <phone number>`. For changing the server-address (for example to use a development or production Botli instance), use `-a <address>`.

1. Start Botli (`python botli.py <flags>`).
2. When the server is running and the address is valid, you should see a message with some short introduction. If you need more help regarding the flags, you can use this command to start Botli: `python botli.py -h`.
3. Just write any message or use one of the specified commands to start a chat.

> If you make an update to your local bot instance (localhost) locally, make sure to restart the server to make the changes effective. Botli does not need to be restarted unless you make changes to Botli directly.


