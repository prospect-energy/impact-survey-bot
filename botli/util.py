from typing import Any, List


def safe_get(l: List, index: int, default: Any) -> Any:
    try:
        return l[index]
    except IndexError:
        return default


def print_output(string: str) -> None:
    print(f"\u001b[31m>\u001b[0m {string}")
