import html
import os
import re
import sys

import requests

from util import print_output, safe_get


def run(address: str, phone_number: str) -> None:
    latitude_default: str = "0.0"
    longitude_default: str = "0.0"
    image_default: str = "https://a2ei-image-hosting.s3.eu-central-1.amazonaws.com/TESTING/testing_image.jpg"
    voice_message_default: str = "https://a2ei-image-hosting.s3.eu-central-1.amazonaws.com/TESTING/test_voice_message.ogg"
    video_default: str = "https://a2ei-image-hosting.s3.eu-central-1.amazonaws.com/TESTING/testing_video.mp4"
    document_default: str = "https://a2ei-image-hosting.s3.eu-central-1.amazonaws.com/TESTING/testing_document.pdf"

    try:
        requests.get(address)
    except requests.exceptions.ConnectionError:
        print(f"Server is not running on address: {address}")
        return

    print(
        f"""
        \rBotli - Support Tool for Impact-Survey-Bot
        \r© Access to Energy Institute, Lukas Müller

        \rUsage:
        \rWrite in your commandline as you would in WhatsApp. For coordinates and image input use the commands below.

        \rCommands:
        \r  #c / #coordinates <lat> <long>      Latitude and Longitude values for location data | default: lat={latitude_default} long={longitude_default}\n
        \r  #i / #image <url>                   Image url for sending an image to the bot | default: {image_default}\n
        \r  #vo / #voice <url>                  OGG url for sending a voice message to the bot | default: {voice_message_default}\n
        \r  #vi / #video <url>                  MP4 url for sending a video to the bot | default: {video_default}\n
        \r  #d / #document <url>                PDF url for sending a document to the bot | default: {document_default}

        \rLet's get started:\n
        """
    )

    while True:
        latitude: str = ""
        longitude: str = ""
        media_url: str = ""
        content_type: str = ""
        message: str = ""

        input_str = ""
        input_decoration = "\u001b[32m<\u001b[0m "
        try:
            input_str = input(input_decoration)
        except UnicodeDecodeError:
            continue

        if input_str.startswith("#coordinates") or input_str.startswith("#c"):
            input_parts = input_str.split(" ")

            latitude = safe_get(input_parts, 1, latitude_default)
            longitude = safe_get(input_parts, 2, longitude_default)

            latitude = latitude.replace(",", ".")
            longitude = longitude.replace(",", ".")

        elif input_str.startswith("#image") or input_str.startswith("#i"):
            input_parts = input_str.split(" ")
            media_url = safe_get(input_parts, 1, image_default)

        elif input_str.startswith("#voice") or input_str.startswith("#vo"):
            input_parts = input_str.split(" ")
            media_url = safe_get(input_parts, 1, voice_message_default)

        elif input_str.startswith("#video") or input_str.startswith("#vi"):
            input_parts = input_str.split(" ")
            media_url = safe_get(input_parts, 1, video_default)

        elif input_str.startswith("#document") or input_str.startswith("#d"):
            input_parts = input_str.split(" ")
            media_url = safe_get(input_parts, 1, document_default)

        else:
            message = input_str

        try:
            if media_url:
                resp = requests.head(media_url)
                content_type = resp.headers.get("content-type")
        except requests.exceptions.ConnectionError:
            print(f"Defined Media URL is not reachable.")
            continue

        try:
            resp = requests.post(
                url=address,
                data={
                    "From": f"whatsapp:{phone_number}",
                    "Body": message,
                    "Latitude": latitude,
                    "Longitude": longitude,
                    "MediaUrl0": media_url,
                    "MediaContentType0": content_type,
                },
            )
        except requests.exceptions.ConnectionError:
            print(f"Server stopped running on address: {address}")
            break

        if resp.status_code != 200:
            print_output(f"An error occured, status code: {resp.status_code}")

        resp_str = "<no xml>"

        # regex: https://regex101.com/r/06nFeD/3
        pattern = (
            r"^(<\?xml.*[?]><Response><Message>)([\s\S]*)(<\/Message><\/Response>)$"
        )
        match = re.match(pattern, str(resp.text))
        if match:
            resp_str = match.group(2)
            resp_str = html.unescape(resp_str)
            print_output(resp_str)
            continue

        pattern = r"^(<\?xml.*[?]><Response\s*?\\>)"
        match = re.match(pattern, str(resp.text))
        if match:
            resp_str = "<no response>"
            print_output(resp_str)


if __name__ == "__main__":
    address: str = "http://localhost:{port}/twilio"
    port: int = 8000
    phone_number: str = "+4900000000000"

    if len(sys.argv) > 1:
        skip = ["botli.py", "botli"]

        for i, arg in enumerate(sys.argv):
            if arg in skip:
                continue

            if arg == "-a" or arg == "--address":
                if len(sys.argv) >= i:
                    address = sys.argv[i + 1]
                    skip.append(address)

            elif arg == "-p" or arg == "--port":
                if len(sys.argv) >= i:
                    port = sys.argv[i + 1]
                    skip.append(port)

            elif arg == "-n" or arg == "--number":
                if len(sys.argv) >= i:
                    phone_number = sys.argv[i + 1]
                    skip.append(phone_number)

            else:
                print(
                    f"""
                    \rBotli - Support Tool for Impact-Survey-Bot
                    \r© Access to Energy Institute, Lukas Müller

                    \rUsage:
                    \rpython3 botli.py <flag> <value>

                    \r-a   --address        HTTP-Address of running server | default: {address}

                    \r-p   --port           Port within default HTTP-address | default: {port}
                    
                    \r-n   --number         Phone-Number of participant for bot communication | default: {phone_number}
                    """
                )
                sys.exit()

    address = address.format(port=port)

    try:
        run(address, phone_number)
    except KeyboardInterrupt:
        print("\rBotli has been stopped.")
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)
