# Module: Result

The result module contains classes/custom exceptions to indicate the result of a function/operation.

## When to use it?

When you want to indicate the result of a function and can't rely on the output alone,
you can use the classes and exceptions of this module.
