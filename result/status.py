"""Contains classes associated with providing status information."""
from enum import Enum


class NotSet:
    """Class to indicate that variable is not set in survey."""

    def __str__(self) -> str:
        return ""


class StatusCode(Enum):
    """Enum to indicate the result of a function without specific return value."""

    SUCCESS = "success"
    INVALID = "invalid"
    FAIL = "failure"

    @classmethod
    def has_value(cls, value: str) -> bool:
        """Returns if value is valid status code"""
        return value in cls._value2member_map_
