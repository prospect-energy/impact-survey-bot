"""Contains util functions for app.py ."""
import os

from result.error import ConfigException


def get_bot_config():
    """Returns structured bot configuration."""
    return {
        "instance": {"instance_id": get_env("INSTANCE_ID")},
        "surveys": {"registration_survey": get_env("REGISTRATION_SURVEY")},
        "postgres": {
            "host": get_env("DB_ADDRESS"),
            "port": get_env("DB_PORT"),
            "database": get_env("DB_NAME"),
            "user": get_env("DB_USER"),
            "password": get_env("DB_PASSWORD"),
            "system_schema": get_env("DB_SYSTEM_SCHEMA"),
            "shared_schema": get_env("DB_SHARED_SCHEMA"),
        },
        "s3": {
            "enabled": get_env("S3_ENABLED"),
            "access_key": get_env("S3_ACCESS_KEY"),
            "secret_key": get_env("S3_SECRET_ACCESS_KEY"),
            "endpoint": get_env("S3_ENDPOINT"),
            "bucket": get_env("S3_BUCKET"),
            "localpath": get_env("DOWNLOAD_PATH"),
        },
        "whatsapp_business": {
            "api_token": get_env("WAB_API_TOKEN"),
            "phone_number_id": get_env("WAB_PHONE_NUMBER_ID"),
        },
        "telegram": {
            "api_token": get_env("TELEGRAM_API_TOKEN"),
        },
        "timeout": {"timeout_interval": get_env("TIMEOUT_INTERVAL")},
        "logging": {"level": get_env("LOG_LEVEL")},
    }


def get_env(key: str):
    """Convenience function to safely get value of environment variable."""
    env_value = os.getenv(key)
    if env_value:
        return env_value

    raise ConfigException(f"Environment variable {key} is not set.")
