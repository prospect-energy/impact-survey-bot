"""Contains request model for Workspace Updates."""
from pydantic import BaseModel


class UpdateRequest(BaseModel):
    """Class for conversion and validation of workspace-update request."""

    workspace_id: int
