"""Contains request models for Twilio."""
from typing import ClassVar, List, Optional, Tuple

from pydantic import BaseModel, Field

from structs.messages import Media, MediaOrigin, MediaType


class User(BaseModel):
    user_id: int = Field(..., alias="id")


class Audio(BaseModel):
    file_id: str
    media_type: ClassVar[MediaType] = MediaType.AUDIO


class Document(BaseModel):
    file_id: str
    media_type: ClassVar[MediaType] = MediaType.DOCUMENT


class Photo(BaseModel):
    file_id: str
    media_type: ClassVar[MediaType] = MediaType.IMAGE


class Video(BaseModel):
    file_id: str
    media_type: ClassVar[MediaType] = MediaType.VIDEO


class Voice(BaseModel):
    file_id: str
    media_type: ClassVar[MediaType] = MediaType.AUDIO


class Location(BaseModel):
    latitude: float
    longitude: float


class Message(BaseModel):
    user: Optional[User] = Field(..., alias="from")
    text: Optional[str] = None
    audio: Optional[Audio] = None
    document: Optional[Document] = None
    photo: Optional[List[Photo]] = []
    video: Optional[Video] = None
    voice: Optional[Voice] = None
    location: Optional[Location] = None
    caption: Optional[str] = None


class TelegramRequest(BaseModel):
    """Class for conversion and validation of telegram update request."""

    message: Optional[Message]

    def is_message_request(self) -> bool:
        return bool(self.message)

    def get_identifier(self) -> str:
        return f"{self.message.user.user_id}"

    def get_message(self) -> str:
        message = self.message
        if message.text:
            return message.text

        if message.caption:
            return message.caption

        return ""

    def get_coordinates(self) -> Tuple[Optional[str], Optional[str]]:
        if self.message.location:
            return str(self.message.location.latitude), str(
                self.message.location.longitude
            )

        return None, None

    def get_media(self) -> Optional[Media]:
        message = self.message

        if message.photo:
            return Media(
                origin=MediaOrigin.TELEGRAM,
                file_id=message.photo[-1].file_id,
                media_type=message.photo[-1].media_type,
            )

        media_list = [message.voice, message.document, message.video, message.audio]
        for item in media_list:
            if item:
                return Media(
                    origin=MediaOrigin.TELEGRAM,
                    file_id=item.file_id,
                    media_type=item.media_type,
                )

        return None
