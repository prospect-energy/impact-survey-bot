"""Contains request models for WhatsApp Business."""
from typing import List, Optional, Tuple

from pydantic import BaseModel, Field

from structs.messages import Media, MediaOrigin


class Text(BaseModel):
    body: str


class MediaModel(BaseModel):
    file_id: str = Field(..., alias="id")
    caption: Optional[str] = ""


class Location(BaseModel):
    latitude: float
    longitude: float


class Message(BaseModel):
    phone_number: str = Field(..., alias="from")
    type: str
    text: Optional[Text]
    image: Optional[MediaModel]
    audio: Optional[MediaModel]
    video: Optional[MediaModel]
    document: Optional[MediaModel]
    location: Optional[Location]


class WhatsAppChangeUpdate(BaseModel):
    messages: Optional[List[Message]]


class WhatsAppChangeUpdateContainer(BaseModel):
    value: WhatsAppChangeUpdate


class WhatsAppRequestEntry(BaseModel):
    changes: List[WhatsAppChangeUpdateContainer]


class WhatsAppRequest(BaseModel):
    """Class for conversion and validation of whatsapp business api request."""

    entry: List[WhatsAppRequestEntry]

    def is_message_request(self) -> bool:
        if self.entry[0].changes[0].value.messages is not None:
            return True

        return False

    def get_phone_number(self) -> str:
        return f"+{self.message_data().phone_number}"

    def get_message(self) -> str:
        message_data_dict = self.message_data().__dict__
        message_type = message_data_dict["type"]

        if message_type == "text":
            return message_data_dict["text"].body

        if message_type in ["image", "video", "document"]:
            return message_data_dict[message_type].caption

        return ""

    def get_coordinates(self) -> Tuple[Optional[str], Optional[str]]:
        if self.message_data().type == "location":
            return str(self.message_data().location.latitude), str(
                self.message_data().location.longitude
            )

        return None, None

    def get_media(self) -> Optional[Media]:
        message_data_dict = self.message_data().__dict__
        message_type = message_data_dict["type"]

        if message_type in ["image", "video", "audio", "document"]:
            return Media(
                origin=MediaOrigin.WHATSAPP_BUSINESS,
                file_id=message_data_dict[message_type].file_id,
                media_type=message_type,
            )

        return None

    def message_data(self) -> Message:
        return self.entry[0].changes[0].value.messages[0]
