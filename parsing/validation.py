"""Contains functions related to validation of surveys."""
import copy
import datetime
from typing import Any, Dict, List, Optional, Tuple

import parsing.util as util
from database.components import Column, Table, WhereClause
from result.error import ValidationException
from result.status import StatusCode
from structs.context import WorkspaceContext
from structs.messages import Media, TranslatedMessage
from structs.sections import CompareSection, ConditionSection, SpecifySection
from structs.selection import SelectionOption
from survey.steps import ActionStep, MessageStep, SelectionStep, Step
from survey.survey import Survey
from type_check import request_validation
from type_check.input_types import Coordinate, Number, PhoneNumber, Url
from type_check.typed_string import is_url_str

REQUEST_VARIABLE_PREFIX = "request."
STEP_VARIABLE_PREFIX = "step."
VAR_VARIABLE_PREFIX = "var."
TIME_VARIABLE_PREFIX = "time."
SESSION_VARIABLE_PREFIX = "session."

REQUEST_VARIABLES = {
    f"{REQUEST_VARIABLE_PREFIX}input": str,
    f"{REQUEST_VARIABLE_PREFIX}identifier": str,
    f"{REQUEST_VARIABLE_PREFIX}lat": Coordinate,
    f"{REQUEST_VARIABLE_PREFIX}lng": Coordinate,
    f"{REQUEST_VARIABLE_PREFIX}media": Media,
}

SESSION_VARIABLES = {
    f"{SESSION_VARIABLE_PREFIX}uuid": str,
    f"{SESSION_VARIABLE_PREFIX}identifier": str,
    f"{SESSION_VARIABLE_PREFIX}name": str,
    f"{SESSION_VARIABLE_PREFIX}participant_id": str,
    f"{SESSION_VARIABLE_PREFIX}groups": list,
    f"{SESSION_VARIABLE_PREFIX}db_cache": dict,
}

TIME_VARIABLES = {
    f"{TIME_VARIABLE_PREFIX}now": datetime,
    f"{TIME_VARIABLE_PREFIX}yesterday": datetime,
}

STATIC_VARIABLES = {**REQUEST_VARIABLES, **SESSION_VARIABLES, **TIME_VARIABLES}


def validate(ctx: WorkspaceContext, survey: Survey) -> None:
    """Validates a survey.
    Returns a ValidationException including details in case of an error."""
    linked_ids = get_linked_ids(survey.steps)

    starting_step = survey.steps.get("START", None)
    if not starting_step:
        raise ValidationException(
            f"Survey does not contain starting step ('START').", {"survey": survey.name}
        )

    check_for_missing_or_redundant_ids(ctx, survey, linked_ids)
    available_groups = get_active_groups_of_workspace(ctx)

    for id_, step in survey.steps.items():
        try:
            if step.type == "action":
                validate_action_step(survey, step)

            if step.type == "message":
                validate_message_step(survey, step)

            if step.type == "selection":
                validate_selection_step(survey, step, available_groups)

        except ValidationException as error:
            error.detail["survey"] = survey.name
            error.detail["id"] = id_
            raise error


def validate_message_step(survey: Survey, step: MessageStep) -> None:
    """Validates variables and condition of message step."""
    try:
        validate_variable_in_text(survey, step.message)
    except ValidationException as error:
        error.detail["field"] = "message"
        raise error

    try:
        validate_variable_in_text(survey, step.help_)
    except ValidationException as error:
        error.detail["field"] = "help"
        raise error

    validate_compare(survey, step.compare)
    validate_condition(survey, step.condition)


def validate_action_step(survey: Survey, step: ActionStep) -> None:
    """Validates params, successors and condition of action step."""
    function_annotations = step.function.__annotations__

    validate_params(function_annotations, step.params)
    validate_condition(survey, step.condition)
    validate_action_variables(survey, step.variables)
    validate_action_successors(function_annotations, step.successors)


def validate_selection_step(
    survey: Survey, step: SelectionStep, available_groups: List[str]
) -> None:
    """Validates variables, options and condition of selection step."""

    try:
        validate_variable_in_text(survey, step.message)
    except ValidationException as error:
        error.detail["field"] = "message"
        raise error

    validate_options(survey, step.options, available_groups)
    validate_condition(survey, step.condition)
    validate_please_specify(survey, step.please_specify)


def validate_variable_in_text(
    survey: Survey, translated_message: Optional[TranslatedMessage]
) -> None:
    """Validates that variables in translated message exist."""
    if translated_message is None:
        return

    for var_token, var_name in translated_message.variables.items():
        verify_variable_exists(survey, var_token, var_name)


def validate_please_specify(
    survey: Survey, please_specify: Optional[SpecifySection]
) -> None:
    """Validate variables in messages of specify section."""
    if please_specify is None:
        return

    try:
        validate_variable_in_text(survey, please_specify.message)
        validate_variable_in_text(survey, please_specify.help_)
    except ValidationException as error:
        error.detail["field"] = "please-specify"
        raise error


def validate_condition(
    survey: Survey, condition_section: Optional[ConditionSection]
) -> None:
    """Validates that params and function of condition match."""
    if condition_section is None:
        return

    annotations = condition_section.func.__annotations__
    try:
        validate_action_variables(survey, condition_section.variables)
        validate_params(annotations, condition_section.params)

    except ValidationException as error:
        error.detail["field"] = "condition"
        raise error


def validate_compare(survey: Survey, compare_section: Optional[CompareSection]) -> None:
    """Validates that params and function of compare section match and that variables exist."""
    if compare_section is None:
        return

    annotations = compare_section.func.__annotations__
    try:
        validate_variable_in_text(survey, compare_section.fail_message)
        validate_action_variables(survey, compare_section.variables)
        validate_params(annotations, compare_section.params)

    except ValidationException as error:
        error.detail["field"] = "compare"
        raise error


def validate_params(function_annotations: Any, params: Dict[str, Any]) -> None:
    """Validates that params match the required parameters of function."""
    annotations_copy = copy.deepcopy(function_annotations)

    if annotations_copy.get("return", "") != "":
        del annotations_copy["return"]
    if annotations_copy.get("ctx", "") != "":
        del annotations_copy["ctx"]

    missing_params = list(annotations_copy.keys() - params.keys())
    if missing_params:
        raise ValidationException(
            f"Function is missing one or more params.",
            {
                "field": "params",
                "missing": missing_params,
                "required": list(annotations_copy.keys()),
            },
        )

    for key, value in params.items():
        if annotations_copy.get(key) is None:
            continue

        if not is_compatible_type(annotations_copy[key], value):
            raise ValidationException(
                "Type of param value does not match the required type for the function.",
                {
                    "field": "params",
                    "given": str(get_value_type(value)),
                    "required": str(annotations_copy[key]),
                },
            )


def validate_options(
    survey: Survey,
    options: Dict[str, List[SelectionOption]],
    available_groups: List[str],
) -> None:
    """Validates that groups and variables in options exist."""

    for group, option_list in options.items():
        if group not in available_groups:
            raise ValidationException(
                f"Group '{group}' is not available in workspace",
                {
                    "field": "options",
                    "group": group,
                    "available_groups": available_groups,
                },
            )

        for option in option_list:
            if option.output_is_variable:
                verify_variable_exists(
                    survey, "{{" + option.output + "}}", option.output
                )


def validate_action_variables(survey: Survey, variables: Dict[str, Any]) -> None:
    """Validates that variables referenced in action step exist."""

    try:
        for variable_struct in variables.values():
            if isinstance(variable_struct, dict):
                for variable_tuple in variable_struct.values():
                    verify_variable_exists(survey, variable_tuple[0], variable_tuple[1])
                continue

            verify_variable_exists(survey, variable_struct[0], variable_struct[1])
    except ValidationException as error:
        error.detail["field"] = "params"
        raise error


def validate_action_successors(
    function_annotations: Any, successors: Dict[str, Any]
) -> None:
    """Validates that successors of action step match the output of the function."""

    return_type = function_annotations.get("return", Any)

    for expected_output in successors:
        if expected_output == "*":
            continue

        if not is_compatible_type(return_type, expected_output):
            raise ValidationException(
                "Type of action successor does not match return type of function.",
                {
                    "field": "successors",
                    "key": expected_output,
                    "expected_type": str(return_type),
                },
            )


def verify_variable_exists(survey: Survey, var_token: str, var_name: str) -> None:
    """Verifies that a variable defined by token and name exists (in survey)."""
    if REQUEST_VARIABLE_PREFIX in var_token:
        if var_name not in REQUEST_VARIABLES:
            raise ValidationException(
                "Request variable does not exist.",
                {
                    "var_token": var_token,
                    "var_name": var_name,
                    "available_variables": list(REQUEST_VARIABLES.keys()),
                },
            )
        return

    if SESSION_VARIABLE_PREFIX in var_token:
        if var_name not in SESSION_VARIABLES:
            raise ValidationException(
                "Session variable does not exist.",
                {
                    "var_token": var_token,
                    "var_name": var_name,
                    "available_variables": list(SESSION_VARIABLES.keys()),
                },
            )
        return

    if TIME_VARIABLE_PREFIX in var_token:
        if var_name not in TIME_VARIABLES:
            raise ValidationException(
                "Time variable does not exist.",
                {
                    "var_token": var_token,
                    "var_name": var_name,
                    "available_variables": list(TIME_VARIABLES.keys()),
                },
            )
        return

    if STEP_VARIABLE_PREFIX in var_token:
        clean_var_name = var_name
        if len(var_name.split(".")) == 1:
            clean_var_name = f"step.{var_name}"
        referred_step_id = util.get_id_from_step_variable_name(clean_var_name)
        if referred_step_id not in survey.steps:
            raise ValidationException(
                "Step referred by variable does not exist in survey.",
                {
                    "var_token": var_token,
                    "var_name": clean_var_name,
                    "referred_id": referred_step_id,
                },
            )
        return

    if VAR_VARIABLE_PREFIX in var_token:
        if var_name not in survey.custom_variables:
            raise ValidationException(
                "Custom variable is not set in survey.",
                {
                    "var_token": var_token,
                    "var_name": var_name,
                    "available_variables": survey.custom_variables,
                },
            )
        return

    raise ValidationException(
        "Invalid variable.",
        {
            "var_token": var_token,
            "var_name": var_name,
            "allowed_prefixes": ["request.", "session.", "time.", "step.", "var."],
        },
    )


def get_linked_ids(steps: List[Step]) -> Dict[str, str]:
    """Returns a dictionary of all survey ids that are linked by another id.

    Format: {<id>: <linked_by_id>}

    Multiple links to an id are counted as one.
    To be used for missing and redundant id check.
    """

    linked_ids: Dict[str, str] = {}

    def add_to_linked_ids(step_id: str, linked_by_id: str) -> None:
        """Convenience function to add step-id and linked-by-id to dictionary."""
        if step_id and "$" not in step_id and step_id != "END":
            linked_ids[step_id] = linked_by_id

    for step_obj in steps.values():
        if step_obj.type == "message":
            add_to_linked_ids(step_obj.successor, step_obj.id_)
            if step_obj.skip:
                add_to_linked_ids(step_obj.skip.successor, step_obj.id_)
            if step_obj.condition:
                add_to_linked_ids(step_obj.condition.successor, step_obj.id_)
            continue

        if step_obj.type == "action":
            for step_id in step_obj.successors.values():
                add_to_linked_ids(step_id, step_obj.id_)
            if step_obj.condition:
                add_to_linked_ids(step_obj.condition.successor, step_obj.id_)
            continue

        if step_obj.type == "selection":
            for option_array in step_obj.options.values():
                for option in option_array:
                    add_to_linked_ids(option.successor, step_obj.id_)
            add_to_linked_ids(step_obj.successor, step_obj.id_)
            if step_obj.skip:
                add_to_linked_ids(step_obj.skip.successor, step_obj.id_)
            if step_obj.condition:
                add_to_linked_ids(step_obj.condition.successor, step_obj.id_)

    return linked_ids


def check_for_missing_or_redundant_ids(
    ctx: WorkspaceContext, survey: Survey, linked_ids: Dict[str, str]
) -> None:
    """Wrapper function for checking survey steps for missing or redundant ids."""

    step_id, linked_by = has_missing_id(survey.steps, linked_ids)
    if step_id:
        raise ValidationException(
            f"""Step with id '{step_id}' (linked by '{linked_by}')
            is missing in survey '{survey.name}'.
            """,
            {"survey": survey.name, "id": step_id, "linked_by": linked_by},
        )

    redundant_ids = get_redundant_ids(survey.steps, linked_ids)
    for id_ in redundant_ids:
        ctx.logger.error(
            f"Step with id '{id_}' is not linked in survey '{survey.name}'"
        )


def has_missing_id(
    steps: List[Step], linked_ids: Dict[str, str]
) -> Tuple[Optional[str], Optional[str]]:
    """Checks if linked_ids contains step id that is not part of survey steps.
    Returns tuple consiting of step-id and linked-by-id.
    Both return values are None if no missing id is found.
    """

    for step_id, linked_by_id in linked_ids.items():
        if step_id not in steps:
            return step_id, linked_by_id

    return None, None


def get_redundant_ids(steps: List[Step], linked_ids: Dict[str, str]) -> List[str]:
    """Checks if survey steps contain step ids that are not linked by other steps.
    Returns list of redundant step ids.
    """

    redundant_ids = []

    for step_id in steps:
        if step_id not in linked_ids and step_id not in ["START", "END"]:
            redundant_ids.append(step_id)

    return redundant_ids


def is_compatible_type(expected_type: Any, provided_value: Any) -> bool:
    """Checks whether a value's type mathes an expected type."""

    provided_type = get_value_type(provided_value)

    if expected_type == provided_type:
        return True

    if Any in [expected_type, provided_type]:
        return True
    if hasattr(expected_type, "__origin__"):
        return expected_type.__origin__ == provided_type
    if hasattr(provided_type, "__origin__"):
        return provided_type.__origin__ == expected_type
    if expected_type in [int, float]:
        return provided_type == Number
    if provided_type in [int, float]:
        return expected_type == Number

    if expected_type == StatusCode and expected_type.has_value(provided_value):
        return True
    if expected_type == Url and is_url_str(provided_value):
        return True
    if expected_type == Coordinate and request_validation.can_be_float_str(
        provided_value
    ):
        return True
    if expected_type == PhoneNumber and request_validation.is_phone_number_str(
        provided_value
    ):
        return True

    return False


def get_value_type(value: Any) -> Any:
    """Returns the type of a value by considering json format and variables."""
    if value in ["True", "False"]:
        return bool

    if isinstance(value, str):
        _, var_name = util.get_var_token_and_name(value)
        if var_name is not None:
            if var_name in STATIC_VARIABLES:
                return STATIC_VARIABLES[var_name]
            else:
                return Any

    return type(value)


def get_active_groups_of_workspace(ctx: WorkspaceContext) -> List[str]:
    """Returns a list of active groups in a survey."""
    groups_table = Table(ctx.postgres.shared_schema, "groups")

    result = ctx.postgres.select(
        table=groups_table,
        all_columns=False,
        columns=[Column(groups_table, "name")],
        joined_tables=None,
        where_clauses=[
            WhereClause(
                Column(groups_table, "workspace_id"),
                ctx.workspace_id,
            )
        ],
        order_by=None,
        group_by=None,
        fetch_all=True,
    )

    return [group_entry[0] for group_entry in result]
