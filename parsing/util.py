"Contains utility functions that are used accross multiple files."
import re
from typing import Any, Optional, Tuple


def get_var_token_and_name(string: str) -> Tuple[Optional[str], Optional[str]]:
    """Detects variable token in string that has format '{{ <name> }}'.
    Returns token and striped variable name."""
    # regex: https://regex101.com/r/41pGjN/7
    pattern = r".*({{\s*(\w+[.][\w|\/-]+)\s*}}).*"
    match = re.match(pattern, str(string))
    if match:
        token = match.group(1)
        name = match.group(2)
        return token, name

    return None, None


def get_id_from_step_variable_name(var_name: str) -> str:
    """Returns id from step variable name (e.g. 'step.<id>')."""
    refered_step_id = var_name.split(".")[1]
    return refered_step_id


def safe_get(list_: Any, index: int, default: Any) -> Any:
    """Function allows for the safe retrievel of value at index in list.
    If index is out of range, the provided default value is returned.
    """
    allowed_types = [list, str, tuple]

    if type(list_) not in allowed_types:
        raise ValueError(
            f"Parameter list_ hasn't an allowed type ({[t.__name__ for t in allowed_types]})"
        )

    try:
        return list_[index]
    except IndexError:
        return default
