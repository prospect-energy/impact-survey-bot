"""Contains math functions and their type check."""
from typing import Any, Union

from type_check.typed_string import can_be_float_str


def add(value1: Any, value2: Any) -> Union[float, Any]:
    """Returns sum of two values.
    Returns value1 if types are incompatible.
    """
    value1 = __get_typed_value(value1)
    value2 = __get_typed_value(value2)

    if isinstance(value1, type(value2)):
        return value1 + value2

    return value1


def substract(value1: Any, value2: Any) -> Union[float, Any]:
    """Returns result of value2 being substracted from value1.
    Returns value1 if types are incompatible.
    """
    value1 = __get_typed_value(value1)
    value2 = __get_typed_value(value2)

    if isinstance(value1, type(value2)):
        return value1 - value2

    return value1


def multiply(value1: Any, value2: Any) -> Union[float, Any]:
    """Returns result of multiplication of value1 and value2.
    Returns value1 if types are incompatible.
    """
    value1 = __get_typed_value(value1)
    value2 = __get_typed_value(value2)

    if isinstance(value1, type(value2)):
        return value1 * value2

    return value1


def devide(value1: Any, value2: Any) -> Union[float, Any]:
    """Returns result of value1 being devided by value2.
    Returns value1 if types are incompatible.
    """
    value1 = __get_typed_value(value1)
    value2 = __get_typed_value(value2)

    if isinstance(value1, type(value2)):
        return value1 / value2

    return value1


def __get_typed_value(value: Any) -> Union[float, Any]:
    """Converts value to float if possible, else returns value."""
    typed_value = value

    if can_be_float_str(value):
        typed_value = float(value)

    return typed_value
