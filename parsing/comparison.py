"Contains functions to compare untyped values."
from datetime import datetime
from typing import Any, Union

from type_check.datetime import can_be_date, can_be_time, strp_date, strp_time
from type_check.typed_string import can_be_float_str


def less(value: Any, target: Any) -> bool:
    """Is value less than target? Returns false if type conversion fails."""
    value = get_typed_value(value)
    target = get_typed_value(target)

    if not isinstance(value, type(target)):
        return False

    return value < target


def less_or_equal(value: Any, target: Any) -> bool:
    """Is value less or equal target? Returns false if type conversion fails."""
    value = get_typed_value(value)
    target = get_typed_value(target)

    if not isinstance(value, type(target)):
        return False

    return value <= target


def equal(value: Any, target: Any) -> bool:
    """Are value and target equal?"""
    return value == target


def greater_or_equal(value: Any, target: Any) -> bool:
    """Is value greater or equal target? Returns false if type conversion fails."""
    value = get_typed_value(value)
    target = get_typed_value(target)

    if not isinstance(value, type(target)):
        return False

    return value >= target


def greater(value: Any, target: Any) -> bool:
    """Is value greater than target? Returns false if type conversion fails."""
    value = get_typed_value(value)
    target = get_typed_value(target)

    if not isinstance(value, type(target)):
        return False

    return value > target


def between(value: Any, lower_bound: Any, upper_bound: Any) -> bool:
    """Is value inbetween lower and upper bound?
    Returns false if type conversion fails.
    """
    value = get_typed_value(value)
    lower_bound = get_typed_value(lower_bound)
    upper_bound = get_typed_value(upper_bound)

    if not isinstance(value, type(lower_bound)) or not isinstance(
        value, type(upper_bound)
    ):
        return False

    return lower_bound < value < upper_bound


def between_including(value: Any, lower_bound: Any, upper_bound: Any) -> bool:
    """Is value inbetween lower and upper bound or equal to one of them?
    Returns false if type conversion fails.
    """
    value = get_typed_value(value)
    lower_bound = get_typed_value(lower_bound)
    upper_bound = get_typed_value(upper_bound)

    if not isinstance(value, type(lower_bound)) or not isinstance(
        value, type(upper_bound)
    ):
        return False

    return lower_bound <= value <= upper_bound


def greater_midnight(value: Any, target: Any) -> bool:
    """Is value greater than target?
    Includes times until 3am for value if target is greater or equal 9pm.
    Returns false if value or target are not datetimes.
    """
    value = get_typed_value(value)
    target = get_typed_value(target)

    if isinstance(value, datetime) and isinstance(target, datetime):
        if value > target:
            return True

        until_time = strp_time("3:00am")
        from_time = strp_time("9:00pm")

        return target >= from_time and value <= until_time

    return False


def get_typed_value(value: Any) -> Union[datetime, float, Any]:
    """Converts type of value to date, time or float if possible (order applies).
    If no type conversion is possible, function returns value."""
    typed_value = value

    if can_be_date(value):
        return strp_date(value)
    if can_be_time(value):
        return strp_time(value)
    if can_be_float_str(value):
        return float(value)

    return typed_value
