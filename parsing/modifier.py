"Contains functions to modify strings and incomming messages."
import re

from structs.messages import IncommingMessage


def to_lower_str(str_: str) -> str:
    """Returns lowercase version of string."""
    return str_.lower()


def to_lower(req: IncommingMessage) -> IncommingMessage:
    """Returns modified request with lowercase message."""
    req.message = to_lower_str(req.message)
    return req


def to_upper_str(str_: str) -> str:
    """Returns uppercase version of string."""
    return str_.upper()


def to_upper(req: IncommingMessage) -> IncommingMessage:
    """Returns modified request with uppercase message."""
    req.message = to_upper_str(req.message)
    return req


def remove_spaces_str(str_: str) -> str:
    """Returns string with removed whitespaces."""
    return str_.replace(" ", "")


def remove_spaces(req: IncommingMessage) -> IncommingMessage:
    """Returns modified request with message where whitespaces are removed."""
    req.message = remove_spaces_str(req.message)
    return req


def comma_to_dot(req: IncommingMessage) -> IncommingMessage:
    """Returns modified request with message where commas are replaced by dots."""
    req.message = req.message.replace(",", ".")
    return req


def remove_units_str(str_: str) -> str:
    """Returns string where all characters
    except comma, dot and numbers are removed.
    """
    # https://regex101.com/r/uFog2n/1
    return re.sub(r"\^\d+|[^\d|,|.]", "", str_)


def remove_units(req: IncommingMessage) -> IncommingMessage:
    """Returns modified request where all characters
    except comma, dot and numbers in message are removed.
    """
    req.message = remove_units_str(req.message)
    return req
