"""Contains functions to generate survey functionality templates."""
from typing import Dict, List, Optional, Tuple

from parsing import functions
from parsing.functions import update_database_cache, upload_media
from parsing.survey_components import RawFileUploadStruct, RawSpecifyStruct
from structs.sections import CompareSection, SkipSection, SpecifySection
from structs.selection import SelectionOption
from survey.steps import ActionStep, MessageStep, SelectionStep, Step

REQUEST_INPUT_VARIABLE = "request.input"
REQUEST_INPUT_TOKEN = "{{ request.input }}"
REQUEST_MEDIA_VARIABLE = "request.media"
REQUEST_MEDIA_TOKEN = "{{ request.media }}"
REQUEST_LATITUDE_VARIABLE = "request.lat"
REQUEST_LATITUDE_TOKEN = "{{ request.lat }}"
REQUEST_LONGITUDE_VARIABLE = "request.lng"
REQUEST_LONGITUDE_TOKEN = "{{ request.lng }}"
OTHER_TOKEN = "<<other>>"


def get_message_support(
    id_: str,
    message_step: MessageStep,
    compare_section: Optional[CompareSection],
    file_upload: Optional[RawFileUploadStruct],
    has_coordinates: bool,
    db_column: Optional[str],
    successor: str,
) -> Tuple[List[Step], str]:
    """Returns fitting template steps and direct successor for message step."""

    has_compare = bool(compare_section)
    has_file_upload = bool(file_upload)
    has_db_field = bool(db_column)

    if has_db_field:
        if has_coordinates and has_compare:
            return get_coordinates_compare_db_template(
                id_, message_step, compare_section, db_column, successor
            )
        elif has_coordinates and not has_compare:
            return get_coordinates_db_template(id_, db_column, successor)
        elif not has_coordinates and has_compare:
            return get_compare_db_template(
                id_, message_step, compare_section, db_column, successor
            )
        elif has_file_upload:
            return get_upload_db_template(id_, file_upload, db_column, successor)
        else:
            return get_message_db_template(id_, db_column, successor)

    else:
        if has_compare:
            return get_compare_template(id_, message_step, compare_section, successor)
        elif has_file_upload:
            return get_upload_template(id_, file_upload, successor)
        else:
            return [], successor


def get_selection_support(
    id_: str,
    selection_step: SelectionStep,
    processed_options: Dict[str, List[SelectionOption]],
    multiple_choice: Optional[bool],
    please_specify: Optional[RawSpecifyStruct],
    db_column: Optional[str],
) -> Tuple[List[ActionStep], Dict[str, List[SelectionOption]], str]:
    """Returns fitting template steps and direct successor for selection step."""

    switch_struct = [bool(multiple_choice), bool(please_specify), bool(db_column)]

    if switch_struct == [True, True, True]:
        return get_specify_db_template(
            id_, True, processed_options, please_specify, db_column, selection_step.skip
        )
    if switch_struct == [True, False, True]:
        return get_selection_db_template(id_, True, processed_options, db_column)
    if switch_struct == [True, True, False]:
        return get_specify_template(
            id_, True, processed_options, please_specify, selection_step.skip
        )
    if switch_struct == [True, False, False]:
        return get_selection_action_template(id_, True, processed_options)
    if switch_struct == [False, True, True]:
        return get_specify_db_template(
            id_,
            False,
            processed_options,
            please_specify,
            db_column,
            selection_step.skip,
        )
    if switch_struct == [False, False, True]:
        return get_selection_db_template(id_, False, processed_options, db_column)
    if switch_struct == [False, True, False]:
        return get_specify_template(
            id_, False, processed_options, please_specify, selection_step.skip
        )

    return get_selection_action_template(id_, False, processed_options)


def get_compare_db_template(
    id_: str,
    origin_step: MessageStep,
    compare: CompareSection,
    db_column: str,
    successor: str,
) -> Tuple[List[Step], str]:
    """Returns steps and successor for combined comparison and db caching functionality.
    Only works for message step.
    """

    comparison_action_id = f"{id_}-comparison-action"
    comparison_message_id = f"{id_}-comparison-fail"
    db_action_id = f"{id_}-set_db_value"

    return [
        ActionStep(
            id_=comparison_action_id,
            is_selection_action=False,
            function=compare.func,
            params=compare.params,
            variables=compare.variables,
            successors={"True": db_action_id, "False": comparison_message_id},
        ),
        MessageStep(
            id_=comparison_message_id,
            variable_id=id_,
            message=compare.fail_message,
            modifiers=origin_step.modifiers,
            input_validation=origin_step.input_validation,
            input_type=origin_step.input_type,
            invalid_input_message=origin_step.invalid_input_message,
            successor=comparison_action_id,
            image=origin_step.image,
            help_=origin_step.help_,
            help_image=origin_step.help_image,
            skip=origin_step.skip,
            db_column=db_column,
        ),
        ActionStep(
            id_=db_action_id,
            is_selection_action=False,
            function=update_database_cache,
            params={"data": {db_column: REQUEST_INPUT_TOKEN}},
            variables={
                "data": {db_column: [REQUEST_INPUT_TOKEN, REQUEST_INPUT_VARIABLE]}
            },
            successors={
                "*": successor,
            },
        ),
    ], comparison_action_id


def get_compare_template(
    id_: str, origin_step: MessageStep, compare: CompareSection, successor: str
) -> Tuple[List[Step], str]:
    """Returns steps and successor for comparison functionality.
    Only works for message step.
    """

    comparison_action_id = f"{id_}-comparison-action"
    comparison_message_id = f"{id_}-comparison-fail"

    return [
        ActionStep(
            id_=comparison_action_id,
            is_selection_action=False,
            function=compare.func,
            params=compare.params,
            variables=compare.variables,
            successors={"True": successor, "False": comparison_message_id},
        ),
        MessageStep(
            id_=comparison_message_id,
            variable_id=id_,
            message=compare.fail_message,
            modifiers=origin_step.modifiers,
            input_validation=origin_step.input_validation,
            input_type=origin_step.input_type,
            invalid_input_message=origin_step.invalid_input_message,
            successor=comparison_action_id,
            image=origin_step.image,
            help_=origin_step.help_,
            help_image=origin_step.help_image,
            skip=origin_step.skip,
        ),
    ], comparison_action_id


def get_message_db_template(
    id_: str, db_column: str, successor: str
) -> Tuple[List[Step], str]:
    """Returns steps and successor for db caching functionality of message step.
    Only works for message step.
    """

    db_action_id = f"{id_}-set_db_value"

    return [
        ActionStep(
            id_=db_action_id,
            is_selection_action=False,
            function=update_database_cache,
            params={"data": {db_column: REQUEST_INPUT_TOKEN}},
            variables={
                "data": {db_column: [REQUEST_INPUT_TOKEN, REQUEST_INPUT_VARIABLE]}
            },
            successors={
                "*": successor,
            },
        )
    ], db_action_id


def get_upload_db_template(
    id_: str, file_upload: RawFileUploadStruct, db_column: str, successor: str
) -> Tuple[List[Step], str]:
    """Returns steps and successor for combined db caching and file upload functionality.
    Only works for message step.
    """

    upload_action_id = f"{id_}-upload-file"
    db_action_id = f"{id_}-set_db_value"

    upload_step_variable_token = "{{ step." + upload_action_id + " }}"

    return [
        ActionStep(
            id_=upload_action_id,
            is_selection_action=False,
            function=upload_media,
            params={
                "media": REQUEST_MEDIA_TOKEN,
                "name": file_upload.name,
            },
            variables={
                "media": [REQUEST_MEDIA_TOKEN, REQUEST_MEDIA_VARIABLE],
            },
            successors={
                "*": db_action_id,
            },
        ),
        ActionStep(
            id_=db_action_id,
            is_selection_action=False,
            function=update_database_cache,
            params={"data": {db_column: upload_step_variable_token}},
            variables={
                "data": {db_column: [upload_step_variable_token, upload_action_id]}
            },
            successors={
                "*": successor,
            },
        ),
    ], upload_action_id


def get_upload_template(
    id_: str, file_upload: RawFileUploadStruct, successor: str
) -> Tuple[List[Step], str]:
    """Returns steps and successor for file upload functionality.
    Only works for message step.
    """
    upload_action_id = f"{id_}-upload-file"

    return [
        ActionStep(
            id_=upload_action_id,
            is_selection_action=False,
            function=upload_media,
            params={
                "media": REQUEST_MEDIA_TOKEN,
                "name": file_upload.name,
            },
            variables={
                "media": [REQUEST_MEDIA_TOKEN, REQUEST_MEDIA_VARIABLE],
            },
            successors={
                "*": successor,
            },
        )
    ], upload_action_id


def get_coordinates_db_template(
    id_: str, db_column: str, successor: str
) -> Tuple[List[Step], str]:
    """Returns steps and successor for db caching functionality for coordinates.
    Only works for message step.
    """

    db_action_id = f"{id_}-set_db_value"

    lat_column = f"{db_column}_lat"
    lng_column = f"{db_column}_lng"

    return [
        ActionStep(
            id_=db_action_id,
            is_selection_action=False,
            function=update_database_cache,
            params={
                "data": {
                    lat_column: REQUEST_LATITUDE_TOKEN,
                    lng_column: REQUEST_LONGITUDE_TOKEN,
                }
            },
            variables={
                "data": {
                    lat_column: [REQUEST_LATITUDE_TOKEN, REQUEST_LATITUDE_VARIABLE],
                    lng_column: [REQUEST_LONGITUDE_TOKEN, REQUEST_LONGITUDE_VARIABLE],
                }
            },
            successors={
                "*": successor,
            },
        )
    ], db_action_id


def get_coordinates_compare_db_template(
    id_: str,
    origin_step: MessageStep,
    compare: CompareSection,
    db_column: str,
    successor: str,
) -> Tuple[List[Step], str]:
    """Returns steps and successor for combined comparison and db caching functionality.
    Only works for message step.
    """

    comparison_action_id = f"{id_}-comparison-action"
    comparison_message_id = f"{id_}-comparison-fail"
    db_action_id = f"{id_}-set_db_value"

    lat_column = f"{db_column}_lat"
    lng_column = f"{db_column}_lng"

    return [
        ActionStep(
            id_=comparison_action_id,
            is_selection_action=False,
            function=compare.func,
            params=compare.params,
            variables=compare.variables,
            successors={"True": db_action_id, "False": comparison_message_id},
        ),
        MessageStep(
            id_=comparison_message_id,
            variable_id=id_,
            message=compare.fail_message,
            modifiers=origin_step.modifiers,
            input_validation=origin_step.input_validation,
            input_type=origin_step.input_type,
            invalid_input_message=origin_step.invalid_input_message,
            successor=comparison_action_id,
            image=origin_step.image,
            help_=origin_step.help_,
            help_image=origin_step.help_image,
            skip=origin_step.skip,
            db_column=db_column,
        ),
        ActionStep(
            id_=db_action_id,
            is_selection_action=False,
            function=update_database_cache,
            params={
                "data": {
                    lat_column: REQUEST_LATITUDE_TOKEN,
                    lng_column: REQUEST_LONGITUDE_TOKEN,
                }
            },
            variables={
                "data": {
                    lat_column: [REQUEST_LATITUDE_TOKEN, REQUEST_LATITUDE_VARIABLE],
                    lng_column: [REQUEST_LONGITUDE_TOKEN, REQUEST_LONGITUDE_VARIABLE],
                }
            },
            successors={
                "*": successor,
            },
        ),
    ], comparison_action_id


def get_selection_action_template(
    id_: str, multiple_choice: bool, options: Dict[str, List[SelectionOption]]
) -> Tuple[List[Step], Dict[str, List[SelectionOption]], str]:
    """Returns steps, options and successor for selection without additional functionality.
    Only works for selection step.
    """

    selection_action_id = f"{id_}-selection-action"

    return (
        [
            ActionStep(
                id_=selection_action_id,
                is_selection_action=True,
                function=functions.selection_action,
                params={"options": options, "multiple_choice": multiple_choice},
                variables={},
                successors={"invalid": id_},
            )
        ],
        options,
        selection_action_id,
    )


def get_specify_db_template(
    id_: str,
    multiple_choice: bool,
    options: Dict[str, List[SelectionOption]],
    specify_section: SpecifySection,
    db_column: str,
    skip_section: Optional[SkipSection],
) -> Tuple[List[Step], Dict[str, List[SelectionOption]], str]:
    """Returns steps, options and successor for combined please-specify
    and db-caching functionality.
    Only works for selection step.
    """

    selection_action_id = f"{id_}-selection-action"
    step_variable_token = "{{ step." + id_ + " }}"

    support_steps = [
        ActionStep(
            id_=selection_action_id,
            is_selection_action=True,
            function=functions.selection_action,
            params={"options": options, "multiple_choice": multiple_choice},
            variables={},
            successors={"invalid": id_},
        )
    ]

    option_counter = 0
    for _, option_list in options.items():
        for option in option_list:
            successor = option.successor

            option_counter += 1
            check_action_id = f"{id_}-{option_counter}-please-specify-check"
            please_specify_message_id = f"{id_}-{option_counter}-please-specify"
            replacement_step_id = f"{id_}-{option_counter}-please-specify-output"
            overwrite_step_id = f"{id_}-{option_counter}-please-specify-overwrite"
            db_action_id = f"{id_}-{option_counter}-please-specify-database"

            replacement_variable_token = "{{ step." + replacement_step_id + " }}"

            option.successor = check_action_id

            support_steps = support_steps + [
                ActionStep(
                    id_=check_action_id,
                    is_selection_action=False,
                    function=functions.value_in_list,
                    params={
                        "list_": step_variable_token,
                        "value": OTHER_TOKEN,
                    },
                    variables={"list_": [step_variable_token, id_]},
                    successors={
                        "True": please_specify_message_id,
                        "False": db_action_id,
                    },
                ),
                MessageStep(
                    id_=please_specify_message_id,
                    variable_id=please_specify_message_id,
                    message=specify_section.message,
                    modifiers=specify_section.modifiers,
                    input_validation=specify_section.input_validaiton,
                    input_type=specify_section.input_type,
                    invalid_input_message=specify_section.invalid_input_message,
                    successor=replacement_step_id,
                    image=specify_section.image,
                    help_=specify_section.help_,
                    skip=skip_section,
                ),
                ActionStep(
                    id_=replacement_step_id,
                    is_selection_action=False,
                    function=functions.replace_in_text,
                    params={
                        "text": step_variable_token,
                        "value": OTHER_TOKEN,
                        "replace_with": REQUEST_INPUT_TOKEN,
                    },
                    variables={
                        "text": [step_variable_token, id_],
                        "replace_with": [REQUEST_INPUT_TOKEN, REQUEST_INPUT_VARIABLE],
                    },
                    successors={
                        "*": overwrite_step_id,
                    },
                ),
                ActionStep(
                    id_=overwrite_step_id,
                    is_selection_action=False,
                    function=functions.overwrite_step_variable,
                    params={"id_": id_, "value": replacement_variable_token},
                    variables={
                        "value": [replacement_variable_token, replacement_step_id],
                    },
                    successors={
                        "success": db_action_id,
                    },
                ),
                ActionStep(
                    id_=db_action_id,
                    is_selection_action=False,
                    function=functions.update_database_cache,
                    params={"data": {db_column: step_variable_token}},
                    variables={"data": {db_column: [step_variable_token, id_]}},
                    successors={"*": successor},
                ),
            ]

    return support_steps, options, selection_action_id


def get_selection_db_template(
    id_: str,
    multiple_choice: bool,
    options: Dict[str, List[SelectionOption]],
    db_column: str,
) -> Tuple[List[Step], Dict[str, List[SelectionOption]], str]:
    """Returns steps, options and successor for db-caching functionality.
    Only works for selection step.
    """

    selection_action_id = f"{id_}-selection-action"
    step_variable_token = "{{ step." + id_ + " }}"

    support_steps = [
        ActionStep(
            id_=selection_action_id,
            is_selection_action=True,
            function=functions.selection_action,
            params={"options": options, "multiple_choice": multiple_choice},
            variables={},
            successors={"invalid": id_},
        )
    ]

    option_counter = 0
    for _, option_list in options.items():
        for option in option_list:
            successor = option.successor

            option_counter += 1
            db_action_id = f"{id_}-{option_counter}-database-update"

            option.successor = db_action_id

            support_steps.append(
                ActionStep(
                    id_=db_action_id,
                    is_selection_action=False,
                    function=functions.update_database_cache,
                    params={"data": {db_column: step_variable_token}},
                    variables={"data": {db_column: [step_variable_token, id_]}},
                    successors={"*": successor},
                )
            )

    return support_steps, options, selection_action_id


def get_specify_template(
    id_: str,
    multiple_choice: bool,
    options: Dict[str, List[SelectionOption]],
    specify_section: SpecifySection,
    skip_section: Optional[SkipSection],
) -> Tuple[List[Step], Dict[str, List[SelectionOption]], str]:
    """Returns steps, options and successor for please-specify functionality.
    Only works for selection step.
    """

    selection_action_id = f"{id_}-selection-action"
    step_variable_token = "{{ step." + id_ + " }}"

    support_steps = [
        ActionStep(
            id_=selection_action_id,
            is_selection_action=True,
            function=functions.selection_action,
            params={"options": options, "multiple_choice": multiple_choice},
            variables={},
            successors={"invalid": id_},
        )
    ]

    option_counter = 0
    for _, option_list in options.items():
        for option in option_list:
            successor = option.successor

            option_counter += 1
            check_action_id = f"{id_}-{option_counter}-please-specify-check"
            please_specify_message_id = f"{id_}-{option_counter}-please-specify"
            replacement_step_id = f"{id_}-{option_counter}-please-specify-output"
            overwrite_step_id = f"{id_}-{option_counter}-please-specify-overwrite"

            replacement_variable_token = "{{ step." + replacement_step_id + " }}"

            option.successor = check_action_id

            support_steps = support_steps + [
                ActionStep(
                    id_=check_action_id,
                    is_selection_action=False,
                    function=functions.value_in_list,
                    params={
                        "list_": step_variable_token,
                        "value": OTHER_TOKEN,
                    },
                    variables={"list_": [step_variable_token, id_]},
                    successors={"True": please_specify_message_id, "False": successor},
                ),
                MessageStep(
                    id_=please_specify_message_id,
                    variable_id=please_specify_message_id,
                    message=specify_section.message,
                    modifiers=specify_section.modifiers,
                    input_validation=specify_section.input_validaiton,
                    input_type=specify_section.input_type,
                    invalid_input_message=specify_section.invalid_input_message,
                    successor=replacement_step_id,
                    image=specify_section.image,
                    help_=specify_section.help_,
                    skip=skip_section,
                ),
                ActionStep(
                    id_=replacement_step_id,
                    is_selection_action=False,
                    function=functions.replace_in_text,
                    params={
                        "text": step_variable_token,
                        "value": OTHER_TOKEN,
                        "replace_with": REQUEST_INPUT_TOKEN,
                    },
                    variables={
                        "text": [step_variable_token, id_],
                        "replace_with": [REQUEST_INPUT_TOKEN, REQUEST_INPUT_VARIABLE],
                    },
                    successors={
                        "*": overwrite_step_id,
                    },
                ),
                ActionStep(
                    id_=overwrite_step_id,
                    is_selection_action=False,
                    function=functions.overwrite_step_variable,
                    params={"id_": id_, "value": replacement_variable_token},
                    variables={
                        "value": [replacement_variable_token, replacement_step_id],
                    },
                    successors={
                        "success": successor,
                    },
                ),
            ]

    return support_steps, options, selection_action_id
