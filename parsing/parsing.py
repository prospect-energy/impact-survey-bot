"Contains functions for parsing survey data."
from typing import Any, Callable, Dict, List, Optional, Tuple

import parsing.util as p_util
from parsing import functions, modifier, templates, validation
from parsing.survey_components import (
    RawActionStep,
    RawCompareStruct,
    RawConditionStruct,
    RawMessageStep,
    RawSelectionStep,
    RawSkipStruct,
    RawSpecifyStruct,
    RawStep,
    RawSurveyStruct,
)
from parsing.translations import Translations
from result.error import (
    ComponentException,
    SurveyException,
    TranslationException,
    ValidationException,
    WorkspaceException,
)
from structs.context import WorkspaceContext
from structs.messages import TranslatedMessage
from structs.sections import (
    CompareSection,
    ConditionSection,
    ConfigSection,
    SkipSection,
    SpecifySection,
)
from structs.selection import SelectionOption
from survey.steps import ActionStep, MessageStep, SelectionStep, Step
from survey.survey import Survey
from type_check import request_validation

FUNCTIONS = {
    "add-numbers": functions.add_numbers,
    "append-to-database-value": functions.append_to_database_value,
    "can-be-float": functions.can_be_float,
    "database-contains": functions.database_contains,
    "devide-numbers": functions.devide_numbers,
    "event": functions.event,
    "get-database-value": functions.get_database_value,
    "get-database-value-by-order": functions.get_database_value_by_order,
    "get-first-match": functions.get_first_match,
    "has-format": functions.has_format,
    "is-between": functions.is_between,
    "is-between-including": functions.is_between_including,
    "is-database-field-empty": functions.is_database_field_empty,
    "is-equal": functions.is_equal,
    "is-greater-or-equal": functions.is_greater_or_equal_than,
    "is-greater-than": functions.is_greater_than,
    "is-later-time-including-midnight": functions.is_greater_time_including_midnight,
    "is-less-or-equal": functions.is_less_or_equal_than,
    "is-less-than": functions.is_less_than,
    "multiply-numbers": functions.multiply_numbers,
    "replace-in-text": functions.replace_in_text,
    "set-cached-database-value": functions.update_database_cache,
    "set-database-value": functions.set_database_value,
    "set-variable": functions.set_variable,
    "substract-numbers": functions.substract_numbers,
    "update-database-entry": functions.update_database_entry,
    "upload-file-to-s3": functions.upload_file_to_s3,
    "upload-media": functions.upload_media,
    "value-in-list": functions.value_in_list,
    "--check_verification_code": functions.check_verification_code,
    "--clear-database-cache": functions.clear_database_cache,
    "--forward-to-step": functions.forward_to_step,
    "--overwrite-step-variable": functions.overwrite_step_variable,
    "--set-custom-field": functions.set_custom_field,
    "--set-language": functions.set_language,
    "--update-session": functions.update_session,
    "--upload-registration-inputs": functions.upload_registration_inputs,
}


COMPARISONS = {
    "<": functions.is_less_than,
    "<=": functions.is_less_or_equal_than,
    "equals": functions.is_equal,
    "=": functions.is_equal,
    ">=": functions.is_greater_or_equal_than,
    ">": functions.is_greater_than,
    "><": functions.is_between,
    ">=<": functions.is_between_including,
    ">midnight": functions.is_greater_time_including_midnight,
    "has-format": functions.has_format,
}


CONDITIONS = {
    "<": functions.is_less_than,
    "<=": functions.is_less_or_equal_than,
    "equals": functions.is_equal,
    "=": functions.is_equal,
    ">=": functions.is_greater_or_equal_than,
    ">": functions.is_greater_than,
    "><": functions.is_between,
    ">=<": functions.is_between_including,
    "has-selected": functions.has_selected,
    "has-groups": functions.has_groups,
}


VALIDATIONS = {
    "text-100": request_validation.is_text100,
    "text-long": request_validation.is_text,
    "integer": request_validation.can_be_int,
    "decimal": request_validation.can_be_float,
    "number": request_validation.can_be_float,
    "location": request_validation.has_coordinates,
    "picture": request_validation.has_image,
    "audio": request_validation.has_audio,
    "video": request_validation.has_video,
    "document": request_validation.has_document,
    "date": request_validation.is_date,
    "time": request_validation.is_time,
    "time-12": request_validation.is_time12,
    "time-24": request_validation.is_time24,
    "url": request_validation.is_url,
    "phonenumber": request_validation.is_phone_number,
}


MODIFIERS = {
    "comma-to-dot": modifier.comma_to_dot,
    "to-lower": modifier.to_lower,
    "to-upper": modifier.to_upper,
    "remove-spaces": modifier.remove_spaces,
    "remove-units": modifier.remove_units,
}


class DBColumn:
    """Struct for db column name and input-type."""

    def __init__(self, name: str, input_type: str) -> None:
        self.name = name
        self.input_type = input_type


class ParsedStepsOfSurvey:
    """Struct for result of parsing of all steps in a survey."""

    def __init__(
        self, step_dictionary: Dict[str, Step], db_columns: Dict[str, str]
    ) -> None:
        self.step_dictionary = step_dictionary
        self.db_columns = db_columns


class ParsedStep:
    """Struct for result of message and selection step parsing."""

    def __init__(
        self, step: Step, support_steps: List[Step], db_column: Optional[DBColumn]
    ) -> None:
        self.step = step
        self.support_steps = support_steps
        self.db_column = db_column


class InputValidation:
    """Struct for input validation items."""

    def __init__(self, function: Callable, invalid_message: TranslatedMessage) -> None:
        self.function = function
        self.invalid_message = invalid_message


class ActionParams:
    """Struct for action step params."""

    def __init__(self, params: Dict[str, Any], variables: Dict[str, Any]) -> None:
        self.params = params
        self.variables = variables


def parse(
    ctx: WorkspaceContext, translations: Translations, config: Dict[str, Any]
) -> Dict[str, Survey]:
    """Parses all surveys of a workspace.
    Returns dictionary consiting of survey name and survey object."""
    check_validation_translation_mapping(translations)

    ctx.logger.info("Parsing surveys...")
    surveys: Dict[str, Survey] = {}

    survey_config = config["surveys"]
    registration_survey_name = config["registration"]

    for survey_name, data in survey_config.items():

        survey = parse_survey(ctx, translations, survey_name, data)
        validation.validate(ctx, survey)

        surveys[survey_name] = survey

        ctx.logger.info(f"Added survey: {survey_name}")

    selection_survey = generate_selection_survey(
        ctx, translations, registration_survey_name, survey_config
    )
    surveys["selection"] = selection_survey

    ctx.logger.info("Successfully completed parsing.")

    return surveys


def parse_survey(
    ctx: WorkspaceContext,
    translations: Translations,
    survey_name: str,
    survey_entry: Dict[str, Any],
) -> Survey:
    """Parses and returns survey from survey data by name."""

    data = survey_entry["data"]

    try:
        raw_survey = RawSurveyStruct(data)
    except ComponentException as error:
        error.detail["survey"] = survey_name
        raise error

    raw_config = raw_survey.config
    config = ConfigSection(
        raw_config.create_db_entry,
        raw_config.table,
        raw_config.id_column,
        raw_config.has_finish_message,
        process_message(ctx, translations, raw_config.finish_message),
    )

    parsed_steps = parse_steps(ctx, translations, survey_name, raw_survey)

    return Survey(
        survey_name,
        config,
        parsed_steps.db_columns,
        parsed_steps.step_dictionary,
    )


def add_parsed_step(
    survey_steps: Dict[str, Step], db_columns: Dict[str, str], parsed_step: ParsedStep
) -> None:
    """Adds items from parsed step to steps dictionary and db mapping."""
    step = parsed_step.step
    support_steps = parsed_step.support_steps
    db_column = parsed_step.db_column

    if parsed_step.db_column:
        if db_column.input_type == "location":
            db_columns[f"{db_column.name}_lat"] = db_column.input_type
            db_columns[f"{db_column.name}_lng"] = db_column.input_type
        else:
            db_columns[db_column.name] = db_column.input_type

    survey_steps[step.id_] = step
    for support_step in support_steps:
        survey_steps[support_step.id_] = support_step


def parse_steps(
    ctx: WorkspaceContext,
    translations: Translations,
    survey_name: str,
    raw_survey: RawSurveyStruct,
) -> ParsedStepsOfSurvey:
    """Handles the parsing of all steps in a survey.
    Returns parsed step dictionary as well as db mapping.
    """

    survey_steps = {}
    db_columns = {
        "created_at": "timestamp",
        "participant_id": "integer",
        "survey_duration": "decimal",
        "completed": "boolean",
    }

    for step_index, item in enumerate(raw_survey.steps):
        # Item is a comment
        if isinstance(item, str):
            continue

        try:
            raw_step = RawStep(item)
        except ComponentException as error:
            error.detail["survey"] = survey_name
            error.detail["index"] = step_index
            raise error

        step_id = raw_step.id_

        if step_id in survey_steps:
            raise SurveyException(
                f"Duplicated id: {raw_step.id_}",
                {"survey": survey_name, "id": raw_step.id_},
            )

        try:
            if raw_step.type == "message":
                add_parsed_step(
                    survey_steps, db_columns, parse_message(ctx, translations, item)
                )

            elif raw_step.type == "selection":
                add_parsed_step(
                    survey_steps, db_columns, parse_selection(ctx, translations, item)
                )

            elif raw_step.type == "action":
                step = parse_action(item)
                survey_steps[step.id_] = step

            else:
                raise SurveyException(
                    f"Invalid step type: {raw_step.type}",
                    {"survey": survey_name, "id": raw_step.id_},
                )

        except (WorkspaceException, ComponentException) as error:
            error.detail["survey"] = survey_name
            error.detail["id"] = raw_step.id_
            raise error

    try:
        survey_steps["START"]
    except KeyError:
        raise SurveyException(
            f"Step with id 'START' is missing",
            {"survey": survey_name},
        )

    return ParsedStepsOfSurvey(survey_steps, db_columns)


# Parse and process message step
def parse_message(
    ctx: WorkspaceContext, translations: Translations, structured_dict: dict
) -> ParsedStep:
    """Returns parsed message step."""

    db_column = None
    raw_message = None

    raw_message = RawMessageStep(structured_dict)

    input_validation = process_input_type(translations, raw_message.input_type)
    compare_section = process_compare(ctx, translations, raw_message.compare)
    successor = get_successor_id(raw_message.successor)

    message_step = MessageStep(
        id_=raw_message.id_,
        variable_id=raw_message.id_,
        message=process_message(ctx, translations, raw_message.message),
        modifiers=get_modifier_functions(raw_message.modifiers),
        input_validation=input_validation.function,
        input_type=raw_message.input_type,
        invalid_input_message=input_validation.invalid_message,
        successor=successor,
        image=raw_message.image,
        help_=process_message(ctx, translations, raw_message.help_message),
        help_image=raw_message.help_image,
        skip=get_skip_section(raw_message.skip),
        condition=process_condition(raw_message.condition),
        compare=compare_section,
        db_column=raw_message.db_column,
    )

    support_steps, successor = templates.get_message_support(
        raw_message.id_,
        message_step,
        compare_section,
        raw_message.file_upload,
        (raw_message.input_type == "location"),
        raw_message.db_column,
        successor,
    )

    if raw_message.db_column:
        db_column = DBColumn(raw_message.db_column, raw_message.input_type)

    message_step.successor = successor
    return ParsedStep(message_step, support_steps, db_column)


# Parse and process action step
def parse_action(structured_dict: dict) -> ActionStep:
    """Returns parsed action step."""
    raw_action = None

    raw_action = RawActionStep(structured_dict)

    action_params = process_params(raw_action.params)

    return ActionStep(
        id_=raw_action.id_,
        is_selection_action=False,
        function=get_action_function(raw_action.function),
        params=action_params.params,
        variables=action_params.variables,
        condition=process_condition(raw_action.condition),
        successors=process_successor_dict(raw_action.successors),
    )


# Parse and process selection step
def parse_selection(
    ctx: WorkspaceContext, translations: Translations, structured_dict: dict
) -> ParsedStep:
    """Returns parsed selection step."""

    db_column = None
    raw_selection = None

    raw_selection = RawSelectionStep(structured_dict)

    specify_section = process_please_specify(
        ctx, translations, raw_selection.please_specify
    )

    processed_options = process_options(
        ctx, translations, raw_selection.options, specify_section
    )

    selection_step = SelectionStep(
        id_=raw_selection.id_,
        message=process_message(ctx, translations, raw_selection.message),
        options=processed_options,
        image=raw_selection.image,
        help_=process_message(ctx, translations, raw_selection.help_message),
        help_image=raw_selection.help_image,
        skip=get_skip_section(raw_selection.skip),
        condition=process_condition(raw_selection.condition),
        please_specify=specify_section,
        db_column=raw_selection.db_column,
    )

    (
        support_steps,
        modified_options,
        selection_action_id,
    ) = templates.get_selection_support(
        raw_selection.id_,
        selection_step,
        processed_options,
        raw_selection.multiple_choice,
        specify_section,
        raw_selection.db_column,
    )

    selection_step.options = modified_options

    input_type = (
        raw_selection.please_specify.input_type
        if raw_selection.please_specify
        else "text-long"
    )
    if raw_selection.db_column:
        db_column = DBColumn(raw_selection.db_column, input_type)

    selection_step.successor = selection_action_id

    return ParsedStep(selection_step, support_steps, db_column)


def process_condition(
    condition_struct: Optional[RawConditionStruct],
) -> Optional[ConditionSection]:
    """Returns parsed and type-checked condition section."""

    if condition_struct:
        function = CONDITIONS.get(condition_struct.operator, None)
        if function is None:
            raise SurveyException(
                f"Condition: Operator must be one of: {list(CONDITIONS.keys())}"
            )

        action_params = process_params(
            {
                "value": condition_struct.value,
                "compare_with": condition_struct.compare_with,
                "lower_bound": (
                    condition_struct.compare_range[0]
                    if condition_struct.compare_range
                    else None
                ),
                "upper_bound": (
                    condition_struct.compare_range[1]
                    if condition_struct.compare_range
                    else None
                ),
            }
        )

        return ConditionSection(
            function,
            action_params.params,
            action_params.variables,
            get_successor_id(condition_struct.jump_to),
        )

    return None


def process_compare(
    ctx: WorkspaceContext,
    translations: Translations,
    compare_struct: Optional[RawCompareStruct],
) -> Optional[CompareSection]:
    """Returns parsed and type-checked compare section."""

    if compare_struct:
        function = COMPARISONS.get(compare_struct.operator)
        if function is None:
            raise SurveyException(
                f"Comparison: Operator must be one of: {list(COMPARISONS.keys())}"
            )

        action_params = process_params(
            {
                "compare_with": compare_struct.compare_with,
                "lower_bound": (
                    compare_struct.compare_range[0]
                    if compare_struct.compare_range
                    else None
                ),
                "upper_bound": (
                    compare_struct.compare_range[1]
                    if compare_struct.compare_range
                    else None
                ),
            }
        )

        action_params.params["value"] = "{{ request.input }}"
        action_params.variables["value"] = ["{{ request.input }}", "request.input"]

        return CompareSection(
            func=function,
            params=action_params.params,
            variables=action_params.variables,
            fail_message=process_message(ctx, translations, compare_struct.fail),
        )

    return None


def process_input_type(translations: Translations, input_type: str) -> InputValidation:
    """Converts input-type string to validation function, input-type object
    and invalid input translated message.
    Returns SurveyException if input-type is not mapped.
    """
    if input_type not in VALIDATIONS:
        raise SurveyException(f"Validation {input_type} does not exist")

    processed_validation = VALIDATIONS.get(input_type)
    invalid_input_message = translations.get_invalid_input_tm(input_type)

    return InputValidation(processed_validation, invalid_input_message)


def process_message(
    ctx: WorkspaceContext, translations: Translations, message: Optional[str]
) -> Optional[TranslatedMessage]:
    """Return translated message of message key.
    If message is empty string or None, return None.
    """

    if not message:
        return None

    translated_message = translations.get_translated_message(message)
    if translated_message.get_message(ctx, "") == message:
        var_token, var_name = p_util.get_var_token_and_name(message)
        if var_token:
            translated_message.variables[var_token] = var_name
    return translated_message


def process_options(
    ctx: WorkspaceContext,
    translations: Translations,
    options: List[List[Any]],
    specify_section: Optional[SpecifySection],
) -> Dict[str, List[SelectionOption]]:
    """Parses and converts selection options.
    Returns dictionary with format:

    {
        "<group>": [SelectionOption]
    }

    Groups included specified groups and default group '*'.
    """

    processed_options: Dict[str, List[SelectionOption]] = {}

    # Convert option items into SelectionOption objects, convert text and successors
    for option_tuple in options:
        text = option_tuple[0]
        successor = option_tuple[1]
        output: Any = p_util.safe_get(option_tuple, 2, "")
        group: str = p_util.safe_get(option_tuple, 3, "default")

        output_is_variable = False
        processed_successor = get_successor_id(successor)

        if isinstance(output, str):
            token, var_name = p_util.get_var_token_and_name(output)
            if token:
                output = var_name
                output_is_variable = True

        selection_option = SelectionOption(
            text=process_message(ctx, translations, text),
            output=output,
            output_is_variable=output_is_variable,
            successor=processed_successor,
        )

        if not processed_options.get(group, None):
            processed_options[group] = []

        processed_options[group].append(selection_option)

    if specify_section:
        option_list = processed_options.get("default", [])
        option_list.append(
            SelectionOption(
                text=specify_section.option,
                output="<<other>>",
                output_is_variable=False,
                successor=specify_section.successor,
            )
        )
        processed_options["default"] = option_list

    return processed_options


def process_please_specify(
    ctx: WorkspaceContext,
    translations: Translations,
    specify_struct: Optional[RawSpecifyStruct],
) -> Optional[SpecifySection]:
    """Returns parsed and type-checked please-specify section."""

    if not specify_struct:
        return None

    input_validation = process_input_type(translations, specify_struct.input_type)

    return SpecifySection(
        option=process_message(ctx, translations, specify_struct.option),
        message=process_message(ctx, translations, specify_struct.message),
        help_=process_message(ctx, translations, specify_struct.help_message),
        image=specify_struct.image,
        input_validation=input_validation.function,
        input_type=specify_struct.input_type,
        invalid_input_message=input_validation.invalid_message,
        modifiers=get_modifier_functions(specify_struct.modify),
        successor=get_successor_id(specify_struct.successor),
    )


def get_skip_section(skip_struct: Optional[RawSkipStruct]) -> Optional[SkipSection]:
    """Returns parsed and type-checked skip section."""
    if skip_struct:
        return SkipSection(skip_struct.enabled, get_successor_id(skip_struct.jump_to))

    return None


def get_modifier_functions(modifier_list: Optional[List[str]]) -> List[Callable]:
    """Maps modifier names to actual functions and returns list.
    Raises ValidationException if modifier name is not associated with function.
    """
    if modifier_list is None:
        return []

    invalid_modifiers = set(modifier_list) - MODIFIERS.keys()
    if invalid_modifiers:
        raise ValidationException(
            f"Modifier(s) {','.join(invalid_modifiers)} doesn't/don't exist"
        )

    return [MODIFIERS[name] for name in modifier_list]


def get_successor_id(successor: str) -> Optional[str]:
    """Replaces 'none' and 'end' successor ids by None."""
    if str(successor).lower() in ["none", "end"]:
        return None

    return successor


def process_successor_dict(successor_dict: Dict[str, Any]) -> Dict[str, Any]:
    """Converts json successor ids to python-compatible successor ids.
    Returns modified successor dictionary.
    """
    processed_successors = {}

    for output, successor in successor_dict.items():
        processed_successors[output] = get_successor_id(successor)

    return processed_successors


def process_params(params: Dict[str, Any]) -> ActionParams:
    """Modifies param names to match function mapping and detects variables in params."""
    modified_params = {}
    variables = {}

    for key, value in params.items():
        if value is None:
            continue

        function_param_key = key.replace("-", "_")
        modified_params[function_param_key] = value

        if isinstance(value, str):
            token, name = get_var_token_and_name_incl_step_var(value)
            if token:
                variables[function_param_key] = [token, name]

        if isinstance(value, dict):
            for nested_key, nested_value in value.items():
                token, name = get_var_token_and_name_incl_step_var(nested_value)
                if token:
                    if variables.get(function_param_key, None) is None:
                        variables[function_param_key] = {}

                    variables[function_param_key].update({nested_key: [token, name]})

    return ActionParams(modified_params, variables)


# Map function names to actual functions
def get_action_function(function_name: str) -> Callable:
    """Maps function name to actual function and returns it.
    Raises ValidationException if function name is not associated with function.
    """
    if function_name in FUNCTIONS:
        return FUNCTIONS[function_name]

    raise ValidationException(f"Function {function_name} does not exist")


def generate_selection_survey(
    ctx: WorkspaceContext,
    translations: Translations,
    registration_survey: str,
    survey_dict: Dict[str, Dict[str, Any]],
) -> Survey:
    """Generates and returns selection survey based on survey data."""

    selection_options = {}
    default_language = ctx.default_language
    config = ConfigSection(
        create_db_entry=False,
        table=None,
        id_column=None,
        has_finish_message=False,
        finish_message=None,
    )

    for survey_name, entry in survey_dict.items():
        if survey_name == registration_survey:
            continue

        group = entry["group"]

        option = SelectionOption(
            TranslatedMessage({default_language: survey_name}),
            None,
            False,
            f"${survey_name}",
        )
        if selection_options.get(group):
            selection_options[group].append(option)
        else:
            selection_options[group] = [option]

    selection_step_dict = {
        "START": SelectionStep(
            id_="START",
            message=translations.get_survey_selection_tm(),
            options=selection_options,
            successor="selection-action",
        ),
        "selection-action": ActionStep(
            id_="selection-action",
            is_selection_action=False,
            function=functions.selection_action,
            params={"options": selection_options, "multiple_choice": False},
            variables={},
            successors={
                "invalid": "START",
            },
        ),
    }

    return Survey("selection", config, None, selection_step_dict)


def get_var_token_and_name_incl_step_var(
    value: Any,
) -> Tuple[Optional[str], Optional[str]]:  # TODO: Rename and better return type
    """Returns variable token and name for detected variable in value.
    Replaces step variable name by the referenced step id.
    """
    if isinstance(value, str):
        token, name = p_util.get_var_token_and_name(value)
        if token:
            if "step." in name:
                refered_step_id = p_util.get_id_from_step_variable_name(name)
                name = refered_step_id
            return token, name

    return None, None


def check_validation_translation_mapping(translations: Translations) -> None:
    """Raise TranslationException if translation mapping is missing
    translation for invalid input message of input type.
    """
    for identifier in VALIDATIONS:
        if identifier not in translations.invalid_input_keys.keys():
            raise TranslationException(
                f"Validation identifier '{identifier}' is not part of static translation mapping"
            )
