# Module: Parsing

The parsing module contains the functionality to parse json surveys and translations.
Also the validation of surveys is covered by this module.
Main entry point for this module is [parsing.py](./parsing.py).

## When to use it?

The parsing module is mainly used by the `Workspace` class to parse surveys and translations.

## How to use it?

```python
translation_config = {
    "translations": <translation json>,
    "enabled_languages": {
        <shortcode>: {
            "name": <language name>
            "command": <language command>
        }
    },
    "mandatory_translations": {
        <name>: <translation key>
    }
}

survey_config = {
    "surveys": {
        <survey_name>: {
            "display_name": <display_name of survey>
            "data": <survey json>
        }
    },
    "registration": <name of registration survey>
}


translations = parsing.Translations(
    <WorkspaceContext obj>, translation_config
)

surveys = parsing.parse(
    <WorkspaceContext obj>, translations, survey_config
)
```
