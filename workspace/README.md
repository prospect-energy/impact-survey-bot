# Module: Workspace

The workspace module contains the `Workspace` class and its direct, internal dependencies like `EventHandler` and `CommandHandler`.
The `Workspace` class is used to isolate configurations, surveys and translations from different tenants (plattform-users) on the same bot-instance. Therefore, each participant session is associated with a specific workspace (version). All workspaces are initialized and managed by the `WorkspaceHandler` class which can be found in the [bot](../bot/) module.

## When to use it?

You can use this module if you want to define a `Workspace` object yourself, for example for using it in test cases. Also it is required for initializing a `Session` object and hence also the `SessionContext` class.

## How to use it?

```python
workspace_config = {
    "workspace": {
        "workspace_id": <int>,
        "display_name": <str>,
    },
    "timeout": {
        "timeout_in_minutes": <int>,
        "timeout_enabled": <bool>
    },
    "commands": {
        "command_aliases": <List[Tuple[command_type, default, alias]]>,
    },
    "translations": {
        "translations": <dict>,
        "enabled_languages": <List[Tuple[shortcode, display_name, command]]>,
        "mandatory_translations": <List[Tuple[display_name, translation_key]]>,
    },
    "events": {
        "webhooks": <List[Tuple[display_name, url]]>
    },
    "db_schema": <str>,
    "bucket_folder": <str>,
    "default_language": <str>,
}

workspace = Workspace(
    workspace_ctx = <WorkspaceContext obj>,
    config = workspace_config
)
```
*More about workspace initialization can be found here: [update_handler.py](../bot/update_handler.py).*
