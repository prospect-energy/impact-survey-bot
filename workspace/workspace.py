"""Contains workspace class."""
from __future__ import annotations

from typing import TYPE_CHECKING, Any, Dict

from result.error import BotException

if TYPE_CHECKING:
    from session.session import Session

from typing import List

from database.migration import migrate_survey_tables
from parsing import parsing
from parsing.translations import Translations
from structs.context import SessionContext, WorkspaceContext
from survey.survey import Survey
from workspace.command_handler import CommandHandler
from workspace.event_handler import EventHandler


class Workspace:
    """Class to store data of and manage a workspace."""

    def __init__(self, workspace_ctx: WorkspaceContext, config: Dict[str, Any]) -> None:
        self.__event_handler = EventHandler(config["events"], workspace_ctx.logger)

        self.workspace_id = config["workspace"]["workspace_id"]
        self.display_name = config["workspace"]["display_name"]

        self.ctx = workspace_ctx

        self.timeout_in_minutes = config["timeout"]["timeout_in_minutes"]
        self.timeout_enabled = config["timeout"]["timeout_enabled"]

        self.command_handler: CommandHandler = CommandHandler(
            workspace_ctx, config["commands"], config["translations"]
        )

        self.translations: Translations = Translations(
            workspace_ctx, config["translations"]
        )
        self.surveys: List[Survey] = parsing.parse(
            workspace_ctx, self.translations, config["surveys"]
        )

        self.registration_survey = self.surveys.get("registration")
        if not self.registration_survey:
            raise BotException(
                "Workspace is missing registration survey.",
                {"workspace_id": self.workspace_id},
            )

        self.selection_survey = self.surveys["selection"]

        migrate_survey_tables(
            workspace_ctx.postgres, workspace_ctx.workspace_schema, self.surveys
        )

    def get_session_context(self, session: Session) -> SessionContext:
        """Convenience function to get the session-context for a session and its associated workspace."""
        return SessionContext(
            *self.ctx.__dict__.values(),
            event_handler=self.__event_handler,
            session=session,
        )
