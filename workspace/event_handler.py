"""Contains EventHandler class."""
from typing import Any, Dict

import requests

from logger.logger import Logger
from result.status import StatusCode
from structs.messages import Media


class EventHandler:
    """Class for managing and executing workspace-defined events."""

    def __init__(self, config: Dict[str, Any], logger: Logger) -> None:
        self.__logger = logger
        self.__events = {}

        self.__workspace_token = config["workspace_token"]

        for event_name, event_config in config["webhooks"].items():
            self.__events[event_name] = event_config

    def trigger(self, event_name: str, data: Dict[str, Any]) -> StatusCode:
        """Sents POST-request to webhook-url that is associated with event_name.
        Returns StatusCode.FAIL if event is not defined or payload is invalid.
        """
        webhook_config = self.__events.get(event_name, None)
        if webhook_config is None:
            self.__logger.error(f"Event {event_name} does not exists!")
            return StatusCode.FAIL

        if not isinstance(data, dict):
            self.__logger.error(
                f"Event {event_name} got invalid payload with type {str(type(data))}!"
            )
            return StatusCode.FAIL
        
        for key, value in data.items():
            if isinstance(value, Media):
                data[key] = value.url

        webhook_url = webhook_config["url"]
        include_token = webhook_config["include_token"]

        headers = {}

        if include_token == True:
            headers["ISB-Workspace-Token"] = self.__workspace_token

        try:
            resp = requests.post(url=webhook_url, headers=headers, json=data, timeout=3.0)
            if resp.status_code != 200:
                self.__logger.error(
                    f"Event {event_name} received status_code {resp.status_code}!"
                )
                return StatusCode.FAIL
            
            return resp.json()["response"]

        except requests.exceptions.ReadTimeout:
            self.__logger.error(f"Event {event_name} had a timeout.")
            return StatusCode.FAIL
