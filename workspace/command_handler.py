"""Contains CommandHandler class."""
from typing import Dict, List, Union

from structs.context import WorkspaceContext


class CommandHandler:
    """Class to store and handle workspace-defined commands."""

    def __init__(
        self,
        workspace_ctx: WorkspaceContext,
        command_config: dict,
        translation_config: dict,
    ) -> None:
        self.__ctx = workspace_ctx

        command_aliases: Dict[str, List[str]] = command_config["command_aliases"]

        self.__commands = {
            "start": command_aliases["start"],
            "end": command_aliases["end"],
            "help": command_aliases["help"],
            "back": command_aliases["back"],
            "skip": command_aliases["skip"],
        }

        self.__language_commands = {}
        for shortcode, language_dict in translation_config["enabled_languages"].items():
            self.__language_commands[shortcode] = language_dict["command"]

    def is_command(self, command: str, value: str) -> bool:
        """Returns if value is a valid command-alias for command."""
        if command not in self.__commands:
            self.__ctx.logger.error(
                f"""Command {command} is not a defined command.
                Must be one of: {self.__commands.keys()}
                """
            )

        if str(value).strip().lower() in self.__commands[command]:
            return True

        return False

    def is_language_command(self, value: str) -> Union[str, bool]:
        """Returns shortcode of language for which value is the language command.
        Returns None if value is not a valid (enabled) language command.
        """
        for language, command in self.__language_commands.items():
            if value == command:
                return language

        return False
